//draw pipeline of rigidbodies
//zhang ruisong

#include "RigidBodyUtilities.hlsli"
#include "LightingUtilities.hlsli"

struct VSRigidBodyOut
{
    float4 color : COLOR;
    uint vertexID : VERTEXID;
};

struct GSRigidBodyOut
{
    float4 color : COLOR;
    float2 texcoord : TEXCOORD0;
    float3 normal : NORMAL;
    float3 positionH : POSITION;
    float4 position : SV_Position;
};

struct PSRigidBodyIn
{
    float4 color : COLOR;
    float2 texcoord : TEXCOORD0;
    float3 normal : NORMAL;
    float3 positionH : POSITION;
};

struct RigidBodyDataForRender
{
    float3 position;
    bool isStatic;
    float3 bodySize; //bodysize is half size
    bool isContact;
    float4 orientation;
};

cbuffer cb0
{
    float4x4 g_worldViewProj;
    float3 g_eyePosition;
    float paddings;
};

StructuredBuffer<RigidBodyDataForRender> g_RigidBodyBuffer;

//vertex shader ,different body state use different color
VSRigidBodyOut RigidBodyVS(uint id : SV_VertexID)
{
    VSRigidBodyOut output;
    float4 color = float4(0.0, 0, 1, 1);
    
    if (g_RigidBodyBuffer[id].isContact)
    {
        color += float4(1, 0, 0, 0);
    }

    if (g_RigidBodyBuffer[id].isStatic)
    {
        color += float4(0.5, 0, 0, 0);
    }
	
    output.color = color;
    output.vertexID = id;
	
    return output;
}

cbuffer cbCubeData
{
    //used int geometry shader,from a point create a box
    static float3 g_cubeVertices[6][4] =
    {
		//front
        {
            float3(-1, -1, -1),
		    float3(-1, 1, -1),
		    float3(1, -1, -1),
		    float3(1, 1, -1),
        },
        //right
        {
            float3(1, -1, -1),
            float3(1, 1, -1),
            float3(1, -1, 1),
		    float3(1, 1, 1),
        },
        //back
        {
            float3(1, -1, 1),
		    float3(1, 1, 1),
		    float3(-1, -1, 1),
		    float3(-1, 1, 1),
        },
		//top
        {
            float3(1, 1, 1),
		    float3(1, 1, -1),
		    float3(-1, 1, 1),
		    float3(-1, 1, -1),
        },
        //left
        {
            float3(-1, 1, 1),
		    float3(-1, 1, -1),
		    float3(-1, -1, 1),
		    float3(-1, -1, -1),
        },
        //bottom
        {
            float3(-1, -1, 1),
		    float3(-1, -1, -1),
		    float3(1, -1, 1),
		    float3(1, -1, -1),
        },
    };
	
    //box normals
    static float3 g_normals[6] =
    {
		//front
        float3(0, 0, -1),
        //right
		float3(1, 0, 0),
		//back   
		float3(0, 0, 1),
		//top
		float3(0, 1, 0),
		//left
		float3(-1, 0, 0),
		//bottom
		float3(0, -1, 0),
    };
	
    //texture coordination
    //for body textures ,haven't been used yet
    static float2 g_texcoords[4] =
    {
        float2(0, 0),
		float2(1, 0),
		float2(0, 1),
		float2(1, 1),
    };
	
    //vertex color
    //can use different color per face
    static float4 g_colors[6] =
    {
        float4(1, 0, 0, 1),
		float4(0, 1, 0, 1),
		float4(0, 0, 1, 1),
		float4(1, 1, 0, 1),
		float4(1, 0, 1, 1),
		float4(0, 1, 1, 1),
    };
};

//geometry shader ,create a cube from a point(position)
[maxvertexcount(24)]
void RigidBodyGS(point VSRigidBodyOut input[1],
        inout TriangleStream<GSRigidBodyOut> SpriteStream)
{
    GSRigidBodyOut output;

    float3x3 rotationMat = QuaternionToRotationMatrix(g_RigidBodyBuffer[input[0].vertexID].orientation);
    float3x3 inverseTransposeRotationMat = inverse(transpose(rotationMat));
    
    for (uint i = 0; i < 6; ++i)
    {
        for (uint j = 0; j < 4; ++j)
        {
            float3 position = g_cubeVertices[i][j] * g_RigidBodyBuffer[input[0].vertexID].bodySize;
            position = mul(rotationMat, position);
            position = position + g_RigidBodyBuffer[input[0].vertexID].position;
            output.positionH = output.position = mul(float4(position, 1.0), g_worldViewProj);
            output.color = /*g_colors[i]; */input[0].color;
            output.texcoord = g_texcoords[j];
            //the normal should mul invresetranspose rotation matrix
            output.normal = normalize(mul(inverseTransposeRotationMat, g_normals[i]));
            SpriteStream.Append(output);
        }
    }
    
    SpriteStream.RestartStrip();

}

//static light datas
static Light g_rigidBodyDirectionLight[3] =
{
    float3(1.0, 1.0, 1.0), 0.0, float3(-0.6, -0.6, -0.6), 0.0, float3(0, 0, 0), 0.0, true, float3(0, 0, 0),
    float3(1.0, 1.0, 1.0), 0.0, float3(0.3, 0.7, -0.8), 0.0, float3(0, 0, 0), 0.0, true, float3(0, 0, 0),
    float3(1.0, 1.0, 1.0), 0.0, float3(0.1, 0.2, 0.8), 0.0, float3(0, 0, 0), 0.0, true, float3(0, 0, 0),
};

//add some ambient for not all black
//static ambient light data
static float4 g_Ambient = float4(0.2, 0.2, 0.2, 1.0);

//calculate pixel color(bodies) under lights
float4 RigidBodyPS(PSRigidBodyIn input) : SV_Target
{
    float3 directLights;
    float3 toEye = normalize(g_eyePosition - input.positionH);
    
    Material mat = { input.color, float3(0.7, 0.8, 0.9), 0.8 };
    
    for (uint i = 0; i < 3; i++)
    {
        float3 dl = ComputeDirectionalLight(g_rigidBodyDirectionLight[i], mat, input.normal, g_eyePosition);
        directLights += dl;
    }

    float4 litColor = g_Ambient + float4(directLights, 1.0);
    
    return litColor;
}