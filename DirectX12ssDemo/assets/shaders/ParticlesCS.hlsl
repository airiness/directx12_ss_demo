cbuffer cbCS : register(b0)
{
    uint4 g_param;
    float4 g_paramf;
}

struct PosVelo
{
    float4 position;
    float4 velocity;
    bool isUse;
    float3 paddings;
};

StructuredBuffer<PosVelo> oldPosVelo : register(t0); //srv
RWStructuredBuffer<PosVelo> newPosVelo : register(u0); //uav

[numthreads(128, 1, 1)]
void ParticleCS(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID,
uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{
    float4 position = oldPosVelo[DTid.x].position;
    float4 velocity = oldPosVelo[DTid.x].velocity;
    
    position += velocity * g_paramf.x ;

    if(position.y < -200.0f)
    {
        position.y = 200.0f;
    }
    
    if(DTid.x < g_param.x)
    {
        newPosVelo[DTid.x].position = position;
        newPosVelo[DTid.x].velocity = velocity;
        newPosVelo[DTid.x].isUse = true;
        newPosVelo[DTid.x].paddings = 0.0f;
    }
}