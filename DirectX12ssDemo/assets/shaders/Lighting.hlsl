// Include structures and functions for lighting.
#include "LightingUtilities.hlsli"

//textures
Texture2D gDiffuseMap : register(t0);

//samplers
SamplerState gsamPointWrap : register(s0);
SamplerState gsamPointClamp : register(s1);
SamplerState gsamLinearWrap : register(s2);
SamplerState gsamLinearClamp : register(s3);
SamplerState gsamAnisotropicWrap : register(s4);
SamplerState gsamAnisotropicClamp : register(s5);

// Constant data that varies per frame.
cbuffer cbPerObject : register(b0)
{
    float4x4 gWorld;
    float4x4 gTexTransform;
};

// Constant data that varies per material.
cbuffer cbPass : register(b1)
{
    float4x4 gView;
    float4x4 gInvView;
    float4x4 gProj;
    float4x4 gInvProj;
    float4x4 gViewProj;
    float4x4 gInvViewProj;
    float3 gEyePosW;
    float cbPerObjectPad1;
    float2 gRenderTargetSize;
    float2 gInvRenderTargetSize;
    float gNearZ;
    float gFarZ;
    float gTotalTime;
    float gDeltaTime;
    float4 gAmbientLight;
	
    int gDirectionLightNum;
    int gPointLightNum;
    int gSpotLightNum;
    int gMaxLightNum;
}

StructuredBuffer<Light> gLights : register(t3,space2);

//material dat
cbuffer cbMaterial : register(b2)
{
    float4 gDiffuseAlbedo;
    float3 gFresnelR0;
    float gRoughness;
    float4x4 gMatTransform;
};

struct VertexIn
{
    float3 PosL : POSITION;
    float3 NormalL : NORMAL;
    float2 TexC : TEXCOORD;
};

struct VertexOut
{
    float4 PosH : SV_POSITION;
    float3 PosW : POSITION;
    float3 NormalW : NORMAL;
    float2 TexC : TEXCOORD;
};

float4 ComputeLighting(Material mat,
                       float3 pos, float3 normal, float3 toEye,
                       float3 shadowFactor)
{
    float3 result = 0.0f;

    int i = 0;
    if (gDirectionLightNum > 0)
    {
        for (i = 0; i < gDirectionLightNum; ++i)
        {
            if (gLights[i].IsLightUse)
            {
                result += shadowFactor * ComputeDirectionalLight(gLights[i], mat, normal, toEye);
            }
        }
    }
    
    if (gPointLightNum > 0)
    {
        for (i = gDirectionLightNum; i < gDirectionLightNum + gPointLightNum; ++i)
        {
            if (gLights[i].IsLightUse)
            {
                result += ComputePointLight(gLights[i], mat, pos, normal, toEye);
            }
        }
    }

    if (gSpotLightNum > 0)
    {
        for (i = gDirectionLightNum + gPointLightNum; i < gMaxLightNum; ++i)
        {
            if (gLights[i].IsLightUse)
            {
                result += ComputeSpotLight(gLights[i], mat, pos, normal, toEye);
            }
        }
    }

    return float4(result, 0.0f);
}

VertexOut VS(VertexIn vin)
{
    VertexOut vout = (VertexOut) 0.0f;
	
    // Transform to world space.
    float4 posW = mul(float4(vin.PosL, 1.0f), gWorld);
    vout.PosW = posW.xyz;

    // Assumes nonuniform scaling; otherwise, need to use inverse-transpose of world matrix.
	//
    vout.NormalW = mul(vin.NormalL, (float3x3) gWorld);

    // Transform to homogeneous clip space.
    vout.PosH = mul(posW, gViewProj);
	
	// Output vertex attributes for interpolation across triangle.
    float4 texC = mul(float4(vin.TexC, 0.0f, 1.0f), gTexTransform);
    vout.TexC = mul(texC, gMatTransform).xy;
	
    return vout;
}

float4 PS(VertexOut pin) : SV_Target
{
    float4 diffuseAlbedo = gDiffuseMap.Sample(gsamAnisotropicWrap, pin.TexC) * gDiffuseAlbedo;
	
	//if texture alpha is smaller than 0.1f ,skip
#ifdef ALPHA_TEST
	clip(diffuseAlbedo.a - 0.1f);
#endif
	
    // Interpolating normal can unnormalize it, so renormalize it.
    pin.NormalW = normalize(pin.NormalW);

    // Vector from point being lit to eye. 
    float3 toEyeW = normalize(gEyePosW - pin.PosW);

    // Light terms.
    float4 ambient = gAmbientLight * diffuseAlbedo;

    const float shininess = 1.0f - gRoughness;
    Material mat = { diffuseAlbedo, gFresnelR0, shininess };
    float3 shadowFactor = 1.0f;
    float4 directLight = ComputeLighting(mat, pin.PosW, pin.NormalW, toEyeW, shadowFactor);

    float4 litColor = ambient + directLight;

    // Common convention to take alpha from diffuse albedo.
    litColor.a = diffuseAlbedo.a;

    return litColor;
}

float4 AdjustSaturation(float4 color, float saturation)
{
    // The constants 0.3, 0.59, and 0.11 are chosen because the
    // human eye is more sensitive to green light, and less to blue.
    float grey = dot(color.rgb, float3(0.3, 0.59, 0.11));

    return lerp(grey, color, saturation);
}

float4 BloomPS(VertexOut pin) : SV_TARGET
{
    float intensity = 0.8f - length(float2(0.5f, 0.5f) - pin.TexC);
    intensity = clamp(intensity, 0.0f, 0.5f) * 2.0f;
    return float4(float3(1.0f, 1.0f, 1.0f), intensity);
}