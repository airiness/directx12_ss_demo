//compute shader of rigdbody(OBB) physics
//include collision
//zhang ruisong

//some functions header
#include "RigidBodyUtilities.hlsli"

//block size of one thread group
#define BLOCK_SIZE (128)
//pi / 4
#define ANGULAR_MOTION_THRESHOLD (0.7853981633925f)
//epslon
#define EPSLON (0.0001)

struct RigidBodyDataForCompute
{
    float3 position;
    float mass;
    float3 bodySize;
    float inverseMass;
    float4 orientation;
    float3 velocity;
    bool isContact;
    float3 angularVelocity;
    bool isStatic;
    float3 acceleration;
    bool isUse;
    float3 paddings;
    uint id;
};

//data cache
groupshared RigidBodyDataForCompute bodyDatashaderd[BLOCK_SIZE];

struct RigidBodyDataForRender
{
    float3 position;
    bool isStatic;
    float3 bodySize;
    bool isContact;
    float4 orientation;
};

cbuffer cbframe : register(b0)
{
	//gravity
    float3 g_gravity;
	//body count
    uint g_bodyCount;
    
	//add new body position
    float3 g_addBodyPosition;
	//active body Index(count now)
    uint g_bodyIndex;
    
	//add new body velocity
    float3 g_addBodyVelocity;
	//as a flag to control add a new body ?1:true|0:false
    bool g_isAddNewBody;
    
	//add new body angular velocity
    float3 g_addBodyAngularVelocity;
	//thread goup count
    uint g_threadGroupCount;
    
	//add new body bodysize
    float3 g_addBodySize;
	// debug mode flag
    bool g_isDebugMode;
    
	//delta time
    float g_deltaTime;
	//body density
    float g_bodyDensity;
	//body linear Damping 
    float g_linearDamping;
	//body angularDamping
    float g_angularDamping;
    
    float3 g_gravityCenter;
    float g_spread;
    
    bool g_allCubeFlag;
    bool g_isCenterMode;
    bool g_reset;
    float paddings;
    
	//cube move flags
    bool4 g_moveflags[2];
    bool4 g_rotateflags[2];
};

//get the data from this buffer
StructuredBuffer<RigidBodyDataForCompute> beforeRBCompute : register(t0);
//when finishedcompute save data in this buffer
RWStructuredBuffer<RigidBodyDataForCompute> afterRBCompute : register(u0);
//this buffer for rendering
RWStructuredBuffer<RigidBodyDataForRender> afterRBRender : register(u1);

//create a new rigidbody data of shoot
RigidBodyDataForCompute CreateNewBody(float3 position, float3 bodySize, float3 velocity, float3 angVel)
{
    RigidBodyDataForCompute result;
    
    result.position = position;
    result.mass = g_bodyDensity * bodySize.x * bodySize.y * bodySize.z;
    result.bodySize = bodySize;
    result.inverseMass = 1.0 / result.mass;
    result.orientation = float4(0, 0, 0, 1);
    result.velocity = velocity;
    result.isContact = false;
    result.angularVelocity = angVel;
    result.isStatic = false;
    result.acceleration = float3(0, 0, 0);
    result.isUse = true;
    result.paddings = float3(0, 0, 0);
    result.id = g_bodyIndex - 1;
    //result.inverseInertiaTensor = inverse(InertiaTensor(result.mass, result.bodySize));
    //result.paddings2 = float3(0, 0, 0);
    
    return result;
}

//the function to get projection length.part of SAT
inline float ProjectionLengthToAxis(float3 bodySize, float3 axis, float3x3 rotmat)
{
    float result;
    result =
    bodySize.x * abs(dot(axis, rotmat._11_21_31)) +
    bodySize.y * abs(dot(axis, rotmat._12_22_32)) +
    bodySize.z * abs(dot(axis, rotmat._13_23_33));
    
    return result;
}

//get the penetration of collision
inline float PenetrationOnAxis(float3 bodySize1, float3 bodySize2, float3 centre, inout float3 axis, float3x3 rotmat1, float3x3 rotmat2)
{
    float oneProject = ProjectionLengthToAxis(bodySize1, axis, rotmat1);
    float twoProject = ProjectionLengthToAxis(bodySize2, axis, rotmat2);
    
    float distance = abs(dot(centre, axis));
    
    return oneProject + twoProject - distance;
}

//collision detection
inline bool TryAxis(float3 bodySize1, float3 bodySize2, float3 centre, float3 axis, float3x3 rotmat1, float3x3 rotmat2, uint index, inout float smallestPenetration, inout uint smallestIndex)
{
    if (dot(axis, axis) == 0.0)
        return true;
    normalize(axis);
    
    float penetration = PenetrationOnAxis(bodySize1, bodySize2, centre, axis, rotmat1, rotmat2);
    
    if (penetration <= EPSLON)
        return false;
    
    if (penetration < smallestPenetration)
    {
        smallestPenetration = penetration;
        smallestIndex = index;
    }
    
    return true;
}

//detect two boxes(as OBB) collision state
void BoxBoxCollisionProcess(inout uint cCount, float3x3 rotMat1, RigidBodyDataForCompute b1, RigidBodyDataForCompute b2)
{
    //if the body do not active ,return
    if (b1.isUse == false || b2.isUse == false)
        return;
    //if same body ,return
    if (b1.id == b2.id)
        return;
    
    //-------------------collision detect
    //get the vector from center of box 1 to box2 ,relative to box 1
    float3 toCentre = b2.position - b1.position;

    //penetration
    float pen;
    //best penetration axis index
    uint best = ~0;
    
    //get two box rotation matrix
    //float3x3 rotMat1 = QuaternionToRotationMatrix(b1.orientation); // b1 rotation matrix
    float3x3 rotMat2 = QuaternionToRotationMatrix(b2.orientation); // b2 rotatoin matrix
    
    //6 + 9 
    //if intersection
    bool passDetect;
    
    //0
    float3 axis = rotMat1._11_21_31;
    if (TryAxis(b1.bodySize, b2.bodySize, toCentre, axis, rotMat1, rotMat2, 0, pen, best) == false) 
        return;
    
    //1
    axis = rotMat1._12_22_32;
    if (TryAxis(b1.bodySize, b2.bodySize, toCentre, axis, rotMat1, rotMat2, 1, pen, best) == false)
        return;

    //2
    axis = rotMat1._13_23_33;
    if (TryAxis(b1.bodySize, b2.bodySize, toCentre, axis, rotMat1, rotMat2, 2, pen, best) == false)
        return;
    //3
    axis = rotMat2._11_21_31;
    if (TryAxis(b1.bodySize, b2.bodySize, toCentre, axis, rotMat1, rotMat2, 3, pen, best) == false)
        return;
    //4
    axis = rotMat2._12_22_32;
    if (TryAxis(b1.bodySize, b2.bodySize, toCentre, axis, rotMat1, rotMat2, 4, pen, best) == false)
        return;
    //5
    axis = rotMat2._13_23_33;
    if (TryAxis(b1.bodySize, b2.bodySize, toCentre, axis, rotMat1, rotMat2, 5, pen, best) == false)
        return;
    
    //6
    axis = cross(rotMat1._11_21_31, rotMat2._11_21_31);
    if (TryAxis(b1.bodySize, b2.bodySize, toCentre, axis, rotMat1, rotMat2, 6, pen, best) == false)
        return;
    
    //insure next detect parallel station
    uint bestSingleAxis = best;
    
    //7
    axis = cross(rotMat1._11_21_31, rotMat2._12_22_32);
    if (TryAxis(b1.bodySize, b2.bodySize, toCentre, axis, rotMat1, rotMat2, 7, pen, best) == false)
        return;
    //8
    axis = cross(rotMat1._11_21_31, rotMat2._13_23_33);
    if (TryAxis(b1.bodySize, b2.bodySize, toCentre, axis, rotMat1, rotMat2, 8, pen, best) == false)
        return;
    //9
    axis = cross(rotMat1._12_22_32, rotMat2._11_21_31);
    if (TryAxis(b1.bodySize, b2.bodySize, toCentre, axis, rotMat1, rotMat2, 9, pen, best) == false)
        return;
    //10
    axis = cross(rotMat1._12_22_32, rotMat2._12_22_32);
    if (TryAxis(b1.bodySize, b2.bodySize, toCentre, axis, rotMat1, rotMat2, 10, pen, best) == false)
        return;
    //11
    axis = cross(rotMat1._12_22_32, rotMat2._13_23_33);
    if (TryAxis(b1.bodySize, b2.bodySize, toCentre, axis, rotMat1, rotMat2, 11, pen, best) == false)
        return;
    //12
    axis = cross(rotMat1._13_23_33, rotMat2._11_21_31);
    if (TryAxis(b1.bodySize, b2.bodySize, toCentre, axis, rotMat1, rotMat2, 12, pen, best) == false)
        return;
    //13
    axis = cross(rotMat1._13_23_33, rotMat2._12_22_32);
    if (TryAxis(b1.bodySize, b2.bodySize, toCentre, axis, rotMat1, rotMat2, 13, pen, best) == false)
        return;
    //14
    axis = cross(rotMat1._13_23_33, rotMat2._13_23_33);
    if (TryAxis(b1.bodySize, b2.bodySize, toCentre, axis, rotMat1, rotMat2, 14, pen, best) == false)
        return;

    //intersect flag
    cCount++;

}

//tmp debug func : to detect collision state without rotation
void NoRotateBoxBoxCollision(inout uint cCount, RigidBodyDataForCompute b1, RigidBodyDataForCompute b2)
{
    if (b1.isUse == false || b2.isUse == false)
        return;
    if (b1.id == b2.id)
        return;
    
    float3 dis = abs(b1.position - b2.position);
    
    if (b1.bodySize.x + b2.bodySize.x < dis.x)
        return;
    if (b1.bodySize.y + b2.bodySize.y < dis.y)
        return;
    if (b1.bodySize.z + b2.bodySize.z < dis.z)
        return;
    cCount++;

}

//main compute shader to simulate physics
[numthreads(BLOCK_SIZE, 1, 1)]
void RigidBodyCS(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{
    //debug code for test collision detect
    float3 speed = float3(0, 0, 0);
    float3 rotate = float3(0, 0, 0);
    float unit = (g_rotateflags[1].w == true) ? 0.3 : 0.9;
    if (g_moveflags[0].x == true)
    {
        speed.z = unit;
    }
    if (g_moveflags[0].y == true)
    {
        speed.z = -unit;
    }
    if (g_moveflags[0].z == true)
    {
        speed.x = -unit;
    }
    if (g_moveflags[0].w == true)
    {
        speed.x = unit;
    }
    if (g_moveflags[1].x == true)
    {
        speed.y = unit;
    }
    if (g_moveflags[1].y == true)
    {
        speed.y = -unit;
    }
    if (g_rotateflags[0].x == true)//5
    {
        rotate.x = unit;
    }
    if (g_rotateflags[0].y == true)//6
    {
        rotate.x = -unit;
    }
    if (g_rotateflags[0].z == true)//7
    {
        rotate.y = unit;
    }
    if (g_rotateflags[0].w == true)//8
    {
        rotate.y = -unit;
    }
    if (g_rotateflags[1].x == true)//9
    {
        rotate.z = unit;
    }
    if (g_rotateflags[1].y == true)//0
    {
        rotate.z = -unit;
    }
    
    //get the data from SRV
    RigidBodyDataForCompute bodyData = beforeRBCompute[DTid.x];
    float3x3 rotationMatrix = QuaternionToRotationMatrix(bodyData.orientation);
    
    ///detect collision flag
    uint contactCount = 0;

    [loop]
    for (uint tile = 0; tile < g_threadGroupCount; tile++)
    {
        bodyDatashaderd[GI] = beforeRBCompute[tile * BLOCK_SIZE + GI];
        
        GroupMemoryBarrierWithGroupSync();

        //[unroll]
        for (uint counter = 0; counter < BLOCK_SIZE; counter++)
        {
            BoxBoxCollisionProcess(contactCount, rotationMatrix, bodyData, bodyDatashaderd[counter]);
        }

        GroupMemoryBarrierWithGroupSync();
    }
    
    if (contactCount > 0)
        bodyData.isContact = true;
    else
        bodyData.isContact = false;

    //get the acc
    float3 acc = float3(0, 0, 0);
    if (g_isCenterMode == true)
    {
        acc = (g_gravityCenter - bodyData.position) * bodyData.mass;
    }
    else
    {
        acc += g_gravity;
    }

    ///update position and rotation
    if (bodyData.isStatic == false)//static body's speed is static
    {
        if (g_isDebugMode == true)
        {
            bodyData.velocity = speed;
            bodyData.angularVelocity = rotate;
            bodyData.acceleration = float3(0, 0, 0);
        }
        else
        {
            bodyData.acceleration = g_gravity;
        }

        //add some velocity damping
        bodyData.velocity *= pow(g_linearDamping, g_deltaTime);
        bodyData.angularVelocity *= pow(g_angularDamping, g_deltaTime);
        
        //angular velocity update using quaternion
        float4 axis;
        float fAngle = sqrt(dot(bodyData.angularVelocity, bodyData.angularVelocity));
        
        if (fAngle * g_deltaTime > ANGULAR_MOTION_THRESHOLD)
            fAngle = ANGULAR_MOTION_THRESHOLD / g_deltaTime;
        
        if (fAngle < 0.001f)
        {
            axis = float4(bodyData.angularVelocity, 1.0f) * (0.5f * g_deltaTime -
            (g_deltaTime * g_deltaTime * g_deltaTime) * 0.020833333333f * fAngle * fAngle);
        }
        else
        {
            axis = float4(bodyData.angularVelocity, 1.0f) * (sin(0.5f * fAngle * g_deltaTime) / fAngle);
        }
        
        float4 dorn;
        dorn.xyz = axis.xyz;
        dorn.w = cos(fAngle * g_deltaTime * 0.5f);
        float4 orn0 = bodyData.orientation;
        float4 predictedOrn = QuaternionMultiply(dorn, orn0);
        predictedOrn = normalize(predictedOrn);
        bodyData.orientation = predictedOrn;
        
        //linear velocity update
        bodyData.position = bodyData.position + (bodyData.velocity + acc * g_deltaTime * 0.5f) * g_deltaTime;
        bodyData.velocity = bodyData.velocity + acc * g_deltaTime;
         
    }
    
    ///should insure the bodies in the field?
    //if (bodyData.position.y < -g_spread)
    //{
    //    bodyData.position.y = g_spread;
    //    bodyData.velocity.y = 0.0f;
    //}

    if (g_reset == true && bodyData.isStatic == false)
    {
        float spreadscale = 5;
        float2 randuv_1 = bodyData.velocity.xz;
        float2 randuv_2 = bodyData.angularVelocity.xz;
        bodyData.position.x = (rand_1_05(randuv_1) - 0.5) * g_spread * spreadscale;
        bodyData.position.y = g_spread;
        bodyData.position.z = (rand_1_05(randuv_2) - 0.5) * g_spread * spreadscale;
        bodyData.velocity = float3(0, 0, 0);
    }
    
    ///write datas
    //if the add cube flag is true ,add a new cube as a bullet
    //because the count is begin from 0,when add a new body , the index should -1 here
    if (DTid.x < g_bodyCount)
    {
        if (DTid.x == g_bodyIndex - 1 && g_isAddNewBody == true)
        {
            afterRBCompute[DTid.x] = CreateNewBody(g_addBodyPosition, g_addBodySize, g_addBodyVelocity, g_addBodyAngularVelocity);
            afterRBRender[DTid.x].position = afterRBCompute[DTid.x].position;
            afterRBRender[DTid.x].bodySize = afterRBCompute[DTid.x].bodySize;
        }
        else if (bodyData.isUse == true)
        {
            //copy data to double buffer
            afterRBCompute[DTid.x] = bodyData;
            
            //copy datas to render shader
            afterRBRender[DTid.x].position = afterRBCompute[DTid.x].position;
            afterRBRender[DTid.x].orientation = afterRBCompute[DTid.x].orientation;
            afterRBRender[DTid.x].bodySize = afterRBCompute[DTid.x].bodySize;
            afterRBRender[DTid.x].isContact = afterRBCompute[DTid.x].isContact;
            afterRBRender[DTid.x].isStatic = afterRBCompute[DTid.x].isStatic;
        }
    }
}