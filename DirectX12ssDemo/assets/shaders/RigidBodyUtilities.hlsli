//inertia tensor of a cube x:width, y: height , z: depth
float3x3 InertiaTensor(float mass, float3 bodySize)
{
    float a = (1.0 / 12.0) * mass * (bodySize.y * bodySize.y + bodySize.z * bodySize.z);
    float b = (1.0 / 12.0) * mass * (bodySize.x * bodySize.x + bodySize.z * bodySize.z);
    float c = (1.0 / 12.0) * mass * (bodySize.x * bodySize.x + bodySize.y * bodySize.y);
    
    float3x3 result;
    result[0][1] = 0;
    result[0][2] = 0;
    result[1][0] = 0;
    result[1][2] = 0;
    result[2][0] = 0;
    result[2][1] = 0;
    result[0][0] = a;
    result[1][1] = b;
    result[2][2] = c;

    return result;
}

//https://en.wikipedia.org/wiki/Quaternion
//quaternion multiply
float4 QuaternionMultiply(float4 q1, float4 q2)
{

    float3 q1q2v = cross(q1.xyz, q2.xyz);
    q1q2v += q1.w * q2.xyz + q2.w * q1.xyz;
    float q1q2s = q1.w * q2.w - dot(q1.xyz, q2.xyz);
    
    float4 result = float4(q1q2v, q1q2s);
    
    return result;
}

float3x3 QuaternionToRotationMatrix(float4 q)
{
    float xx = 2 * q.x * q.x, yy = 2 * q.y * q.y, zz = 2 * q.z * q.z;
    float xy = 2 * q.x * q.y, yz = 2 * q.y * q.z;
    float xz = 2 * q.x * q.z;
    float wx = 2 * q.w * q.x, wy = 2 * q.w * q.y, wz = 2 * q.w * q.z;

    float3x3 result;
    
    result[0][0] = 1 - yy - zz;
    result[0][1] = xy - wz;
    result[0][2] = xz + wy;
    
    result[1][0] = xy + wz;
    result[1][1] = 1 - xx - zz;
    result[1][2] = yz - wx;
    
    result[2][0] = xz - wy;
    result[2][1] = yz + wx;
    result[2][2] = 1 - xx - yy;
    
    return result;
}

float3x3 inverse(float3x3 mat)
{
    float a = (mat[1][1] * mat[2][2] - mat[1][2] * mat[2][1]);
    float b = -(mat[1][0] * mat[2][2] - mat[1][2] * mat[2][0]);
    float c = (mat[1][0] * mat[2][1] - mat[1][1] * mat[2][0]);
    float d = -(mat[0][1] * mat[2][2] - mat[0][2] * mat[2][1]);
    float e = (mat[0][0] * mat[2][2] - mat[0][2] * mat[2][0]);
    float f = -(mat[0][0] * mat[2][1] - mat[0][1] * mat[2][0]);
    float g = (mat[0][1] * mat[1][2] - mat[0][2] * mat[1][1]);
    float h = -(mat[0][0] * mat[1][2] - mat[0][2] * mat[1][0]);
    float i = (mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0]);

    float det = 1 / (mat[0][0] * a + b * mat[0][1] + c * mat[0][2]);

    float3x3 imat;
    imat[0][0] = a * det;
    imat[0][1] = d * det;
    imat[0][2] = g * det;
    imat[1][0] = b * det;
    imat[1][1] = e * det;
    imat[1][2] = h * det;
    imat[2][0] = c * det;
    imat[2][1] = f * det;
    imat[2][2] = i * det;

    return imat;
}

//PRNG
float rand_1_05(in float2 uv)
{
    float2 noise = (frac(sin(dot(uv, float2(12.9898, 78.233) * 2.0)) * 43758.5453));
    return abs(noise.x + noise.y) * 0.5;
}