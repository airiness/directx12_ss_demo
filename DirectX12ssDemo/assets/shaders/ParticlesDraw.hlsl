//render particles pipeline
//zhang ruisong
struct VSParticleIn
{
	//float4 color : COLOR;
	uint id : SV_VERTEXID;
};

struct VSParticleOut
{
	float3 position : POSITION;
	float4 color : COLOR;
};

struct GSParticleOut
{
	float2 texcoord : TEXCOORD0;
	float4 color : COLOR;
	float4 position : SV_Position;
};

struct PSParticleIn
{
	float2 texcoord : TEXCOORD0;
	float4 color : COLOR;
};

struct PosVelo
{
	float4 position;
	float4 velocity;
	bool isUse;
	float3 paddings;
};

StructuredBuffer<PosVelo> g_BufPosVelo;

cbuffer cb0
{
	float4x4 g_WorldViewProj;
	float4x4 g_InvView;
};

cbuffer cb1
{
	static float g_ParticleRad = 2.0f;
};

cbuffer cbImmutable
{
	static float3 g_positions[4] =
	{
		float3(-1, 1, -1),
		float3(1, 1, -1),
		float3(-1, -1, -1),
		float3(1, -1, -1),
	};
	
	static float3 g_positionsbottom[4] =
	{
		float3(-1, -1, -1),
		float3(1, -1, -1),
		float3(-1, -1, 1),
		float3(1, -1, 1),
	};
	
	static float2 g_texcoords[4] =
	{
		float2(0, 0),
		float2(1, 0),
		float2(0, 1),
		float2(1, 1),
	};
};

VSParticleOut ParticleVS(uint id : SV_VertexID)
{
	VSParticleOut output;
	
	output.position = g_BufPosVelo[id].position.xyz;
	float mag = g_BufPosVelo[id].velocity.w / 9;
	float4 colorDiv = g_BufPosVelo[id].velocity.xyzw;
    output.color = lerp( /*colorDiv*/float4(0.6f, 0.8f, 0.8f, 1.0f), float4(1.0f, 1.0f, 1.0f, 1.0f) /*input.color*/, mag);
	
	return output;
}


[maxvertexcount(4)]
void ParticleGS(point VSParticleOut input[1], inout TriangleStream<GSParticleOut> SpriteStream)
{
	GSParticleOut output;
	
	for (uint i = 0; i < 4; i++)
	{
		float3 position = g_positions[i] * g_ParticleRad;
		//make the particle billboard
        position = mul(position, (float3x3) g_InvView) + input[0].position;
		output.position = mul(float4(position, 1.0), g_WorldViewProj);
		
		output.color = input[0].color;
		output.texcoord = g_texcoords[i];
		SpriteStream.Append(output);
	}
	
	SpriteStream.RestartStrip();
}

float4 ParticlePS(PSParticleIn input) : SV_Target
{
	float intensity = 0.5f - length(float2(0.5f, 0.5f) - input.texcoord);
	intensity = clamp(intensity, 0.0f, 0.5f) * 2.0f;
    return float4(input.color.xyz, intensity);
}