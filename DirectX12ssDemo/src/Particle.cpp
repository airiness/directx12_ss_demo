#include "pch.h"
#include "Particle.h"
#include "DirectX12Graphics.h"
#include "FrameResource.h"

Particle::Particle()
{
}

Particle::~Particle()
{
}

void Particle::Initialize()
{
	auto device = Graphics::GetDevice();
	//initialize particles color data
	{
		m_ParticleVertices.resize(m_ParticleCount);
		for (size_t i = 0; i < m_ParticleCount; i++)
		{
			m_ParticleVertices[i].color = { 1.0f,1.0f,0.2f,1.0f };
		}
	}

	//initialize particles velocity and position data 
	//and descripte them in descriptor table
	// two buffers for render and compute pipeline
	//like double buffers ,swap them
	{

		m_ParticleDatas.resize(m_ParticleCount);

		for (size_t i = 0; i < m_ParticleCount; i++)
		{
			//velocity
			m_ParticleDatas[i].velocity =
				m_SpeedBase + Vector4(randf(-0.3f, 3.0f), randf(-12.0f, -0.5f), randf(-3.0f, 3.0f), 0.0f);

			//
			Vector3 delta = { m_Spread,m_Spread,m_Spread };
			while (delta.LengthSquared() > m_Spread* m_Spread)
			{
				delta.x = randf(-1.0f, 1.0f) * m_Spread;
				delta.y = randf(-1.0f, 1.0f) * m_Spread;
				delta.z = randf(-1.0f, 1.0f) * m_Spread;
			}

			m_ParticleDatas[i].position.x = m_Center.x + delta.x;
			m_ParticleDatas[i].position.y = m_Center.y + delta.y;
			m_ParticleDatas[i].position.z = m_Center.z + delta.z;
			m_ParticleDatas[i].position.w = 10000.0f * 10000.0f;
		}

		//create and initialize compute shader constant buffer
		//saved particle counts
		{
			ParticleCSConstantsData = {};
			ParticleCSConstantsData.param[0] = m_ParticleCount;
			ParticleCSConstantsData.param[1] = m_ParticleCount;	//need edit
			ParticleCSConstantsData.paramf[0] = 0.1f;
			ParticleCSConstantsData.paramf[1] = 1.0f;

			ParticleCSCB = std::make_unique<UploadBuffer<ParticleCSConstants>>(device, 1, true);

			ParticleCSCB->CopyData(0, ParticleCSConstantsData);
		}
	}
}

void Particle::Update(double dt)
{
}

void Particle::Compute()
{
	auto computeCommandList = Graphics::GetCommandList(D3D12_COMMAND_LIST_TYPE_COMPUTE);
	auto computeCommandAlloc = Graphics::GetCommandAlloc(D3D12_COMMAND_LIST_TYPE_COMPUTE);
	auto computeCommandQueue = Graphics::GetCommandQueue(D3D12_COMMAND_LIST_TYPE_COMPUTE);
	auto computeFence = Graphics::GetFence(D3D12_COMMAND_LIST_TYPE_COMPUTE);

	UINT srvIndex;
	UINT uavIndex;
	ID3D12Resource* pUavResource;
	if (m_SrvIndex == 0)
	{
		srvIndex = ParticleDescriptorHeapIndex::ParticleSrv0;
		uavIndex = ParticleDescriptorHeapIndex::ParticleUav1;
		pUavResource = m_ParticleDataBuffers[0].ParticleDataResource.Get();
	}
	else
	{
		srvIndex = ParticleDescriptorHeapIndex::ParticleSrv1;
		uavIndex = ParticleDescriptorHeapIndex::ParticleUav0;
		pUavResource = m_ParticleDataBuffers[1].ParticleDataResource.Get();
	}

	computeCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(pUavResource,
		D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS));
	computeCommandList->SetPipelineState(m_pPsoCompute);
	computeCommandList->SetComputeRootSignature(m_pRootSigCompute);

	ID3D12DescriptorHeap* ppHeap = m_pDescriptorHeap;
	ID3D12DescriptorHeap* particleHeaps[] = { ppHeap };
	computeCommandList->SetDescriptorHeaps(_countof(particleHeaps), particleHeaps);

	CD3DX12_GPU_DESCRIPTOR_HANDLE srvHandle(ppHeap->GetGPUDescriptorHandleForHeapStart(), srvIndex, Graphics::g_DescriptorSizeCbvSrvUav);
	CD3DX12_GPU_DESCRIPTOR_HANDLE uavHandle(ppHeap->GetGPUDescriptorHandleForHeapStart(), uavIndex, Graphics::g_DescriptorSizeCbvSrvUav);

	computeCommandList->SetComputeRootConstantBufferView(ParticleCSRootParameters::ParticleCSCBV,
		ParticleCSCB->Resource()->GetGPUVirtualAddress());
	computeCommandList->SetComputeRootDescriptorTable(ParticleCSRootParameters::ParticleCSSRVTable, srvHandle);
	computeCommandList->SetComputeRootDescriptorTable(ParticleCSRootParameters::ParticleCSUAVTable, uavHandle);

	computeCommandList->Dispatch(static_cast<UINT>(ceil(m_ParticleCount / 128.0f)), 1, 1);

	computeCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(pUavResource,
		D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE));

	ThrowIfFailed(computeCommandList->Close());
	ID3D12CommandList* ppCommandLists[] = { computeCommandList };

	PIXBeginEvent(computeCommandQueue, 0, L"compute particle");
	computeCommandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);
	PIXEndEvent(computeCommandQueue);

	uint64_t fenceValue = m_ParticleComputeFenceValue++;
	ThrowIfFailed(computeCommandQueue->Signal(computeFence, fenceValue));
	ThrowIfFailed(computeFence->SetEventOnCompletion(fenceValue, m_ComputeEventHandle));
	WaitForSingleObject(m_ComputeEventHandle, INFINITE);

	m_SrvIndex = 1 - m_SrvIndex;

	ThrowIfFailed(computeCommandAlloc->Reset());
	ThrowIfFailed(computeCommandList->Reset(computeCommandAlloc, m_pPsoCompute));
}

void Particle::Render()
{
	auto frameResource = FrameResource::GetCurrentFrameResource();
	auto commandList = Graphics::GetCommandList();

	if (m_IsWireframe) commandList->SetPipelineState(m_pPsoWireframeDraw);
	else commandList->SetPipelineState(m_pPsoDraw);

	commandList->SetGraphicsRootSignature(m_pRootSignatureDraw);

	//set root parameters of particle draw root signature
	auto particleDrawCB = frameResource->ParticleDrawCB->Resource();
	commandList->SetGraphicsRootConstantBufferView(ParticleDrawRootParameters::ParticleDrawCBV,
		particleDrawCB->GetGPUVirtualAddress());

	ID3D12DescriptorHeap* particleHeaps[] = { m_pDescriptorHeap };
	commandList->SetDescriptorHeaps(_countof(particleHeaps), particleHeaps);

	commandList->IASetVertexBuffers(0, 1, &m_ParticleVBuffer.ParticleVertexBufferView);
	commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);

	const UINT srvIndex = (m_SrvIndex == 1) ?
		ParticleDescriptorHeapIndex::ParticleSrv0 :
		ParticleDescriptorHeapIndex::ParticleSrv1;

	CD3DX12_GPU_DESCRIPTOR_HANDLE srvHandle(
		m_pDescriptorHeap->GetGPUDescriptorHandleForHeapStart(),
		srvIndex, Graphics::g_DescriptorSizeCbvSrvUav);

	commandList->SetGraphicsRootDescriptorTable(ParticleDrawRootParameters::ParticleDrawSRVTable, srvHandle);

	//use pix for debug
	PIXBeginEvent(commandList, 0, L"begin particle render");
	commandList->DrawInstanced(m_ParticleCount, 1, 0, 0);
	PIXEndEvent(commandList);
}

void Particle::Finalize()
{
}

void Particle::BuildParticleDatasGPU(DescriptorHeapsManage& descriptorHeaps)
{
	//intialize the particle datas on GPU 
	auto device = Graphics::GetDevice();
	auto commandList = Graphics::GetCommandList();

	//create the descriptor heap
	D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
	heapDesc.NumDescriptors = static_cast<UINT>(ParticleDescriptorCount);
	heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	ThrowIfFailed(device->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(descriptorHeaps[DHeapParticles].GetAddressOf())));


	//intialize vertex buffer
	{
		const size_t particleVertexByteSize = m_ParticleCount * sizeof(ParticleVertex);

		m_ParticleVBuffer.ParticleVertexResource =
			D3DUtility::CreateGPUBuffer(device, commandList,
				m_ParticleVertices.data(), m_ParticleVertices.size() * sizeof(ParticleVertex),
				m_ParticleVBuffer.ParticleVertexResourceUpload);

		m_ParticleVBuffer.ParticleVertexBufferView.BufferLocation =
			m_ParticleVBuffer.ParticleVertexResource->GetGPUVirtualAddress();
		m_ParticleVBuffer.ParticleVertexBufferView.SizeInBytes = static_cast<UINT>(particleVertexByteSize);
		m_ParticleVBuffer.ParticleVertexBufferView.StrideInBytes = sizeof(ParticleVertex);
	}

	//initialize the descriptor heap of particle data
	{
		m_ParticleDataBuffers.resize(2);

		const size_t particleDataByteSize = m_ParticleCount * sizeof(ParticleData);
		//create particle velocity and position resource for SRV and UAV buffer0(1/2)

		for (size_t i = 0; i < m_ParticleDataBuffers.size(); i++)
		{
			m_ParticleDataBuffers[i].ParticleDataResource = D3DUtility::CreateGPUBuffer(
				device, commandList,
				m_ParticleDatas.data(),
				particleDataByteSize,
				m_ParticleDataBuffers[i].ParticleDataResourceUpload,
				D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);

			commandList->ResourceBarrier(1,
				&CD3DX12_RESOURCE_BARRIER::Transition(m_ParticleDataBuffers[i].ParticleDataResource.Get(),
					D3D12_RESOURCE_STATE_GENERIC_READ, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE));

		}

		//
		D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
		srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		srvDesc.Format = DXGI_FORMAT_UNKNOWN;
		srvDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;	//dimension the view as a buffer
		srvDesc.Buffer.FirstElement = 0;
		srvDesc.Buffer.NumElements = static_cast<UINT>(m_ParticleCount);
		srvDesc.Buffer.StructureByteStride = sizeof(ParticleData);
		srvDesc.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_NONE;

		int srvIndexForInit = ParticleSrv0;
		for (size_t i = 0; i < m_ParticleDataBuffers.size(); i++)
		{
			CD3DX12_CPU_DESCRIPTOR_HANDLE srvHandle0(descriptorHeaps[DHeapParticles]->GetCPUDescriptorHandleForHeapStart(),
				srvIndexForInit++, Graphics::g_DescriptorSizeCbvSrvUav);
			device->CreateShaderResourceView(m_ParticleDataBuffers[i].ParticleDataResource.Get(), &srvDesc, srvHandle0);
		}

		D3D12_UNORDERED_ACCESS_VIEW_DESC uavDesc = {};
		uavDesc.Format = DXGI_FORMAT_UNKNOWN;
		uavDesc.ViewDimension = D3D12_UAV_DIMENSION_BUFFER;
		uavDesc.Buffer.FirstElement = 0;
		uavDesc.Buffer.NumElements = static_cast<UINT>(m_ParticleCount);
		uavDesc.Buffer.StructureByteStride = sizeof(ParticleData);
		uavDesc.Buffer.CounterOffsetInBytes = 0;
		uavDesc.Buffer.Flags = D3D12_BUFFER_UAV_FLAG_NONE;

		int uavIndexForInit = ParticleUav0;
		for (size_t i = 0; i < m_ParticleDataBuffers.size(); i++)
		{
			CD3DX12_CPU_DESCRIPTOR_HANDLE uavHandle0(descriptorHeaps[DHeapParticles]->GetCPUDescriptorHandleForHeapStart(),
				uavIndexForInit++, Graphics::g_DescriptorSizeCbvSrvUav);
			device->CreateUnorderedAccessView(m_ParticleDataBuffers[i].ParticleDataResource.Get(), nullptr, &uavDesc, uavHandle0);
		}
	}

	m_pDescriptorHeap = descriptorHeaps[DHeapParticles].Get();
}

void Particle::BuildParticleRootSignature(RootSignaturesManage& rootSignatures)
{
	auto device = Graphics::GetDevice();
	//if support version1.1?
	D3D12_FEATURE_DATA_ROOT_SIGNATURE featureData = Graphics::GetRootSigFeature();

	D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags;
	// Serialize the root signature.
	ComPtr<ID3DBlob> rootSignatureBlob;
	ComPtr<ID3DBlob> errorBlob;

	//create root signature for particles draw
	///name :particleDrawRS
	{
		rootSignatureFlags =
			D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS;

		CD3DX12_DESCRIPTOR_RANGE1 partcleDrawTableRange[1];
		partcleDrawTableRange[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0, 0, D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC);
		//descriptor range flag for driver optimization

		CD3DX12_ROOT_PARAMETER1 particleDrawRootParameters[ParticleDrawRootParameters::ParticleDrawRootParametersCount];
		particleDrawRootParameters[ParticleDrawRootParameters::ParticleDrawCBV].InitAsConstantBufferView(0, 0,
			D3D12_ROOT_DESCRIPTOR_FLAG_DATA_STATIC, D3D12_SHADER_VISIBILITY_ALL);
		particleDrawRootParameters[ParticleDrawRootParameters::ParticleDrawSRVTable].InitAsDescriptorTable(
			_countof(partcleDrawTableRange), partcleDrawTableRange, D3D12_SHADER_VISIBILITY_VERTEX);

		CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc;
		rootSignatureDesc.Init_1_1(_countof(particleDrawRootParameters),
			particleDrawRootParameters, 0, nullptr, rootSignatureFlags);

		rootSignatureBlob.Reset();
		errorBlob.Reset();
		ThrowIfFailed(D3DX12SerializeVersionedRootSignature(&rootSignatureDesc,
			featureData.HighestVersion, rootSignatureBlob.GetAddressOf(), errorBlob.GetAddressOf()));

		ThrowIfFailed(device->CreateRootSignature(0,
			rootSignatureBlob->GetBufferPointer(),
			rootSignatureBlob->GetBufferSize(),
			IID_PPV_ARGS(rootSignatures[RootSigParticleDraw].GetAddressOf())));

		m_pRootSignatureDraw = rootSignatures[RootSigParticleDraw].Get();

	}

	//particle compute shader root signature
	{
		CD3DX12_DESCRIPTOR_RANGE1 particleCSTableRanges[2];
		particleCSTableRanges[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0, 0, D3D12_DESCRIPTOR_RANGE_FLAG_DESCRIPTORS_VOLATILE);
		//note the flag:D3D12_DESCRIPTOR_RANGE_FLAG_DESCRIPTORS_VOLATILE
		//for UVA:D3D12_DESCRIPTOR_RANGE_FLAG_DATA_VOLATILE: Descriptors are staticand the data is volatile.
		//for SRV:D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC_WHILE_SET_AT_EXECUTE:	Descriptors are staticand data is static while set at execute.
		particleCSTableRanges[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 1, 0, 0, D3D12_DESCRIPTOR_RANGE_FLAG_DATA_VOLATILE);

		CD3DX12_ROOT_PARAMETER1 particleCSRootParameters[ParticleCSRootParameters::ParticleCSParametersCount];
		particleCSRootParameters[ParticleCSRootParameters::ParticleCSCBV].InitAsConstantBufferView(0, 0, D3D12_ROOT_DESCRIPTOR_FLAG_DATA_STATIC);
		particleCSRootParameters[ParticleCSRootParameters::ParticleCSSRVTable].InitAsDescriptorTable(1, &particleCSTableRanges[0]);
		particleCSRootParameters[ParticleCSRootParameters::ParticleCSUAVTable].InitAsDescriptorTable(1, &particleCSTableRanges[1]);

		CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc;
		rootSignatureDesc.Init_1_1(_countof(particleCSRootParameters), particleCSRootParameters, 0, nullptr);

		rootSignatureBlob.Reset();
		errorBlob.Reset();
		ThrowIfFailed(D3DX12SerializeVersionedRootSignature(&rootSignatureDesc,
			featureData.HighestVersion, rootSignatureBlob.GetAddressOf(), errorBlob.GetAddressOf()));

		ThrowIfFailed(device->CreateRootSignature(0,
			rootSignatureBlob->GetBufferPointer(),
			rootSignatureBlob->GetBufferSize(),
			IID_PPV_ARGS(rootSignatures[RootSigParticleCS].GetAddressOf())));

		m_pRootSigCompute = rootSignatures[RootSigParticleCS].Get();
	}
}

void Particle::BuildParticlePSO(PSOsManage& PSOs,ShadersManage& shaders, InputLayoutsManage& inputLayouts)
{
	auto device = Graphics::GetDevice();
	auto backBufferFormat = Graphics::GetBackBufferFormat();
	auto depthStencilFormat = Graphics::GetDepthStencilFormat();
	//create particle pso
	//watch out if the Access violation reading error happended,check out if the size right
	{
		struct GraphicsPSOStream
		{
			CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE pRootSignature;
			CD3DX12_PIPELINE_STATE_STREAM_INPUT_LAYOUT InputLayout;
			CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
			CD3DX12_PIPELINE_STATE_STREAM_VS VS;
			CD3DX12_PIPELINE_STATE_STREAM_GS GS;
			CD3DX12_PIPELINE_STATE_STREAM_PS PS;
			CD3DX12_PIPELINE_STATE_STREAM_BLEND_DESC Blend;
			CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL DepthStencil;
			CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL_FORMAT DSVFormat;
			CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
			CD3DX12_PIPELINE_STATE_STREAM_RASTERIZER Rasterizer;
		} drawPsoStream;

		CD3DX12_BLEND_DESC blendDesc(D3D12_DEFAULT);
		blendDesc.RenderTarget[0].BlendEnable = TRUE;
		blendDesc.RenderTarget[0].LogicOpEnable = FALSE;
		blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
		blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
		blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
		blendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
		blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

		CD3DX12_DEPTH_STENCIL_DESC depthStencilDesc(D3D12_DEFAULT);
		depthStencilDesc.DepthEnable = TRUE;
		depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ZERO;	//if not setting this ,will have square mask (bug

		D3D12_RT_FORMAT_ARRAY rtvFormats = {};
		rtvFormats.NumRenderTargets = 1;
		rtvFormats.RTFormats[0] = backBufferFormat;

		drawPsoStream.pRootSignature = m_pRootSignatureDraw;
		drawPsoStream.InputLayout = { inputLayouts[InputLayoutColor]->data(), static_cast<UINT>(inputLayouts[InputLayoutColor]->size()) };
		drawPsoStream.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT;
		drawPsoStream.VS = CD3DX12_SHADER_BYTECODE(shaders[ShaderParticleVS].Get());
		drawPsoStream.GS = CD3DX12_SHADER_BYTECODE(shaders[ShaderParticleGS].Get());
		drawPsoStream.PS = CD3DX12_SHADER_BYTECODE(shaders[ShaderParticlePS].Get());
		drawPsoStream.Blend = blendDesc;
		drawPsoStream.DepthStencil = depthStencilDesc;
		drawPsoStream.DSVFormat = depthStencilFormat;
		drawPsoStream.RTVFormats = rtvFormats;
		drawPsoStream.Rasterizer = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);

		D3D12_PIPELINE_STATE_STREAM_DESC psoParticleDrawDesc = { sizeof(GraphicsPSOStream), &drawPsoStream };
		ThrowIfFailed(device->CreatePipelineState(&psoParticleDrawDesc, IID_PPV_ARGS(PSOs[PSOParticleDraw].GetAddressOf())));

		m_pPsoDraw = PSOs[PSOParticleDraw].Get();

		//create wireframe pso
		CD3DX12_RASTERIZER_DESC wareframeRtDesc(D3D12_DEFAULT);
		wareframeRtDesc.FillMode = D3D12_FILL_MODE_WIREFRAME;
		wareframeRtDesc.CullMode = D3D12_CULL_MODE_NONE;
		drawPsoStream.Rasterizer = wareframeRtDesc;

		D3D12_PIPELINE_STATE_STREAM_DESC psoWireframeDrawDesc = { sizeof(GraphicsPSOStream), &drawPsoStream };
		ThrowIfFailed(device->CreatePipelineState(&psoWireframeDrawDesc, IID_PPV_ARGS(PSOs[PSOParticleWireframeDraw].GetAddressOf())));

		m_pPsoWireframeDraw = PSOs[PSOParticleWireframeDraw].Get();

	}

	//particle cs pso
	{
		D3D12_COMPUTE_PIPELINE_STATE_DESC particleComputeDesc = {};
		particleComputeDesc.pRootSignature = m_pRootSigCompute;
		particleComputeDesc.CS = CD3DX12_SHADER_BYTECODE(shaders[ShaderParticleCS].Get());

		ThrowIfFailed(device->CreateComputePipelineState(&particleComputeDesc, IID_PPV_ARGS(PSOs[PSOParticleCompute].GetAddressOf())));

		m_pPsoCompute = PSOs[PSOParticleCompute].Get();
	}
}
