//camera class 
//zhang ruisong
#pragma once
#include "IObject.h"

class Camera : implements IObject
{
public:
	Camera(uint32_t displayWidth = 1920, uint32_t displayHeight = 1080,
		Vector3 position = { 10.0f, 15.0f, 0.0f },
		Vector3 focus = { 0.0f, 0.0f, 0.0f },
		Vector3 front = { 0.0f, 0.0f, 1.0f },
		Vector3 right = { 1.0f, 0.0f, 0.0f },
		float Fov = 60.0f, float znear = 0.1f, float zfar = 13000.0f);
	~Camera();

	virtual void Initialize() override;
	virtual void Update(double dt) override;
	virtual void Compute() override;
	virtual void Render() override;
	virtual void Finalize() override;

	void Resize(uint32_t width, uint32_t height);
	void Reset(Vector3 position = { 0.0f, 3.0f, -10.0f },
		Vector3 focus = { 0.0f, 0.0f, 0.0f },
		Vector3 front = { 0.0f, 0.0f, 1.0f },
		Vector3 right = { 1.0f, 0.0f, 0.0f });

	void MoveForword(float units);
	void Strafe(float units);
	void MoveUp(float units);

	void Yaw(float radians);
	void Pitch(float radians);
	void Roll(float radians);
	void RotateY(float radians);

	void SetYmovement(bool isYmovement);

	const Matrix GetViewMatrix() const { return m_viewMatrix; }
	const Matrix GetProjectionMatrix() const { return m_projMatrix; }
	const Vector3 GetEyePosition() const { return m_position; }
	const Vector3 GetFocusPosition() const { return m_focus; }
	const float GetClippingNear() const { return m_near; }
	const float GetClippingFar() const { return m_far; }

private:
	Matrix m_viewMatrix;
	Matrix m_projMatrix;

	Vector3 m_position;
	Vector3 m_focus;
	Vector3 m_up;
	Vector3 m_right;
	Vector3 m_front;
	Vector3 m_velocity;
	const float m_maxVelocity = 2.0f;

	uint32_t m_clientWidth;
	uint32_t m_clientHeight;

	float m_fov;
	float m_aspect;

	float m_near;
	float m_far;

	bool m_bYmovement;
};