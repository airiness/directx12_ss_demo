#include "pch.h"
#include "WindowApplication.h"
#include "DirectX12App.h"
#include "Keyboard.h"
#include "Mouse.h"

#include "imgui.h"
#include "imgui_impl_win32.h"

HWND WindowApplication::m_hWnd = nullptr;
bool WindowApplication::m_bIsQuit = false;
bool WindowApplication::m_fullscreenMode = false;
RECT WindowApplication::m_windowRect;
std::unique_ptr<Input::Keyboard> WindowApplication::m_pKeyboard = nullptr;
std::unique_ptr<Input::Mouse> WindowApplication::m_pMouse = nullptr;


int WindowApplication::Run(DirectX12App* d3d12app, HINSTANCE hInstance, int nCmdShow)
{
	WNDCLASSEX wc = { 0 };
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WindowProc;
	wc.hInstance = hInstance;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.lpszClassName = L"ssDemo";
	RegisterClassEx(&wc);

	RECT windowRect = { 0, 0, static_cast<LONG>(d3d12app->GetWidth()),static_cast<LONG>(d3d12app->GetHeight()) };
	AdjustWindowRect(&windowRect, m_windowStyle, FALSE);

	m_hWnd = CreateWindow(
		wc.lpszClassName,
		d3d12app->GetTitle(),
		m_windowStyle,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		windowRect.right - windowRect.left,
		windowRect.bottom - windowRect.top,
		nullptr,	//no parent window
		nullptr,	//no menus
		hInstance,
		d3d12app); //for Retrieving the DirectX App Pointer

	//keyboard and mouse
	{
		m_pKeyboard = std::make_unique<Input::Keyboard>();
		m_pMouse = std::make_unique<Input::Mouse>();
		m_pMouse->SetWindow(m_hWnd);
	}

	//DirectX12 application initialize
	d3d12app->Initialize();
	ShowWindow(m_hWnd, nCmdShow);

	//msg loop
	MSG msg = {};
	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	d3d12app->Finalize();

	return 0;
}

void WindowApplication::ToggleFullscreenWindow(IDXGISwapChain* swapchain)
{
	if (m_fullscreenMode)
	{
		//restore window's attributes and size
		SetWindowLong(m_hWnd, GWL_STYLE, m_windowStyle);
			
			//HWND_NOTOPMOST
			//Places the window above all non-topmost windows (that is, behind all topmost windows). 
			//This flag has no effect if the window is already a non-topmost window.
		SetWindowPos(m_hWnd, HWND_NOTOPMOST,
			m_windowRect.left, m_windowRect.top,
			m_windowRect.right - m_windowRect.left,
			m_windowRect.bottom - m_windowRect.top,
			SWP_FRAMECHANGED | SWP_NOACTIVATE);

		ShowWindow(m_hWnd, SW_NORMAL);
	}
	else
	{
		//save the old window rect
		GetWindowRect(m_hWnd, &m_windowRect);

		//boardless window can fill the screen
		SetWindowLong(m_hWnd, GWL_STYLE, m_windowStyle & ~(WS_CAPTION | WS_MAXIMIZEBOX | WS_MINIMIZEBOX | WS_SYSMENU | WS_THICKFRAME));

		RECT fullscreenWindowRect;
		try
		{
			if (swapchain)
			{
				// Get the settings of the display on which the app's window is currently displayed
				ComPtr<IDXGIOutput> pOutput;
				ThrowIfFailed(swapchain->GetContainingOutput(&pOutput));
				DXGI_OUTPUT_DESC Desc;
				ThrowIfFailed(pOutput->GetDesc(&Desc));
				fullscreenWindowRect = Desc.DesktopCoordinates;
			}
			else
			{
				// Fallback to EnumDisplaySettings implementation
				throw HrException(S_FALSE);
			}
		}
		catch (HrException& e)
		{
			UNREFERENCED_PARAMETER(e);

			// Get the settings of the primary display
			//The DEVMODE data structure contains information about 
			//the initialization and environment of a printer or a display device.
			DEVMODE devMode = {};
			devMode.dmSize = sizeof(DEVMODE);
			EnumDisplaySettings(nullptr, ENUM_CURRENT_SETTINGS, &devMode);

			fullscreenWindowRect = {
				devMode.dmPosition.x,
				devMode.dmPosition.y,
				devMode.dmPosition.x + static_cast<LONG>(devMode.dmPelsWidth),
				devMode.dmPosition.y + static_cast<LONG>(devMode.dmPelsHeight)
			};
		}

		SetWindowPos(
			m_hWnd,
			HWND_TOPMOST,
			fullscreenWindowRect.left,
			fullscreenWindowRect.top,
			fullscreenWindowRect.right,
			fullscreenWindowRect.bottom,
			SWP_FRAMECHANGED | SWP_NOACTIVATE);


		ShowWindow(m_hWnd, SW_MAXIMIZE);
	}

	m_fullscreenMode = !m_fullscreenMode;
}

//imgui proc function
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
//main windows proc function
LRESULT WindowApplication::WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//Retrieving the App Pointer
	DirectX12App* D3D12App = reinterpret_cast<DirectX12App*>(GetWindowLongPtr(hWnd, GWLP_USERDATA));

	if (ImGui_ImplWin32_WndProcHandler(hWnd, message, wParam, lParam))
		return true;

	switch (message)
	{
	case WM_CREATE:
	{
		//Save the DirectX12 App pointer passed into CreateWindow
		LPCREATESTRUCT pCreateStruct = reinterpret_cast<LPCREATESTRUCT>(lParam);
		SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pCreateStruct->lpCreateParams));
	}
	return 0;
	case WM_ACTIVATEAPP:
		Input::Keyboard::ProcessMessage(message, wParam, lParam);
		Input::Mouse::ProcessMessage(message, wParam, lParam);
		break;
	case WM_SIZE:
		if (D3D12App)
		{
			//set the window bounds
			RECT windowRect = {};
			GetWindowRect(hWnd, &windowRect);
			D3D12App->SetWindowBounds(windowRect.left, windowRect.top, windowRect.right, windowRect.bottom);

			//set the client rect
			RECT clientRect = {};
			GetClientRect(hWnd, &clientRect);

			//auto width = (clientRect.right - clientRect.left);
			//auto height = (windowRect.bottom - windowRect.top);
			D3D12App->Resize(UINT64_MAX, wParam == SIZE_MINIMIZED);
			//UINT64_MAX: dont change the resolution
		}
		return 0;

	case WM_MOVE:
		if (D3D12App)
		{
			//set the window bounds
			RECT windowRect = {};
			GetWindowRect(hWnd, &windowRect);
			D3D12App->SetWindowBounds(windowRect.left, windowRect.top, windowRect.right, windowRect.bottom);


			int xPos = (int)(short)LOWORD(lParam);
			int yPos = (int)(short)HIWORD(lParam);
			D3D12App->WindowMoved(xPos, yPos);
		}
		return 0;
	case WM_PAINT:
		if (D3D12App)
		{
			std::wstring titleText = D3D12App->GetTitle();
			titleText += std::to_wstring(D3D12App->GetWidth()) + L"x" + std::to_wstring(D3D12App->GetHeight());
			auto deltaTime = CalculateFrameState(titleText);
			D3D12App->Update(deltaTime);
			D3D12App->Render();
		}
		return 0;
	case WM_SYSKEYUP:
		break;
	case WM_SYSKEYDOWN:
		if ((wParam == VK_RETURN) &&(lParam & (1<<29)))
		{
			if (D3D12App && D3D12App->GetTearingSupport())
			{
				ToggleFullscreenWindow(D3D12App->GetSwapchain());
				return 0;
			}
		}
		break;
	case WM_KEYDOWN:
	case WM_KEYUP:
		Input::Keyboard::ProcessMessage(message, wParam, lParam);
		break;
	case WM_INPUT:
	case WM_MOUSEMOVE:
	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
	case WM_MBUTTONDOWN:
	case WM_MBUTTONUP:
	case WM_MOUSEWHEEL:
	case WM_XBUTTONDOWN:
	case WM_XBUTTONUP:
	case WM_MOUSEHOVER:
		Input::Mouse::ProcessMessage(message, wParam, lParam);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	default:
		break;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

double WindowApplication::CalculateFrameState(const std::wstring& titleText)
{
	static uint64_t frameCounter = 0;
	static double elapsedSeconds = 0.0;
	static std::chrono::high_resolution_clock clock;
	static auto t0 = clock.now();
	frameCounter++;
	auto t1 = clock.now();
	auto deltaTimeNoSeconds = t1 - t0;
	t0 = t1;

	auto deltaTime = deltaTimeNoSeconds.count() * 1e-9;
	elapsedSeconds += deltaTime;

	if (elapsedSeconds > 0.1)
	{
		auto fps = frameCounter / elapsedSeconds;
		auto mspf = 1000.0f / fps;

		std::wstring window_text = titleText +
			L"--ZRS   fps: " + std::to_wstring(fps) +
			L"        mspf: " + std::to_wstring(mspf);
		::SetWindowText(m_hWnd, window_text.c_str());
		frameCounter = 0;
		elapsedSeconds = 0.0;
	}
	return deltaTime;
}

bool WindowApplication::IsDone()
{
	auto kb = m_pKeyboard->GetState();
	if (kb.Escape)
	{
		m_bIsQuit = true;
	}
	return m_bIsQuit;
}
