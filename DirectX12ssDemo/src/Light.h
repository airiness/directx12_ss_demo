#pragma once
#include "RenderItem.h"
#include "IObject.h"

enum LightingRootParameters : uint32_t
{
	LightingTextureSRVTable = 0,
	LightingObjectCBV,
	LightingPassCBV,
	LightingMaterialCBV,
	LightingDataSRV,
	LightingRootParametersCount
};

enum class LightType : uint32_t
{
	DirectionLight = 0,
	PointLight,
	SpotLight,
	LightTypeNum
};

struct LightData
{
	Vector3 Strength = { 0.5f,0.5f,0.5f };		//
	float FalloffStart = 1.0f;					//16bytes
	Vector3 Direction = { 0.0f,-1.0f,0.0f };	//
	float FalloffEnd = 10.0f;					//16bytes
	Vector3 Position = { 0.0f,0.0f,0.0f };		//
	float SpotPower = 64.0f;					//16bytes
	uint32_t IsLightUse = 0;
	uint32_t Padding[3] = { 0,0,0 };			//16bytes
	//light 16 x 4bytes
};

//struct light info for light control
struct LightInfo
{
	bool IsLightUse = false;
	Vector3 lightPosition = { 0.0f,0.0f,0.0f };
	LightType lightType;
	bool bShowInfoWindow = false;
};

class Lights : implements IObject
{
public:
	Lights();
	Lights(const Lights&) = delete;
	~Lights();

	virtual void Initialize() override;
	virtual void Update(double dt) override;
	virtual void Compute() override;
	virtual void Render() override;
	virtual void Finalize() override;

	void BuildLightInfoRenderItems(RenderItem::RenderItemsManage& renderItems,size_t& objectIndex);
	void BuildLightPSO(PSOsManage& PSOs, ShadersManage& shaders, InputLayoutsManage& inputLayouts);
	void BuildLightRootSignature(RootSignaturesManage& rootSignatures);

	const size_t kDirectionLightCount = 3;
	const size_t kPointLightCount = 8;
	const size_t kSpotLightCount = 6;
	const size_t kMaxLightCount =
		kDirectionLightCount +
		kPointLightCount +
		kSpotLightCount;
	std::vector<LightInfo> LightInfos;
	std::vector<LightData> LightDatas;
	int LightDirtyFlag = 0;
	bool bShowLightSettings = false;
	std::vector<RenderItem*> LightsInfoRenderItems;
	static bool bLightIsShow;

private:
	ID3D12RootSignature* m_pLightRootSignature = nullptr;
	ID3D12PipelineState* m_pLightPsoDefault = nullptr;
	ID3D12PipelineState* m_pLightPsoWireFrame = nullptr;
	ID3D12PipelineState* m_pLightPsoTransparent = nullptr;
	ID3D12PipelineState* m_pLightPsoBloom = nullptr;
	RenderItem::RenderItemsManage* pRenderItemsManage = nullptr;
};