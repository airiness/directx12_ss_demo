//rigidbody  class
//the cpu end of manage rigidbodies
//zhang ruisong

#include "pch.h"
#include "WindowApplication.h"
#include "DirectX12Graphics.h"
#include "RigidBody.h"

#define HLSL_TRUE ~0
#define HLSL_FALSE 0

void RigidBody::Initialize()
{
	auto device = Graphics::GetDevice();

	//initialize command allocator and fence;
	ThrowIfFailed(device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_COMPUTE, IID_PPV_ARGS(m_computeCommandAlloc.GetAddressOf())));
	NAME_D3D12_OBJECT_TEXT(m_computeCommandAlloc, L"RigidBodyComputeCommandAllocator");

	ThrowIfFailed(device->CreateFence(m_fenceValue, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(m_computeFence.GetAddressOf())));
	NAME_D3D12_OBJECT_TEXT(m_computeFence, L"RigidBodyComputeFence");

	//initialize the Gpu body datas
	m_CpuDatasCompute.resize(m_RigidBodyMaxCount);

	Vector3 beginPos = { 0.0f,0.0f,0.0f };
	Vector3 bodySize = { 1.0f,0.5f,1.0f };

	const uint32_t widthCount = 20;
	const uint32_t groundCount = 400;
	uint32_t pillarEachCount = 10;
	uint32_t pillarCount = 4;

	uint32_t widthSize = widthCount * bodySize.x;
	uint32_t depthSize = widthCount * bodySize.z;

	m_RigidBodyIndex = groundCount + pillarCount * pillarEachCount;
	m_RigidBodyDrawCount = m_RigidBodyIndex;

	auto createOneBody = [&](uint32_t index, const Vector3& position, const Vector3& bodySize,
		uint32_t isStatic, const Vector3& angularvel, const Vector3& linearvel = { 0.0f,0.0f,0.0f })->void {
			m_CpuDatasCompute[index].Position = position;
			m_CpuDatasCompute[index].BodySize = bodySize; // { randf(0.25f, 1.0f), randf(0.25f, 1.0f), randf(0.25f, 1.0f) };
			m_CpuDatasCompute[index].Mass = m_CpuDatasCompute[index].BodySize.x * m_CpuDatasCompute[index].BodySize.y * m_CpuDatasCompute[index].BodySize.z * m_density;
			m_CpuDatasCompute[index].InverseMass = 1.0f / m_CpuDatasCompute[index].Mass;
			m_CpuDatasCompute[index].Orientation = { 0.0f,0.0f,0.0f,1.0f };
			m_CpuDatasCompute[index].Velocity = linearvel;
			m_CpuDatasCompute[index].isContact = 0;
			m_CpuDatasCompute[index].AngularVelocity = angularvel;
			m_CpuDatasCompute[index].isStatic = isStatic;
			m_CpuDatasCompute[index].isUse = HLSL_TRUE;
			m_CpuDatasCompute[index].id = index;
	};

	uint32_t index = groundCount;

	for (size_t i = 0; i < groundCount; i++)
	{
		beginPos.x = i / widthCount * bodySize.x * 2.0f - widthSize; // because body size is half size
		beginPos.z = i % widthCount * bodySize.z * 2.0f - depthSize;
		createOneBody(i, beginPos, bodySize, HLSL_TRUE, Vector3(0.0f, 0.0f, 0.0f));
		if (i == 0 || i == (widthCount - 1) || i == (widthCount * (widthCount - 1)) || i == (widthCount * widthCount - 1))
		{
			Vector3 offset = { 0.0f,bodySize.y * 2.0f,0.0f };;
			for (size_t j = 0; j < pillarEachCount; j++)
			{
				createOneBody(index, beginPos + offset, bodySize, HLSL_TRUE, Vector3(0.0f, 0.0f, 0.0f));
				offset.y += bodySize.y * 2.0f;
				index++;
			}
		}
	}

	for (size_t i = m_RigidBodyIndex; i < m_RigidBodyMaxCount; i++)
	{
		createOneBody(i, Vector3(randf(-m_bodySpread, m_bodySpread), randf(-m_bodySpread, m_bodySpread), randf(-m_bodySpread, m_bodySpread)),
			Vector3(randf(0.5f, 1.5f), randf(0.5f, 1.5f), randf(0.5f, 1.5f)), 0,
			Vector3(randf(-2.0f, 2.0f), randf(-2.0f, 2.0f), randf(-2.0f, 2.0f)));
	}

	m_computeConstantsBufferEdit.bodyCount = m_RigidBodyMaxCount;
	m_computeConstantsBufferEdit.bodyIndex = m_RigidBodyIndex;
	m_computeConstantsBufferEdit.threadGroupCount = static_cast<uint32_t>(ceil(m_RigidBodyMaxCount / static_cast<float>(m_RigidBodyGpuGroupThreadCount)));
	m_computeConstantsBufferEdit.bodyDensity = m_density;
	m_computeConstantsBufferEdit.gravity = { 0.0f, -9.8f, 0.0f };
	m_computeConstantsBufferEdit.isDebugMode = m_IsDebugMode ? HLSL_TRUE : HLSL_FALSE;
	m_computeConstantsBufferEdit.isAllActive = m_IsActiveAll ? HLSL_TRUE : HLSL_FALSE;
	m_computeConstantsBufferEdit.spread = m_bodySpread;
	m_computeConstantsBufferEdit.isCenterMode = m_IsCenterMode ? HLSL_TRUE : HLSL_FALSE;
	m_computeConstantsBufferEdit.resetFlag = 0;

	//initialize the constant buffers of compute pipeline
	m_computeConstantResource = std::make_unique<UploadBuffer<RigidBodyComputeConstants>>(device, 1, true);

	//initialize constant buffer of draw pipeline
	for (size_t i = 0; i < Graphics::FrameCount; i++)
	{
		m_drawConstantResources.push_back(std::move(std::make_unique<UploadBuffer<RigidBodyDrawConstants>>(device, 1, true)));
	}
}

void RigidBody::Update(double dt)
{
	auto kb = WindowApplication::m_pKeyboard->GetState();
	auto mouse = WindowApplication::m_pMouse->GetState();
	static Input::Mouse::ButtonStateTracker mouseTracker;
	static Input::Keyboard::KeyboardStateTracker keyTracer;
	mouseTracker.Update(mouse);
	keyTracer.Update(kb);

	int bAddBody = HLSL_FALSE;
	int bReset = HLSL_FALSE;
	if (keyTracer.pressed.R)
	{
		bReset = HLSL_TRUE;
	}
	if (keyTracer.pressed.D2)
	{
		m_IsActiveAll = !m_IsActiveAll;
	}
	if (keyTracer.pressed.D3)
	{
		m_IsCenterMode = !m_IsCenterMode;
	}
	if (keyTracer.pressed.D4)
	{
		m_IsRendering = !m_IsRendering;
	}

	if (mouseTracker.leftButton == Input::Mouse::ButtonStateTracker::PRESSED && m_RigidBodyIndex < m_RigidBodyMaxCount)
	{
		m_RigidBodyIndex++;
		if (m_RigidBodyDrawCount < m_RigidBodyMaxCount)
		{
			m_RigidBodyDrawCount = m_RigidBodyIndex;
		}
		bAddBody = HLSL_TRUE;
	}
	else if (m_RigidBodyIndex == m_RigidBodyMaxCount)
	{
		m_RigidBodyIndex = m_rRgidBodyStaticLimit;
	}

	//update cubes move for debug
	//'~0' is true;
	uint32_t moveFlag[8] = { HLSL_FALSE,HLSL_FALSE,HLSL_FALSE,HLSL_FALSE,HLSL_FALSE,HLSL_FALSE,HLSL_FALSE,HLSL_FALSE };
	if (kb.G)//front +z
	{
		moveFlag[0] = HLSL_TRUE;
	}
	if (kb.B)//back -z
	{
		moveFlag[1] = HLSL_TRUE;
	}
	if (kb.V)//left -x
	{
		moveFlag[2] = HLSL_TRUE;
	}
	if (kb.N)//right +x
	{
		moveFlag[3] = HLSL_TRUE;
	}
	if (kb.F)//up +y
	{
		moveFlag[4] = HLSL_TRUE;
	}
	if (kb.C)//down -y
	{
		moveFlag[5] = HLSL_TRUE;
	}

	uint32_t rotateFlag[8] = { HLSL_FALSE,HLSL_FALSE,HLSL_FALSE,HLSL_FALSE,HLSL_FALSE,HLSL_FALSE,HLSL_FALSE,HLSL_FALSE };

	if (kb.D5)//+x
	{
		rotateFlag[0] = HLSL_TRUE;
	}
	if (kb.D6)//-x
	{
		rotateFlag[1] = HLSL_TRUE;
	}
	if (kb.D7)//+y
	{
		rotateFlag[2] = HLSL_TRUE;
	}
	if (kb.D8)//-y
	{
		rotateFlag[3] = HLSL_TRUE;
	}
	if (kb.D9)//+z
	{
		rotateFlag[4] = HLSL_TRUE;
	}
	if (kb.D0)//-z
	{
		rotateFlag[5] = HLSL_TRUE;
	}

	if (kb.D2)//speed mode flag
	{
		rotateFlag[7] = HLSL_TRUE;
	}
	//update compute constant buffer
	Vector3 eye = m_pCamera->GetEyePosition();
	Vector3 focus = m_pCamera->GetFocusPosition();
	Vector3 direction = focus - eye;
	direction.Normalize();

	m_computeConstantsBufferEdit.bodyIndex = m_RigidBodyIndex;
	m_computeConstantsBufferEdit.isCenterMode = m_IsCenterMode;
	m_computeConstantsBufferEdit.resetFlag = bReset;
	m_computeConstantsBufferEdit.gravityCenter = eye + direction * 150.0f;
	m_computeConstantsBufferEdit.isAddNewBody = bAddBody;
	m_computeConstantsBufferEdit.deltaTime = dt;

	//for bodies debugs move
	std::memcpy(m_computeConstantsBufferEdit.moveflags, moveFlag, sizeof(uint32_t) * 8);
	std::memcpy(m_computeConstantsBufferEdit.rotateflags, rotateFlag, sizeof(uint32_t) * 8);

	if (bAddBody)
	{
		m_computeConstantsBufferEdit.addBodyPosition = eye + direction * 4.0f;
		m_computeConstantsBufferEdit.addBodyVelocity = direction * randf(25.0f, 200.0f);// m_shootingSpeed;
		m_computeConstantsBufferEdit.addBodyAngularVelocity = { randf(-2.0f,2.0f),randf(-2.0f,2.0f),randf(-2.0f,2.0f) };
		m_computeConstantsBufferEdit.addBodySize = { randf(0.5f,1.5f),randf(0.5f,1.5f),randf(0.5f,1.5f) };// m_shootbodySize;
	}

	m_computeConstantResource->CopyAllData(&m_computeConstantsBufferEdit);

	//update renderer constant buffer
	Matrix viewMatrix = m_pCamera->GetViewMatrix();
	Matrix projectionMatrix = m_pCamera->GetProjectionMatrix();

	m_drawConstantsBufferEdit.WorldViewProjection = XMMatrixTranspose(XMMatrixMultiply(viewMatrix, projectionMatrix));
	m_drawConstantsBufferEdit.CameraPosition = m_pCamera->GetEyePosition();
	for (size_t i = 0; i < Graphics::FrameCount; i++)
	{
		m_drawConstantResources[i]->CopyAllData(&m_drawConstantsBufferEdit);
	}

}

void RigidBody::Compute()
{
	auto computeCommandList = Graphics::GetCommandList(D3D12_COMMAND_LIST_TYPE_COMPUTE);
	auto computeCommandQueue = Graphics::GetCommandQueue(D3D12_COMMAND_LIST_TYPE_COMPUTE);

	UINT srvComputeIndex;
	UINT uavComputeIndex;
	UINT uavDrawIndex = RigidBodyUav0 + Graphics::g_CurrentBackBuffer;

	ID3D12Resource* pSrvComputeResource;
	ID3D12Resource* pUavComputeResource;
	ID3D12Resource* pUavDrawResource;

	uint32_t computeResourceIndex = Graphics::FrameCount;
	if (m_srvIndex == 0)
	{
		srvComputeIndex = RigidBodySrvCompute0;
		uavComputeIndex = RigidBodyUavCompute1;
		pSrvComputeResource = m_GpuResources[computeResourceIndex].RigidBodyDataReource.Get();
		pUavComputeResource = m_GpuResources[computeResourceIndex + 1].RigidBodyDataReource.Get();
	}
	else
	{
		srvComputeIndex = RigidBodySrvCompute1;
		uavComputeIndex = RigidBodyUavCompute0;
		pSrvComputeResource = m_GpuResources[computeResourceIndex + 1].RigidBodyDataReource.Get();
		pUavComputeResource = m_GpuResources[computeResourceIndex].RigidBodyDataReource.Get();
	}

	pUavDrawResource = m_GpuResources[Graphics::g_CurrentBackBuffer].RigidBodyDataReource.Get();


	computeCommandList->SetPipelineState(m_pPsoCompute);
	computeCommandList->SetComputeRootSignature(m_pRootSigCompute);

	computeCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(pUavComputeResource,
		D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS));
	computeCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(pUavDrawResource,
		D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS));

	ID3D12DescriptorHeap* ppHeap = m_pDesciptorHeap;
	ID3D12DescriptorHeap* dHeaps[] = { ppHeap };
	computeCommandList->SetDescriptorHeaps(_countof(dHeaps), dHeaps);

	CD3DX12_GPU_DESCRIPTOR_HANDLE srvComputeHandle(ppHeap->GetGPUDescriptorHandleForHeapStart(), srvComputeIndex, Graphics::g_DescriptorSizeCbvSrvUav);
	CD3DX12_GPU_DESCRIPTOR_HANDLE uavComputeHandle(ppHeap->GetGPUDescriptorHandleForHeapStart(), uavComputeIndex, Graphics::g_DescriptorSizeCbvSrvUav);
	CD3DX12_GPU_DESCRIPTOR_HANDLE uavDrawHandle(ppHeap->GetGPUDescriptorHandleForHeapStart(), uavDrawIndex, Graphics::g_DescriptorSizeCbvSrvUav);

	computeCommandList->SetComputeRootConstantBufferView(RigidBodyComputeRootParaCBV,
		m_computeConstantResource->Resource()->GetGPUVirtualAddress());
	computeCommandList->SetComputeRootDescriptorTable(RigidBodyComputeRootParaSRVTable, srvComputeHandle);
	computeCommandList->SetComputeRootDescriptorTable(RigidBodyComputeRootParaUAVTable, uavComputeHandle);
	computeCommandList->SetComputeRootDescriptorTable(RigidBodyComputeRootParaDrawUAVTable, uavDrawHandle);

	computeCommandList->Dispatch(static_cast<UINT>(ceil(m_RigidBodyMaxCount / static_cast<float>(m_RigidBodyGpuGroupThreadCount))), 1, 1);

	computeCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(pUavComputeResource,
		D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE));
	computeCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(pUavDrawResource,
		D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE));

	ThrowIfFailed(computeCommandList->Close());
	ID3D12CommandList* ppCommandLists[] = { computeCommandList };

	PIXBeginEvent(computeCommandQueue, 0, L"compute rigidbody");
	computeCommandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);
	PIXEndEvent(computeCommandQueue);

	uint64_t fenceValue = m_fenceValue++;
	ThrowIfFailed(computeCommandQueue->Signal(m_computeFence.Get(), fenceValue));
	ThrowIfFailed(m_computeFence->SetEventOnCompletion(fenceValue, m_computeEventHandle));
	WaitForSingleObject(m_computeEventHandle, INFINITE);

	m_srvIndex = 1 - m_srvIndex;

	ThrowIfFailed(m_computeCommandAlloc->Reset());
	ThrowIfFailed(computeCommandList->Reset(m_computeCommandAlloc.Get(), m_pPsoCompute));
}

void RigidBody::Render()
{
	if (!m_IsRendering)
		return;

	auto device = Graphics::GetDevice();
	auto commandList = Graphics::GetCommandList();

	UINT srvDrawIndex = RigidBodySrv0 + Graphics::g_CurrentBackBuffer;

	if (m_IsWireframe) commandList->SetPipelineState(m_pPsoWireframeDraw);
	else commandList->SetPipelineState(m_pPsoDraw);

	commandList->SetGraphicsRootSignature(m_pRootSigDraw);

	commandList->SetGraphicsRootConstantBufferView(RigidBodyDrawRootParaCBV,
		m_drawConstantResources[Graphics::g_CurrentBackBuffer]->Resource()->GetGPUVirtualAddress());

	commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);

	ID3D12DescriptorHeap* dHeaps[] = { m_pDesciptorHeap };
	commandList->SetDescriptorHeaps(_countof(dHeaps), dHeaps);

	CD3DX12_GPU_DESCRIPTOR_HANDLE srvHandle(m_pDesciptorHeap->GetGPUDescriptorHandleForHeapStart(), srvDrawIndex, Graphics::g_DescriptorSizeCbvSrvUav);

	commandList->SetGraphicsRootDescriptorTable(RigidBodyDrawRootParaSRVTable, srvHandle);

	PIXBeginEvent(commandList, 0, L"RigidBody rendering");

	commandList->DrawInstanced(m_IsActiveAll ? m_RigidBodyMaxCount : m_RigidBodyDrawCount, 1, 0, 0);
	PIXEndEvent(commandList);

}

void RigidBody::Finalize()
{
}

void RigidBody::GetCamera(Camera* pCam)
{
	m_pCamera = pCam;
}

void RigidBody::BuildRigidBodyDatasGPU(DescriptorHeapsManage& descriptorHeaps)
{
	auto device = Graphics::GetDevice();
	auto commandList = Graphics::GetCommandList();

	//create descriptor heap for rigidbodys
	D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
	heapDesc.NumDescriptors = static_cast<UINT>(RigidBodyDescriptorCount);
	heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	ThrowIfFailed(device->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(descriptorHeaps[DHeapRigidBody].GetAddressOf())));

	//initialize the descriptor heap for rigidbodys
	//framecount render resource (3) and uav resource double buffer (2)
	m_GpuResources.resize(Graphics::FrameCount + 2);

	//create framecount render resource without any data,the data come from the compute shader
	const uint32_t drawDataByteSize = m_RigidBodyMaxCount * sizeof(RigidBodyDataDraw);
	for (size_t i = 0; i < Graphics::FrameCount; i++)
	{
		m_GpuResources[i].RigidBodyDataReource = D3DUtility::CreateUninitGPUBuffer(device, drawDataByteSize,
			D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);

		commandList->ResourceBarrier(1,
			&CD3DX12_RESOURCE_BARRIER::Transition(m_GpuResources[i].RigidBodyDataReource.Get(),
				D3D12_RESOURCE_STATE_GENERIC_READ, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE));

		std::wstring resourceName = L"RigidBodyRenderSRV" + std::to_wstring(i);
		NAME_D3D12_OBJECT_TEXT(m_GpuResources[i].RigidBodyDataReource, resourceName);
	}

	//create double buffer compute resource 
	const uint32_t computeDataByteSize = m_RigidBodyMaxCount * sizeof(RigidBodyDataCompute);
	for (size_t i = Graphics::FrameCount; i < m_GpuResources.size(); i++)
	{
		m_GpuResources[i].RigidBodyDataReource = D3DUtility::CreateGPUBuffer(
			device, commandList, m_CpuDatasCompute.data(),//note :only initialized Index Count data
			computeDataByteSize, m_GpuResources[i].RigidBodyDataReourceUpload,
			D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);

		commandList->ResourceBarrier(1,
			&CD3DX12_RESOURCE_BARRIER::Transition(m_GpuResources[i].RigidBodyDataReource.Get(),
				D3D12_RESOURCE_STATE_GENERIC_READ, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE));

		std::wstring resourceName = L"RigidBodyComputeSRVUAV" + std::to_wstring(i);
		NAME_D3D12_OBJECT_TEXT(m_GpuResources[i].RigidBodyDataReource, resourceName);
	}

	//create SRV descriptors
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Format = DXGI_FORMAT_UNKNOWN;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
	srvDesc.Buffer.FirstElement = 0;
	srvDesc.Buffer.NumElements = static_cast<UINT>(m_RigidBodyMaxCount);
	srvDesc.Buffer.StructureByteStride = sizeof(RigidBodyDataDraw);
	srvDesc.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_NONE;

	int srvIndex = RigidBodySrv0;
	for (size_t i = 0; i < Graphics::FrameCount; i++)
	{
		CD3DX12_CPU_DESCRIPTOR_HANDLE srvHandle(descriptorHeaps[DHeapRigidBody]->GetCPUDescriptorHandleForHeapStart(),
			srvIndex++, Graphics::g_DescriptorSizeCbvSrvUav);
		device->CreateShaderResourceView(m_GpuResources[i].RigidBodyDataReource.Get(), &srvDesc, srvHandle);
	}

	srvDesc.Buffer.StructureByteStride = sizeof(RigidBodyDataCompute);

	int srvComputeIndex = RigidBodySrvCompute0;
	for (size_t i = Graphics::FrameCount; i < m_GpuResources.size(); i++)
	{
		CD3DX12_CPU_DESCRIPTOR_HANDLE srvHandle(descriptorHeaps[DHeapRigidBody]->GetCPUDescriptorHandleForHeapStart(),
			srvComputeIndex++, Graphics::g_DescriptorSizeCbvSrvUav);
		device->CreateShaderResourceView(m_GpuResources[i].RigidBodyDataReource.Get(), &srvDesc, srvHandle);
	}

	//create UAV descriptors
	D3D12_UNORDERED_ACCESS_VIEW_DESC uavDesc = {};
	uavDesc.Format = DXGI_FORMAT_UNKNOWN;
	uavDesc.ViewDimension = D3D12_UAV_DIMENSION_BUFFER;
	uavDesc.Buffer.FirstElement = 0;
	uavDesc.Buffer.NumElements = static_cast<UINT>(m_RigidBodyMaxCount);
	uavDesc.Buffer.StructureByteStride = sizeof(RigidBodyDataDraw);
	uavDesc.Buffer.CounterOffsetInBytes = 0;
	uavDesc.Buffer.Flags = D3D12_BUFFER_UAV_FLAG_NONE;

	int uavDrawIndex = RigidBodyUav0;
	for (size_t i = 0; i < Graphics::FrameCount; i++)
	{
		CD3DX12_CPU_DESCRIPTOR_HANDLE uavHandle(descriptorHeaps[DHeapRigidBody]->GetCPUDescriptorHandleForHeapStart(),
			uavDrawIndex++, Graphics::g_DescriptorSizeCbvSrvUav);
		device->CreateUnorderedAccessView(m_GpuResources[i].RigidBodyDataReource.Get(), nullptr, &uavDesc, uavHandle);
	}

	uavDesc.Buffer.StructureByteStride = sizeof(RigidBodyDataCompute);

	int uavComputeIndex = RigidBodyUavCompute0;
	for (size_t i = Graphics::FrameCount; i < m_GpuResources.size(); i++)
	{
		CD3DX12_CPU_DESCRIPTOR_HANDLE uavHandle(descriptorHeaps[DHeapRigidBody]->GetCPUDescriptorHandleForHeapStart(),
			uavComputeIndex++, Graphics::g_DescriptorSizeCbvSrvUav);
		device->CreateUnorderedAccessView(m_GpuResources[i].RigidBodyDataReource.Get(), nullptr, &uavDesc, uavHandle);
	}

	m_pDesciptorHeap = descriptorHeaps[DHeapRigidBody].Get();
}

void RigidBody::BuildRigidBodyRootSignature(RootSignaturesManage& rootSignatures)
{
	auto device = Graphics::GetDevice();
	auto featureData = Graphics::GetRootSigFeature();

	D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags;
	ComPtr<ID3DBlob> rootSignatureBlob;
	ComPtr<ID3DBlob> errorBlob;

	//create root signature for rigidbody draw
	{
		rootSignatureFlags =
			D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS;

		CD3DX12_DESCRIPTOR_RANGE1 dRange[1];
		dRange[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);

		CD3DX12_ROOT_PARAMETER1 rootParas[RigidBodyDrawRootParaCount];
		rootParas[RigidBodyDrawRootParaCBV].InitAsConstantBufferView(0);
		rootParas[RigidBodyDrawRootParaSRVTable].InitAsDescriptorTable(_countof(dRange), dRange);

		CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc;
		rootSignatureDesc.Init_1_1(_countof(rootParas),
			rootParas, 0, nullptr, rootSignatureFlags);

		rootSignatureBlob.Reset();
		errorBlob.Reset();
		ThrowIfFailed(D3DX12SerializeVersionedRootSignature(&rootSignatureDesc,
			featureData.HighestVersion, rootSignatureBlob.GetAddressOf(), errorBlob.GetAddressOf()));

		ThrowIfFailed(device->CreateRootSignature(0,
			rootSignatureBlob->GetBufferPointer(),
			rootSignatureBlob->GetBufferSize(),
			IID_PPV_ARGS(rootSignatures[RootSigRigidBodyDraw].GetAddressOf())));

		m_pRootSigDraw = rootSignatures[RootSigRigidBodyDraw].Get();

	}

	//rigidbody compute rootsignature
	{
		CD3DX12_DESCRIPTOR_RANGE1 dRange[3];
		dRange[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);
		dRange[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 1, 0);
		dRange[2].Init(D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 1, 1);

		CD3DX12_ROOT_PARAMETER1 rootParas[RigidBodyComputeRootParaCount];
		rootParas[RigidBodyComputeRootParaCBV].InitAsConstantBufferView(0);
		rootParas[RigidBodyComputeRootParaSRVTable].InitAsDescriptorTable(1, &dRange[0]);
		rootParas[RigidBodyComputeRootParaUAVTable].InitAsDescriptorTable(1, &dRange[1]);
		rootParas[RigidBodyComputeRootParaDrawUAVTable].InitAsDescriptorTable(1, &dRange[2]);

		CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc;
		rootSignatureDesc.Init_1_1(_countof(rootParas), rootParas, 0, nullptr);

		rootSignatureBlob.Reset();
		errorBlob.Reset();
		ThrowIfFailed(D3DX12SerializeVersionedRootSignature(&rootSignatureDesc,
			featureData.HighestVersion, rootSignatureBlob.GetAddressOf(), errorBlob.GetAddressOf()));

		ThrowIfFailed(device->CreateRootSignature(0,
			rootSignatureBlob->GetBufferPointer(),
			rootSignatureBlob->GetBufferSize(),
			IID_PPV_ARGS(rootSignatures[RootSigRigidBodyCompute].GetAddressOf())));

		m_pRootSigCompute = rootSignatures[RootSigRigidBodyCompute].Get();
	}
}

void RigidBody::BuildRigidBodyPSO(PSOsManage& PSOs, ShadersManage& shaders, InputLayoutsManage& inputLayouts)
{
	auto device = Graphics::GetDevice();
	auto backBufferFormat = Graphics::GetBackBufferFormat();
	auto depthStencilFormat = Graphics::GetDepthStencilFormat();

	//create PSO for rigidbody draw
	{
		struct GraphicsPSOStream
		{
			CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE pRootSignature;
			CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
			CD3DX12_PIPELINE_STATE_STREAM_VS VS;
			CD3DX12_PIPELINE_STATE_STREAM_GS GS;
			CD3DX12_PIPELINE_STATE_STREAM_PS PS;
			CD3DX12_PIPELINE_STATE_STREAM_BLEND_DESC Blend;
			CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL DepthStencil;
			CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL_FORMAT DSVFormat;
			CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
			CD3DX12_PIPELINE_STATE_STREAM_RASTERIZER Rasterizer;
		} drawPsoStream;

		CD3DX12_BLEND_DESC blendDesc(D3D12_DEFAULT);
		blendDesc.RenderTarget[0].BlendEnable = TRUE;
		blendDesc.RenderTarget[0].LogicOpEnable = FALSE;
		blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
		blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
		blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
		blendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
		blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

		CD3DX12_DEPTH_STENCIL_DESC depthStencilDesc(D3D12_DEFAULT);
		depthStencilDesc.DepthEnable = TRUE;
		//depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ZERO;	//if not setting this ,will have square mask (bug

		D3D12_RT_FORMAT_ARRAY rtvFormats = {};
		rtvFormats.NumRenderTargets = 1;
		rtvFormats.RTFormats[0] = backBufferFormat;

		drawPsoStream.pRootSignature = m_pRootSigDraw;
		drawPsoStream.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT;
		drawPsoStream.VS = CD3DX12_SHADER_BYTECODE(shaders[ShaderRigidBodyVS].Get());
		drawPsoStream.GS = CD3DX12_SHADER_BYTECODE(shaders[ShaderRigidBodyGS].Get());
		drawPsoStream.PS = CD3DX12_SHADER_BYTECODE(shaders[ShaderRigidBodyPS].Get());
		drawPsoStream.Blend = blendDesc;
		drawPsoStream.DepthStencil = depthStencilDesc;
		drawPsoStream.DSVFormat = depthStencilFormat;
		drawPsoStream.RTVFormats = rtvFormats;
		drawPsoStream.Rasterizer = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);

		D3D12_PIPELINE_STATE_STREAM_DESC psoDrawDesc = { sizeof(GraphicsPSOStream), &drawPsoStream };
		ThrowIfFailed(device->CreatePipelineState(&psoDrawDesc, IID_PPV_ARGS(PSOs[PSORigidBodyDraw].GetAddressOf())));

		m_pPsoDraw = PSOs[PSORigidBodyDraw].Get();

		//create wireframe pso
		CD3DX12_RASTERIZER_DESC wareframeRtDesc(D3D12_DEFAULT);
		wareframeRtDesc.FillMode = D3D12_FILL_MODE_WIREFRAME;
		wareframeRtDesc.CullMode = D3D12_CULL_MODE_NONE;
		drawPsoStream.Rasterizer = wareframeRtDesc;

		D3D12_PIPELINE_STATE_STREAM_DESC psoWireframeDrawDesc = { sizeof(GraphicsPSOStream), &drawPsoStream };
		ThrowIfFailed(device->CreatePipelineState(&psoWireframeDrawDesc, IID_PPV_ARGS(PSOs[PSORigidBodyWireframeDraw].GetAddressOf())));

		m_pPsoWireframeDraw = PSOs[PSORigidBodyWireframeDraw].Get();

	}

	//cs pso
	{
		D3D12_COMPUTE_PIPELINE_STATE_DESC computeDesc = {};
		computeDesc.pRootSignature = m_pRootSigCompute;
		computeDesc.CS = CD3DX12_SHADER_BYTECODE(shaders[ShaderRigidBodyCS].Get());

		ThrowIfFailed(device->CreateComputePipelineState(&computeDesc, IID_PPV_ARGS(PSOs[PSORigidBodyCompute].GetAddressOf())));

		m_pPsoCompute = PSOs[PSORigidBodyCompute].Get();
	}
}

void RigidBody::SetWireframe(bool wireframe)
{
	m_IsWireframe = wireframe;
}

void RigidBody::ReleaseCpuResource()
{
	m_CpuDatasCompute.clear();
}

