#include "pch.h"
#include "DirectX12ssDemo.h"
#include "VertexData.h"
using namespace VertexData;

//imgui
#include "imgui.h"
#include "imgui_impl_win32.h"
#include "imgui_impl_dx12.h"


DirectX12ssDemo::DirectX12ssDemo(std::wstring title)
	:DirectX12App(title)
{
}

void DirectX12ssDemo::Initialize()
{
	//DirectX12 graphics intialize
	Graphics::Initialize();
	//imgui intialize
	InitializeIMGUI();
	//load shader resources
	LoadShaders();
	//load texture resources and intialize the textures descriptor heap
	Texture::LoadTextures(m_Textures, m_DescriptorHeaps[DHeapTextures].ReleaseAndGetAddressOf());
	//intialize game objects( camera,light,particle....)
	InitializeObjects();
	//intialize geometries data
	MeshGeometry::BuildGeometries(m_Geometries);
	//intialize materials data
	Material::BuildMaterials(m_Materials);
	//buid render items
	BuildRenderItems();

	//get lights info
	//build lights info debug cube info(render items)
	m_pLights0->BuildLightInfoRenderItems(m_RenderItems, m_GlobleObjectIndex);

	//get and build particles data
	m_pParticles0->BuildParticleDatasGPU(m_DescriptorHeaps);

	//build rigidbodies datas
	m_pRigidBodies0->BuildRigidBodyDatasGPU(m_DescriptorHeaps);

	//build and initialize frameresources
	FrameResource::BuildFrameResources(m_frameResources, Graphics::GetDevice(), 1, m_RenderItems.size(), m_Materials.size(), m_pLights0->kMaxLightCount);
	//initialize the main pass cbuffer
	InitializeMainPassCB();
	//build and initialize input layout
	BuildInputLayouts();
	//build and initialize rootsignatures
	BuildRootSignatures();
	//build and initialize PSOs
	BuildPipelineStateObjects();

	auto commandList = Graphics::GetCommandList();
	auto commandQueue = Graphics::GetCommandQueue();
	auto fence = Graphics::GetFence();
	ThrowIfFailed(commandList->Close());
	ID3D12CommandList* cmdLists[] = { commandList };
	commandQueue->ExecuteCommandLists(_countof(cmdLists), cmdLists);

	Graphics::FlushCommandQueue(Graphics::g_GraphicsFenceValue, fence, commandQueue);

	m_pRigidBodies0->ReleaseCpuResource();
}

void DirectX12ssDemo::Update(double dt)
{
	//update key board
	auto kb = WindowApplication::m_pKeyboard->GetState();
	static Input::Keyboard::KeyboardStateTracker keyTracer;
	keyTracer.Update(kb);

	auto fence = Graphics::GetFence();
	m_currentFrameResource = m_frameResources[Graphics::g_CurrentBackBuffer].get();
	if (m_currentFrameResource->FenceValue != 0 && fence->GetCompletedValue() < m_currentFrameResource->FenceValue)
	{
		HANDLE eventHandle = CreateEventEx(NULL, FALSE, FALSE, EVENT_ALL_ACCESS);
		ThrowIfFailed(fence->SetEventOnCompletion(m_currentFrameResource->FenceValue, eventHandle));
		WaitForSingleObject(eventHandle, INFINITE);
		CloseHandle(eventHandle);
	}

	//update total time
	static double totaltime = 0.0f;
	totaltime += dt;

	//update imgui
	ImGui_ImplDX12_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();

	//for (auto& o : m_GameObjects)
	//{
	//	o.second->Update(dt);
	//}

	//should make sure camera is first update 
	m_pCamera->Update(dt);
	m_pParticles0->Update(dt);
	m_pLights0->Update(dt);
	m_pRigidBodies0->Update(dt);

	Matrix viewMatrix = m_pCamera->GetViewMatrix();
	Matrix projectionMatrix = m_pCamera->GetProjectionMatrix();

	Matrix transposeViewProjectionMatrix = XMMatrixTranspose(XMMatrixMultiply(viewMatrix, projectionMatrix));

	{//update light frame resource
		auto lightSrv = m_currentFrameResource->LightSrv.get();
		lightSrv->CopyAllData(m_pLights0->LightDatas.data());
	}

	{//update particle frame resource
		auto particleDrawCB = m_currentFrameResource->ParticleDrawCB.get();
		m_pParticles0->ParticleDrawCBForEdit.WorldViewProjection = transposeViewProjectionMatrix;
		m_pParticles0->ParticleDrawCBForEdit.InverseView = XMMatrixTranspose(XMMatrixInverse(nullptr, viewMatrix));
		particleDrawCB->CopyData(0, m_pParticles0->ParticleDrawCBForEdit);
		m_pParticles0->ParticleCSConstantsData.paramf[0] = dt;
		m_pParticles0->ParticleCSCB->CopyData(0, m_pParticles0->ParticleCSConstantsData);
	}


	{//update materials changes
		auto currentMaterialCB = m_currentFrameResource->MaterialCB.get();

		for (auto& e : m_Materials)
		{
			if (e.second->NumFramesDirty > 0)
			{
				MaterialConstants materialConstants;
				materialConstants.DiffuseAlbedo = e.second->DiffuseAlbedo;
				materialConstants.FresnelR0 = e.second->FresnelR0;
				materialConstants.Roughness = e.second->Roughness;
				materialConstants.TransformMatrix = XMMatrixTranspose(e.second->MaterialTransform);

				currentMaterialCB->CopyData(e.second->MaterialCBIndex, materialConstants);

				e.second->NumFramesDirty--;
			}
		}
	}

	{
		//update render item
		float angle = static_cast<float>(totaltime * 90.0);
		Matrix transformMat = XMMatrixRotationY(XMConvertToRadians(angle)) * XMMatrixTranslation(0.0f, 0.51f, 0.0f);

		m_RenderItems["centerCube"]->WorldMatrix = transformMat;
		m_RenderItems["centerCube"]->NumFramesDirty = Graphics::FrameCount;

		static Vector3 sideCubePos{ -5.0f,1.0f,2.0f };
		static float sideCubeSpeed = 0.02f;
		if (kb.I)
		{
			sideCubePos.z += sideCubeSpeed;
			m_RenderItems["sideCube"]->NumFramesDirty = Graphics::FrameCount;
		}

		if (kb.K)
		{
			sideCubePos.z -= sideCubeSpeed;
			m_RenderItems["sideCube"]->NumFramesDirty = Graphics::FrameCount;
		}

		if (kb.J)
		{
			sideCubePos.x -= sideCubeSpeed;
			m_RenderItems["sideCube"]->NumFramesDirty = Graphics::FrameCount;
		}

		if (kb.L)
		{
			sideCubePos.x += sideCubeSpeed;
			m_RenderItems["sideCube"]->NumFramesDirty = Graphics::FrameCount;
		}

		transformMat = XMMatrixScaling(2.0f, 2.0f, 2.0f) *
			XMMatrixTranslationFromVector(sideCubePos);
		m_RenderItems["sideCube"]->WorldMatrix = transformMat;
	}


	{
		//update main constant buffer
		m_mainPassCB.ViewMatrix = XMMatrixTranspose(viewMatrix);
		m_mainPassCB.InverseViewMatrix = XMMatrixTranspose(XMMatrixInverse(nullptr, viewMatrix));
		m_mainPassCB.ProjectionMatrix = XMMatrixTranspose(projectionMatrix);
		m_mainPassCB.InverseProjectionMatrix = XMMatrixTranspose(XMMatrixInverse(nullptr, projectionMatrix));
		m_mainPassCB.ViewProjectionMatrix = XMMatrixTranspose(XMMatrixMultiply(viewMatrix, projectionMatrix));
		m_mainPassCB.InverseViewProjection = XMMatrixTranspose(XMMatrixInverse(nullptr, XMMatrixMultiply(viewMatrix, projectionMatrix)));
		m_mainPassCB.EyePositionWorld = m_pCamera->GetEyePosition();
		m_mainPassCB.TotalTime = static_cast<float>(totaltime);
		m_mainPassCB.deltaTime = static_cast<float>(dt);

		auto currentPassCB = m_currentFrameResource->PassCB.get();
		currentPassCB->CopyData(0, m_mainPassCB);

	}

	{
		//update object constant buffer
		auto currentObjectCB = m_currentFrameResource->ObjectCB.get();
		for (auto& e : m_RenderItems)
		{
			if (e.second->NumFramesDirty > 0)
			{
				ObjectConstants objectConstants;
				objectConstants.WorldMatrix = XMMatrixTranspose(e.second->WorldMatrix);
				objectConstants.TexTransform = XMMatrixTranspose(e.second->TexTransform);

				currentObjectCB->CopyData(e.second->ObjCBIndex, objectConstants);

				e.second->NumFramesDirty--;
			}
		}
	}
	//update IMGUI
	{
		if (keyTracer.pressed.P)
		{
			m_bShowIMGUI = !m_bShowIMGUI;
		}

		if (m_bShowIMGUI)
		{
			if (m_bShowHelpWindowIMGUI)
			{
				ImGui::ShowDemoWindow(&m_bShowHelpWindowIMGUI);
			}
			{
				ImGui::Begin("Some settings");
				ImGui::Checkbox("IMGUI Help", &m_bShowHelpWindowIMGUI);
				ImGui::Checkbox("Wireframe", &m_bIsWireframe);
				ImGui::Checkbox("Light Settings", &m_pLights0->bShowLightSettings);
				ImGui::Text("FPS: %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
				ImGui::End();
			}
		}
	}

	ImGui::EndFrame();
}

void DirectX12ssDemo::Render()
{
	Compute();

	auto device = Graphics::GetDevice();
	auto swapchain = Graphics::GetSwapChain();
	auto cmdListAlloc = m_currentFrameResource->CmdListAlloc;
	auto commandList = Graphics::GetCommandList();
	auto commandQueue = Graphics::GetCommandQueue();
	auto fence = Graphics::GetFence();
	auto currentBuffer = Graphics::GetCurrentRenderTarget();

	//reuse the command allocator,should reset it ;
	//can only reset when the associated command lists have finished execution on GPU
	ThrowIfFailed(cmdListAlloc->Reset());


	if (m_bIsWireframe)
	{//if wareframe ,use wareframe PSO
		ThrowIfFailed(commandList->Reset(cmdListAlloc.Get(), m_PSOs[PSOWireFrame].Get()));
	}
	else
	{
		ThrowIfFailed(commandList->Reset(cmdListAlloc.Get(), m_PSOs[PSOLightingDefault].Get()));
	}

	commandList->RSSetViewports(1, &Graphics::GetViewPort());
	commandList->RSSetScissorRects(1, &Graphics::GetScissorRect());

	commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(currentBuffer,
		D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));

	//clear back buffer and depth buffer
	auto rtvHeap = Graphics::GetRtvHeap();
	auto dsvHeap = Graphics::GetDsvHeap();
	auto rtvDescriptorSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtv(rtvHeap->GetCPUDescriptorHandleForHeapStart(),
		Graphics::g_CurrentBackBuffer, rtvDescriptorSize);
	CD3DX12_CPU_DESCRIPTOR_HANDLE dsv(dsvHeap->GetCPUDescriptorHandleForHeapStart());
	commandList->ClearRenderTargetView(rtv, Colors::Black, 0, nullptr);

	//stencil renge 0~255 depth range 0.0f~1.0f
	commandList->ClearDepthStencilView(dsv, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, nullptr);

	commandList->OMSetRenderTargets(1, &rtv, true, &dsv);

	ID3D12DescriptorHeap* descriptorHeaps[] = { m_DescriptorHeaps[DHeapTextures].Get() };
	commandList->SetDescriptorHeaps(_countof(descriptorHeaps), descriptorHeaps);

	commandList->SetGraphicsRootSignature(m_RootSignatures[RootSigLighting].Get());

	auto passCB = m_currentFrameResource->PassCB->Resource();
	auto lightSrv = m_currentFrameResource->LightSrv->Resource();
	commandList->SetGraphicsRootConstantBufferView(LightingRootParameters::LightingPassCBV, passCB->GetGPUVirtualAddress());
	commandList->SetGraphicsRootShaderResourceView(LightingRootParameters::LightingDataSRV, lightSrv->GetGPUVirtualAddress());

	RenderRItems(commandList, m_RenderItemsLayer[RILayerDefault]);

	if (!m_bIsWireframe) commandList->SetPipelineState(m_PSOs[PSOTransparent].Get());
	RenderRItems(commandList, m_RenderItemsLayer[RILayerTransparent]);

	auto lights = dynamic_cast<Lights*>(m_GameObjects[ObjLights0].get());
	if (!m_bIsWireframe) commandList->SetPipelineState(m_PSOs[PSOBloom].Get());
	RenderRItems(commandList, lights->LightsInfoRenderItems);

	m_pRigidBodies0->SetWireframe(m_bIsWireframe);
	m_pParticles0->SetWireFrame(m_bIsWireframe);
	m_pRigidBodies0->Render();
	m_pParticles0->Render();

	//GUI render
	commandList->SetDescriptorHeaps(1, m_DescriptorHeaps[DHeapIMGUI].GetAddressOf());
	ImGui::Render();
	ImGui_ImplDX12_RenderDrawData(ImGui::GetDrawData(), commandList);

	commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(currentBuffer,
		D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));

	ThrowIfFailed(commandList->Close());

	ID3D12CommandList* cmdsLists[] = { commandList };
	commandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);

	ThrowIfFailed(swapchain->Present(0, Graphics::GetTearingSupport() ? DXGI_PRESENT_ALLOW_TEARING : 0));

	Graphics::g_CurrentBackBuffer = swapchain->GetCurrentBackBufferIndex();

	m_currentFrameResource->FenceValue = ++Graphics::g_GraphicsFenceValue;
	commandQueue->Signal(fence, Graphics::g_GraphicsFenceValue);

	if (m_currentFrameResource->FenceValue != 0 && fence->GetCompletedValue() < m_currentFrameResource->FenceValue)
	{
		HANDLE eventHandle = CreateEventEx(NULL, FALSE, FALSE, EVENT_ALL_ACCESS);
		ThrowIfFailed(fence->SetEventOnCompletion(m_currentFrameResource->FenceValue, eventHandle));
		WaitForSingleObject(eventHandle, INFINITE);
		CloseHandle(eventHandle);
	}

}

void DirectX12ssDemo::Resize(size_t resolutionIndex, bool minimized)
{
	Graphics::ChangeResolution(resolutionIndex);

	dynamic_cast<Camera*>(m_GameObjects[ObjCamera].get())->Resize(Graphics::GetWidth(), Graphics::GetHeight());

	if (minimized)
	{
		//pause the game,render nothing
	}

}

void DirectX12ssDemo::Compute()
{
	for (auto& o : m_GameObjects)
	{
		o.second->Compute();
	}
}

void DirectX12ssDemo::InitializeIMGUI()
{
	auto device = Graphics::GetDevice();
	//create srv Heap for imgui
	{
		D3D12_DESCRIPTOR_HEAP_DESC srvHeapDesc = {};
		srvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		srvHeapDesc.NumDescriptors = 1;
		srvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		ThrowIfFailed(device->CreateDescriptorHeap(&srvHeapDesc, IID_PPV_ARGS(m_DescriptorHeaps[DHeapIMGUI].GetAddressOf())));
	}

	//imgui
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	io.WantCaptureMouse = true;
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
	ImGui::StyleColorsDark();
	//ImGui::StyleColorsClassic();
	ImGui_ImplWin32_Init(WindowApplication::GetHandle());
	auto imguiHeap = m_DescriptorHeaps[DHeapIMGUI].Get();
	ImGui_ImplDX12_Init(device, static_cast<int>(Graphics::FrameCount), DXGI_FORMAT_R8G8B8A8_UNORM, imguiHeap,
		imguiHeap->GetCPUDescriptorHandleForHeapStart(), imguiHeap->GetGPUDescriptorHandleForHeapStart());
}

void DirectX12ssDemo::InitializeObjects()
{
	auto camera = std::make_unique<Camera>();
	m_pCamera = camera.get();
	m_GameObjects[ObjCamera] = std::move(camera);

	auto lights = std::make_unique<Lights>();
	m_pLights0 = lights.get();
	m_GameObjects[ObjLights0] = std::move(lights);

	auto particles = std::make_unique<Particle>();
	m_pParticles0 = particles.get();
	m_GameObjects[ObjParticle0] = std::move(particles);

	auto rigidbodies = std::make_unique<RigidBody>();
	m_pRigidBodies0 = rigidbodies.get();
	m_pRigidBodies0->GetCamera(m_pCamera);
	m_GameObjects[ObjRigidBody0] = std::move(rigidbodies);

	for (auto& o : m_GameObjects)
	{
		o.second->Initialize();
	}
}

void DirectX12ssDemo::InitializeMainPassCB()
{
	m_mainPassCB.RenderTargetSize = Vector2(static_cast<float>(Graphics::GetWidth()), static_cast<float>(Graphics::GetHeight()));
	m_mainPassCB.InverseRenderTargetSize = Vector2(1.0f / Graphics::GetWidth(), 1.0f / Graphics::GetHeight());
	m_mainPassCB.NearZ = m_pCamera->GetClippingNear();
	m_mainPassCB.FarZ = m_pCamera->GetClippingFar();
	m_mainPassCB.AmbientLight = { 0.25f,0.25f,0.25f,1.0f };
	m_mainPassCB.DirectionLightNum = static_cast<int>(m_pLights0->kDirectionLightCount);
	m_mainPassCB.PointLightNum = static_cast<int>(m_pLights0->kPointLightCount);
	m_mainPassCB.SpotLightNum = static_cast<int>(m_pLights0->kSpotLightCount);
	m_mainPassCB.MaxLights = static_cast<int>(m_pLights0->kMaxLightCount);
}

void DirectX12ssDemo::LoadShaders()
{
	//shader macros
	//using in shaders
	const D3D_SHADER_MACRO macroOpaque[] = {
		"ALPHA_TEST","0",
		NULL,NULL };

	const D3D_SHADER_MACRO macroAlpha[] = {
		"ALPHA_TEST","1",
		NULL,NULL };
	//shaders
	//lighting shaders
	m_Shaders[ShaderLightingVS] = D3DUtility::LoadAndCompileShader(L"assets/shaders/Lighting.hlsl", nullptr, "VS", "vs_5_1");
	m_Shaders[ShaderBloomPS] = D3DUtility::LoadAndCompileShader(L"assets/shaders/Lighting.hlsl", macroOpaque, "BloomPS", "ps_5_1");
	m_Shaders[ShaderOpaqueLightingPS] = D3DUtility::LoadAndCompileShader(L"assets/shaders/Lighting.hlsl", macroOpaque, "PS", "ps_5_1");
	m_Shaders[ShaderAlphaLightingPS] = D3DUtility::LoadAndCompileShader(L"assets/shaders/Lighting.hlsl", macroAlpha, "PS", "ps_5_1");
	//particles shaders
	m_Shaders[ShaderParticleVS] = D3DUtility::LoadAndCompileShader(L"assets/shaders/ParticlesDraw.hlsl", nullptr, "ParticleVS", "vs_5_1");
	m_Shaders[ShaderParticleGS] = D3DUtility::LoadAndCompileShader(L"assets/shaders/ParticlesDraw.hlsl", nullptr, "ParticleGS", "gs_5_1");
	m_Shaders[ShaderParticlePS] = D3DUtility::LoadAndCompileShader(L"assets/shaders/ParticlesDraw.hlsl", nullptr, "ParticlePS", "ps_5_1");
	m_Shaders[ShaderParticleCS] = D3DUtility::LoadAndCompileShader(L"assets/shaders/ParticlesCS.hlsl", nullptr, "ParticleCS", "cs_5_1");
	//rigidbody shaders
	m_Shaders[ShaderRigidBodyVS] = D3DUtility::LoadAndCompileShader(L"assets/shaders/RigidBodyDraw.hlsl", nullptr, "RigidBodyVS", "vs_5_1");
	m_Shaders[ShaderRigidBodyGS] = D3DUtility::LoadAndCompileShader(L"assets/shaders/RigidBodyDraw.hlsl", nullptr, "RigidBodyGS", "gs_5_1");
	m_Shaders[ShaderRigidBodyPS] = D3DUtility::LoadAndCompileShader(L"assets/shaders/RigidBodyDraw.hlsl", nullptr, "RigidBodyPS", "ps_5_1");
	m_Shaders[ShaderRigidBodyCS] = D3DUtility::LoadAndCompileShader(L"assets/shaders/RigidBodyCS.hlsl", nullptr, "RigidBodyCS", "cs_5_1");
}

void DirectX12ssDemo::BuildRenderItems()
{
	auto centerCube = std::make_unique<RenderItem>();
	centerCube->WorldMatrix = XMMatrixScaling(2.0f, 2.0f, 2.0f) * XMMatrixTranslation(0.0f, 1.0f, 0.0f);
	centerCube->TexTransform = XMMatrixScaling(1.0f, 1.0f, 1.0f);
	centerCube->NumFramesDirty = Graphics::FrameCount;
	centerCube->ObjCBIndex = m_GlobleObjectIndex++;
	centerCube->RenderItemMaterial = m_Materials["TransparentFlower"].get();
	centerCube->RenderItemMeshGeomtry = m_Geometries[MeshGeo].get();
	centerCube->PrimitiveType = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	centerCube->IndexCount = centerCube->RenderItemMeshGeomtry->DrawArgs["cube"].IndexCount;
	centerCube->StartIndexLocation = centerCube->RenderItemMeshGeomtry->DrawArgs["cube"].StartIndexLocation;
	centerCube->BaseVertexLocation = centerCube->RenderItemMeshGeomtry->DrawArgs["cube"].BaseVertexLocation;
	m_RenderItems["centerCube"] = std::move(centerCube);

	auto sideCube = std::make_unique<RenderItem>();
	sideCube->WorldMatrix = XMMatrixScaling(2.5f, 2.5f, 2.5f) * XMMatrixTranslation(-5.0f, 1.25f, 2.0f);
	sideCube->TexTransform = XMMatrixScaling(1.0f, 1.0f, 1.0f);
	sideCube->NumFramesDirty = Graphics::FrameCount;
	sideCube->ObjCBIndex = m_GlobleObjectIndex++;
	sideCube->RenderItemMaterial = m_Materials["WhiteStone"].get();
	sideCube->RenderItemMeshGeomtry = m_Geometries[MeshGeo].get();
	sideCube->PrimitiveType = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	sideCube->IndexCount = sideCube->RenderItemMeshGeomtry->DrawArgs["cube"].IndexCount;
	sideCube->StartIndexLocation = sideCube->RenderItemMeshGeomtry->DrawArgs["cube"].StartIndexLocation;
	sideCube->BaseVertexLocation = sideCube->RenderItemMeshGeomtry->DrawArgs["cube"].BaseVertexLocation;
	m_RenderItems["sideCube"] = std::move(sideCube);

	auto gridTop = std::make_unique<RenderItem>();
	gridTop->WorldMatrix = Matrix::Identity;// XMMatrixScaling(1.0f, 1.0f, 1.0f);
	gridTop->TexTransform = XMMatrixScaling(2.0f, 2.0f, 1.0f);
	gridTop->NumFramesDirty = Graphics::FrameCount;
	gridTop->ObjCBIndex = m_GlobleObjectIndex++;
	gridTop->RenderItemMaterial = m_Materials["WoodFloor"].get();
	gridTop->RenderItemMeshGeomtry = m_Geometries[MeshGeo].get();
	gridTop->PrimitiveType = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	gridTop->IndexCount = gridTop->RenderItemMeshGeomtry->DrawArgs["grid"].IndexCount;
	gridTop->StartIndexLocation = gridTop->RenderItemMeshGeomtry->DrawArgs["grid"].StartIndexLocation;
	gridTop->BaseVertexLocation = gridTop->RenderItemMeshGeomtry->DrawArgs["grid"].BaseVertexLocation;
	m_RenderItems["gridTop"] = std::move(gridTop);

	auto gridWall = std::make_unique<RenderItem>();
	gridWall->WorldMatrix = XMMatrixScaling(0.20f, 1.0f, 1.0f) *
		XMMatrixRotationZ(XMConvertToRadians(270.0f)) *
		XMMatrixTranslation(-8.0f, 1.5f, 0.0f);
	gridWall->TexTransform = XMMatrixScaling(2.0f, 2.0f, 1.0f);
	gridWall->NumFramesDirty = Graphics::FrameCount;
	gridWall->ObjCBIndex = m_GlobleObjectIndex++;
	gridWall->RenderItemMaterial = m_Materials["WhiteMirror"].get();
	gridWall->RenderItemMeshGeomtry = m_Geometries[MeshGeo].get();
	gridWall->PrimitiveType = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	gridWall->IndexCount = gridWall->RenderItemMeshGeomtry->DrawArgs["grid"].IndexCount;
	gridWall->StartIndexLocation = gridWall->RenderItemMeshGeomtry->DrawArgs["grid"].StartIndexLocation;
	gridWall->BaseVertexLocation = gridWall->RenderItemMeshGeomtry->DrawArgs["grid"].BaseVertexLocation;
	m_RenderItems["gridWall"] = std::move(gridWall);

	//put the objects pointers to render layer renderitems vector
	m_RenderItemsLayer[RILayerDefault].push_back(m_RenderItems["sideCube"].get());
	m_RenderItemsLayer[RILayerDefault].push_back(m_RenderItems["gridTop"].get());
	m_RenderItemsLayer[RILayerDefault].push_back(m_RenderItems["gridWall"].get());
	m_RenderItemsLayer[RILayerTransparent].push_back(m_RenderItems["centerCube"].get());
}

void DirectX12ssDemo::BuildInputLayouts()
{

	m_InputLayouts[InputLayoutPosNormTex] = std::make_unique<std::vector<D3D12_INPUT_ELEMENT_DESC>>();
	m_InputLayouts[InputLayoutColor] = std::make_unique<std::vector<D3D12_INPUT_ELEMENT_DESC>>();
	*m_InputLayouts[InputLayoutPosNormTex] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "NORMAL",	  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};

	*m_InputLayouts[InputLayoutColor] =
	{
		{"COLOR",0,DXGI_FORMAT_R32G32B32A32_FLOAT,0,0,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0},
	};
}

void DirectX12ssDemo::BuildRootSignatures()
{
	//build game objects root signatures
	m_pLights0->BuildLightRootSignature(m_RootSignatures);
	m_pParticles0->BuildParticleRootSignature(m_RootSignatures);
	m_pRigidBodies0->BuildRigidBodyRootSignature(m_RootSignatures);

	NAME_D3D12_OBJECT(m_RootSignatures[RootSigLighting]);
	NAME_D3D12_OBJECT(m_RootSignatures[RootSigParticleDraw]);
	NAME_D3D12_OBJECT(m_RootSignatures[RootSigParticleCS]);
}

void DirectX12ssDemo::BuildPipelineStateObjects()
{
	//build game objects PSOs
	m_pLights0->BuildLightPSO(m_PSOs, m_Shaders, m_InputLayouts);
	m_pParticles0->BuildParticlePSO(m_PSOs, m_Shaders, m_InputLayouts);
	m_pRigidBodies0->BuildRigidBodyPSO(m_PSOs, m_Shaders, m_InputLayouts);

	NAME_D3D12_OBJECT(m_PSOs[PSOLightingDefault]);
	NAME_D3D12_OBJECT(m_PSOs[PSOWireFrame]);
	NAME_D3D12_OBJECT(m_PSOs[PSOTransparent]);
	NAME_D3D12_OBJECT(m_PSOs[PSOBloom]);
	NAME_D3D12_OBJECT(m_PSOs[PSOParticleDraw]);
	NAME_D3D12_OBJECT(m_PSOs[PSOParticleCompute]);
	NAME_D3D12_OBJECT(m_PSOs[PSORigidBodyDraw]);
	NAME_D3D12_OBJECT(m_PSOs[PSORigidBodyCompute]);

}

void DirectX12ssDemo::RenderRItems(ID3D12GraphicsCommandList* cmdList, const std::vector<RenderItem*>& ritems)
{
	//constant buffer 256 aligned
	UINT objectCBByteSize = (sizeof(ObjectConstants) + 255) & ~255;
	UINT materialCBByteSize = (sizeof(MaterialConstants) + 255) & ~255;

	auto objectCB = m_currentFrameResource->ObjectCB->Resource();
	auto materialCB = m_currentFrameResource->MaterialCB->Resource();

	for (size_t i = 0; i < ritems.size(); i++)
	{
		auto ri = ritems[i];

		cmdList->IASetVertexBuffers(0, 1, &ri->RenderItemMeshGeomtry->VertexBufferView());
		cmdList->IASetIndexBuffer(&ri->RenderItemMeshGeomtry->IndexBufferView());
		cmdList->IASetPrimitiveTopology(ri->PrimitiveType);

		CD3DX12_GPU_DESCRIPTOR_HANDLE tex(m_DescriptorHeaps[DHeapTextures]->GetGPUDescriptorHandleForHeapStart());
		tex.Offset(ri->RenderItemMaterial->DiffuseSrvHeapIndex, Graphics::g_DescriptorSizeCbvSrvUav);

		D3D12_GPU_VIRTUAL_ADDRESS objectCBAddress = objectCB->GetGPUVirtualAddress() + ri->ObjCBIndex * objectCBByteSize;
		D3D12_GPU_VIRTUAL_ADDRESS materialCBAddress = materialCB->GetGPUVirtualAddress() + ri->RenderItemMaterial->MaterialCBIndex * materialCBByteSize;

		cmdList->SetGraphicsRootDescriptorTable(LightingRootParameters::LightingTextureSRVTable, tex);
		cmdList->SetGraphicsRootConstantBufferView(LightingRootParameters::LightingObjectCBV, objectCBAddress);
		cmdList->SetGraphicsRootConstantBufferView(LightingRootParameters::LightingMaterialCBV, materialCBAddress);

		PIXBeginEvent(cmdList, 0, L"default object");
		cmdList->DrawIndexedInstanced(Lights::bLightIsShow ? ri->IndexCount: 0, 1, ri->StartIndexLocation, ri->BaseVertexLocation, 0);
		PIXEndEvent(cmdList);
	}
}
