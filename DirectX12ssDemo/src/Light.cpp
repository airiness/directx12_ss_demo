#include "pch.h"
#include "Light.h"
#include "imgui/imgui.h"
#include "WindowApplication.h"
#include "DirectX12Graphics.h"

bool Lights::bLightIsShow = false;

Lights::Lights()
{
}

Lights::~Lights()
{
}

void Lights::Initialize()
{
	//some light data for edit
	LightDatas.resize(kMaxLightCount);
	for (size_t i = 0; i < kDirectionLightCount; i++)
	{
		LightInfo lid;
		lid.lightType = LightType::DirectionLight;
		LightInfos.push_back(lid);
	}

	for (size_t i = 0; i < kPointLightCount; i++)
	{
		LightInfo lip;
		lip.lightType = LightType::PointLight;
		LightInfos.push_back(lip);
	}

	for (size_t i = 0; i < kSpotLightCount; i++)
	{
		LightInfo lis;
		lis.lightType = LightType::SpotLight;
		LightInfos.push_back(lis);
	}
}

void Lights::Update(double dt)
{
	auto kb = WindowApplication::m_pKeyboard->GetState();
	static Input::Keyboard::KeyboardStateTracker keyTracer;
	keyTracer.Update(kb);

	if (keyTracer.pressed.D1)
	{
		Lights::bLightIsShow = !Lights::bLightIsShow;
	}

	if (!bShowLightSettings)
	{
		return;
	}

	//clear the light debug cubes
	LightsInfoRenderItems.clear();

	// for gui title string
	std::string lightTypeStr[3] = { "DirectionLight","PointLight","SpotLight" };

	ImGui::Begin("Light Settings");
	for (uint32_t i = 0; i < kMaxLightCount; ++i)
	{
		//label text
		std::string lightlabel = "Light[" + std::to_string(i) + "]:" +
			lightTypeStr[static_cast<int>(LightInfos[i].lightType)] + ":";

		ImGui::Checkbox(lightlabel.c_str(), &LightInfos[i].IsLightUse);
		if (LightInfos[i].IsLightUse) LightDatas[i].IsLightUse = 1;
		else LightDatas[i].IsLightUse = 0;
		ImGui::SameLine();
		if (ImGui::Button((std::to_string(i) + "-settings").c_str()))
		{
			LightInfos[i].bShowInfoWindow = !LightInfos[i].bShowInfoWindow;
		}

	}
	ImGui::End();

	for (size_t i = 0; i < kDirectionLightCount; i++)
	{
		std::string lightGuiTitle = "Light[" + std::to_string(i) + "]:DirectionLight" + "-Properties:";
		if (LightInfos[i].bShowInfoWindow)
		{
			ImGui::Begin(lightGuiTitle.c_str(), &LightInfos[i].bShowInfoWindow);
			ImGui::Text("Color:");
			ImGui::SliderFloat("red", &LightDatas[i].Strength.x, 0.0f, 1.0f);
			ImGui::SliderFloat("green", &LightDatas[i].Strength.y, 0.0f, 1.0f);
			ImGui::SliderFloat("blue", &LightDatas[i].Strength.z, 0.0f, 1.0f);
			ImGui::Text("Direction:");
			ImGui::SliderFloat("x", &LightDatas[i].Direction.x, -1.0f, 1.0f);
			ImGui::SliderFloat("y", &LightDatas[i].Direction.y, -1.0f, 1.0f);
			ImGui::SliderFloat("z", &LightDatas[i].Direction.z, -1.0f, 1.0f);
			ImGui::End();
		}
	}


	for (size_t i = kDirectionLightCount; i < kDirectionLightCount + kPointLightCount; i++)
	{
		std::string lightGuiTitle = "Light[" + std::to_string(i) + "]:PointLight" + "-Properties:";
		if (LightInfos[i].bShowInfoWindow)
		{
			ImGui::Begin(lightGuiTitle.c_str(), &LightInfos[i].bShowInfoWindow);
			ImGui::Text("Color:");
			ImGui::SliderFloat("red", &LightDatas[i].Strength.x, 0.0f, 1.0f);
			ImGui::SliderFloat("green", &LightDatas[i].Strength.y, 0.0f, 1.0f);
			ImGui::SliderFloat("blue", &LightDatas[i].Strength.z, 0.0f, 1.0f);
			ImGui::Text("Fall Off Start and End Distance:");
			ImGui::SliderFloat("start", &LightDatas[i].FalloffStart, 0.0f, 100.0f);
			ImGui::SliderFloat("end", &LightDatas[i].FalloffEnd, 0.0f, 100.0f);
			ImGui::Text("Light Position:");
			ImGui::DragFloat("x", &LightDatas[i].Position.x, 0.1f);
			ImGui::DragFloat("y", &LightDatas[i].Position.y, 0.1f);
			ImGui::DragFloat("z", &LightDatas[i].Position.z, 0.1f);
			ImGui::End();

			auto ri = (*pRenderItemsManage)["lightInfo_Point" + std::to_string(i)].get();
			ri->WorldMatrix = XMMatrixScaling(0.2f, 0.2f, 0.2f) *
				XMMatrixTranslationFromVector(LightDatas[i].Position);
			ri->NumFramesDirty = Graphics::FrameCount;
			LightsInfoRenderItems.push_back(ri);
		}
	}

	for (size_t i = kDirectionLightCount + kPointLightCount;
		i < kDirectionLightCount + kPointLightCount + kSpotLightCount; ++i)
	{
		std::string lightGuiTitle = "Light[" + std::to_string(i) + "]:SpotLight" + "-Properties:";
		if (LightInfos[i].bShowInfoWindow)
		{
			ImGui::Begin(lightGuiTitle.c_str(), &LightInfos[i].bShowInfoWindow);
			ImGui::Text("Color:");
			ImGui::SliderFloat("red", &LightDatas[i].Strength.x, 0.0f, 1.0f);
			ImGui::SliderFloat("green", &LightDatas[i].Strength.y, 0.0f, 1.0f);
			ImGui::SliderFloat("blue", &LightDatas[i].Strength.z, 0.0f, 1.0f);
			ImGui::Text("Fall Off Start and End Distance:");
			ImGui::SliderFloat("start", &LightDatas[i].FalloffStart, 0.0f, 100.0f);
			ImGui::SliderFloat("end", &LightDatas[i].FalloffEnd, 0.0f, 100.0f);
			ImGui::Text("Light Position:");
			ImGui::DragFloat("pos-x", &LightDatas[i].Position.x, 0.1f);
			ImGui::DragFloat("pos-y", &LightDatas[i].Position.y, 0.1f);
			ImGui::DragFloat("pos-z", &LightDatas[i].Position.z, 0.1f);
			ImGui::Text("Direction:");
			ImGui::SliderFloat("dir-x", &LightDatas[i].Direction.x, -1.0f, 1.0f);
			ImGui::SliderFloat("dir-y", &LightDatas[i].Direction.y, -1.0f, 1.0f);
			ImGui::SliderFloat("dir-z", &LightDatas[i].Direction.z, -1.0f, 1.0f);
			ImGui::Text("SpotLightPower:");
			ImGui::SliderFloat("spot power", &LightDatas[i].SpotPower, 1.0f, 64.0f);
			ImGui::End();

			auto ri = (*pRenderItemsManage)["lightInfo_Spot" + std::to_string(i)].get();
			ri->WorldMatrix = XMMatrixScaling(0.2f, 0.2f, 0.2f) *
				XMMatrixTranslationFromVector(LightDatas[i].Position);
			ri->NumFramesDirty = Graphics::FrameCount;
			LightsInfoRenderItems.push_back(ri);
		}
	}

	LightDirtyFlag = Graphics::FrameCount;

}

void Lights::Compute()
{
}

void Lights::Render()
{
}

void Lights::Finalize()
{
}

void Lights::BuildLightInfoRenderItems(RenderItem::RenderItemsManage& renderItems, size_t& objectIndex)
{
	pRenderItemsManage = &renderItems;
	auto materials = Material::GetMaterials();
	auto geometries = MeshGeometry::GetMeshGeometries();

	auto makeLightInfoCube = [&]() ->std::unique_ptr<RenderItem> {
		auto lightInfoCube = std::make_unique<RenderItem>();
		lightInfoCube->WorldMatrix = XMMatrixScaling(0.2f, 0.2f, 0.2f);
		lightInfoCube->TexTransform = XMMatrixScaling(1.0f, 1.0f, 1.0f);
		lightInfoCube->NumFramesDirty = Graphics::FrameCount;
		lightInfoCube->ObjCBIndex = objectIndex++;
		lightInfoCube->RenderItemMaterial = (*materials)["WhiteStone"].get();
		lightInfoCube->RenderItemMeshGeomtry = (*geometries)[MeshGeo].get();
		lightInfoCube->PrimitiveType = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
		lightInfoCube->IndexCount = lightInfoCube->RenderItemMeshGeomtry->DrawArgs["cube"].IndexCount;
		lightInfoCube->StartIndexLocation = lightInfoCube->RenderItemMeshGeomtry->DrawArgs["cube"].StartIndexLocation;
		lightInfoCube->BaseVertexLocation = lightInfoCube->RenderItemMeshGeomtry->DrawArgs["cube"].BaseVertexLocation;

		return std::move(lightInfoCube);
	};

	for (size_t i = kDirectionLightCount; i < kDirectionLightCount + kPointLightCount; ++i)
	{
		renderItems["lightInfo_Point" + std::to_string(i)] = makeLightInfoCube();
	}

	for (size_t i = kDirectionLightCount + kPointLightCount; i < kMaxLightCount; i++)
	{
		renderItems["lightInfo_Spot" + std::to_string(i)] = makeLightInfoCube();
	}
}


void Lights::BuildLightPSO(PSOsManage& PSOs, ShadersManage& shaders, InputLayoutsManage& inputLayouts)
{
	auto device = Graphics::GetDevice();
	auto backBufferFormat = Graphics::GetBackBufferFormat();
	auto depthStencilFormat = Graphics::GetDepthStencilFormat();
	//pso structure
	struct PSOStream
	{
		CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE pRootSignature;
		CD3DX12_PIPELINE_STATE_STREAM_INPUT_LAYOUT InputLayout;
		CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
		CD3DX12_PIPELINE_STATE_STREAM_VS VS;
		CD3DX12_PIPELINE_STATE_STREAM_PS PS;
		CD3DX12_PIPELINE_STATE_STREAM_BLEND_DESC Blend;
		CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL DepthStencil;
		CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL_FORMAT DSVFormat;
		CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
		CD3DX12_PIPELINE_STATE_STREAM_RASTERIZER rasterizer;
	};

	//1.default Lighting PSO(default----------------------------------
	PSOStream defaultPsoStream;

	D3D12_RT_FORMAT_ARRAY rtvFormats = {};
	rtvFormats.NumRenderTargets = 1;
	rtvFormats.RTFormats[0] = backBufferFormat;

	defaultPsoStream.pRootSignature = m_pLightRootSignature;
	defaultPsoStream.InputLayout = { inputLayouts[InputLayoutPosNormTex]->data(), static_cast<UINT>(inputLayouts[InputLayoutPosNormTex]->size()) };
	defaultPsoStream.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	defaultPsoStream.VS = CD3DX12_SHADER_BYTECODE(shaders[ShaderLightingVS].Get());
	defaultPsoStream.PS = CD3DX12_SHADER_BYTECODE(shaders[ShaderOpaqueLightingPS].Get());
	defaultPsoStream.Blend = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	defaultPsoStream.DepthStencil = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	defaultPsoStream.DSVFormat = depthStencilFormat;
	defaultPsoStream.RTVFormats = rtvFormats;
	defaultPsoStream.rasterizer = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);;

	D3D12_PIPELINE_STATE_STREAM_DESC psoLightingDesc = { sizeof(PSOStream), &defaultPsoStream };
	ThrowIfFailed(device->CreatePipelineState(&psoLightingDesc, IID_PPV_ARGS(PSOs[PSOLightingDefault].GetAddressOf())));

	m_pLightPsoDefault = PSOs[PSOLightingDefault].Get();

	//2.wareframe PSO-----------------------------------------------
	PSOStream wareframePsoStream = defaultPsoStream;

	CD3DX12_RASTERIZER_DESC wareframeRtDesc(D3D12_DEFAULT);
	wareframeRtDesc.FillMode = D3D12_FILL_MODE_WIREFRAME;
	wareframeRtDesc.CullMode = D3D12_CULL_MODE_NONE;
	wareframePsoStream.rasterizer = wareframeRtDesc;

	D3D12_PIPELINE_STATE_STREAM_DESC psoWareframeDesc = { sizeof(PSOStream), &wareframePsoStream };
	ThrowIfFailed(device->CreatePipelineState(&psoWareframeDesc, IID_PPV_ARGS(PSOs[PSOWireFrame].GetAddressOf())));

	m_pLightPsoWireFrame = PSOs[PSOWireFrame].Get();

	//3.transparent PSO----------------------------------------------------
	PSOStream transparentPsoStream = defaultPsoStream;

	CD3DX12_BLEND_DESC transblendDesc(D3D12_DEFAULT);
	CD3DX12_RASTERIZER_DESC transpRsDesc(D3D12_DEFAULT);

	D3D12_RENDER_TARGET_BLEND_DESC rtbDesc;
	rtbDesc.BlendEnable = true;
	rtbDesc.LogicOpEnable = false;
	rtbDesc.SrcBlend = D3D12_BLEND_SRC_ALPHA;
	rtbDesc.DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	rtbDesc.BlendOp = D3D12_BLEND_OP_ADD;
	rtbDesc.SrcBlendAlpha = D3D12_BLEND_ONE;
	rtbDesc.DestBlendAlpha = D3D12_BLEND_ZERO;
	rtbDesc.BlendOpAlpha = D3D12_BLEND_OP_ADD;
	rtbDesc.LogicOp = D3D12_LOGIC_OP_NOOP;
	rtbDesc.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	transblendDesc.RenderTarget[0] = rtbDesc;

	transpRsDesc.CullMode = D3D12_CULL_MODE_NONE;

	transparentPsoStream.PS = CD3DX12_SHADER_BYTECODE(shaders[ShaderAlphaLightingPS].Get());
	transparentPsoStream.Blend = transblendDesc;
	transparentPsoStream.rasterizer = transpRsDesc;
	D3D12_PIPELINE_STATE_STREAM_DESC transparentPsoDesc = { sizeof(PSOStream), &transparentPsoStream };
	ThrowIfFailed(device->CreatePipelineState(&transparentPsoDesc, IID_PPV_ARGS(PSOs[PSOTransparent].GetAddressOf())));

	m_pLightPsoTransparent = PSOs[PSOTransparent].Get();

	//bloom PSO
	PSOStream bloomPsoStream = defaultPsoStream;
	bloomPsoStream.PS = CD3DX12_SHADER_BYTECODE(shaders[ShaderBloomPS].Get());

	D3D12_PIPELINE_STATE_STREAM_DESC bloomPsoDesc = { sizeof(PSOStream), &bloomPsoStream };
	ThrowIfFailed(device->CreatePipelineState(&bloomPsoDesc, IID_PPV_ARGS(PSOs[PSOBloom].GetAddressOf())));

	m_pLightPsoBloom = PSOs[PSOBloom].Get();
}

void Lights::BuildLightRootSignature(RootSignaturesManage& rootSignatures)
{
	auto device = Graphics::GetDevice();
	//if support version1.1?
	D3D12_FEATURE_DATA_ROOT_SIGNATURE featureData = Graphics::GetRootSigFeature();

	D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags;
	// Serialize the root signature.
	ComPtr<ID3DBlob> rootSignatureBlob;
	ComPtr<ID3DBlob> errorBlob;

	//create lighting objeccts shader root signature
	{
		// Allow input layout and deny unnecessary access to certain pipeline stages.
		rootSignatureFlags =
			D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
			D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS;

		//descriptor range
		CD3DX12_DESCRIPTOR_RANGE1 textureTableRange[1];
		textureTableRange[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1/*descriptor number*/, 0/*register t0*/);

		CD3DX12_ROOT_PARAMETER1 LightingRootParameters[LightingRootParameters::LightingRootParametersCount];
		LightingRootParameters[LightingRootParameters::LightingTextureSRVTable].InitAsDescriptorTable(1, textureTableRange, D3D12_SHADER_VISIBILITY_PIXEL);
		LightingRootParameters[LightingRootParameters::LightingObjectCBV].InitAsConstantBufferView(0);
		LightingRootParameters[LightingRootParameters::LightingPassCBV].InitAsConstantBufferView(1);
		LightingRootParameters[LightingRootParameters::LightingMaterialCBV].InitAsConstantBufferView(2);
		LightingRootParameters[LightingRootParameters::LightingDataSRV].InitAsShaderResourceView(3, 2);

		auto staticSampler = D3DUtility::GetStaticSamplers();

		CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDescription;
		rootSignatureDescription.Init_1_1(_countof(LightingRootParameters), LightingRootParameters,
			static_cast<UINT>(staticSampler.size()), staticSampler.data(), rootSignatureFlags);

		// Serialize the root signature.
		rootSignatureBlob.Reset();
		errorBlob.Reset();

		ThrowIfFailed(D3DX12SerializeVersionedRootSignature(&rootSignatureDescription,
			featureData.HighestVersion, rootSignatureBlob.GetAddressOf(), errorBlob.GetAddressOf()));
		// Create the root signature.
		ThrowIfFailed(device->CreateRootSignature(0,
			rootSignatureBlob->GetBufferPointer(),
			rootSignatureBlob->GetBufferSize(),
			IID_PPV_ARGS(rootSignatures[RootSigLighting].GetAddressOf())));

	}

	m_pLightRootSignature = rootSignatures[RootSigLighting].Get();
}
