//demo main class
//zhang ruisong

#pragma once
#include "DirectX12App.h"
#include "ResourcesManage.h"
#include "IObject.h"
#include "Camera.h"
#include "Material.h"
#include "Texture.h"
#include "FrameResource.h"
#include "RenderItem.h"


using ObjectsManage = std::unordered_map<uint32_t, std::unique_ptr<IObject>>;
using MaterialsManage = std::unordered_map<std::string, std::unique_ptr<Material>>;
using MeshGeosManage = std::unordered_map<uint32_t, std::unique_ptr<MeshGeometry>>;
using texturesManage = std::vector<std::unique_ptr<Texture>>;

class DirectX12ssDemo : public DirectX12App
{
public:
	DirectX12ssDemo(std::wstring title);
	DirectX12ssDemo(const DirectX12ssDemo&) = delete;

	virtual void Initialize() override;
	virtual void Update(double dt) override;
	virtual void Render() override;
	virtual void Resize(size_t resolutionIndex, bool minimized) override;

	virtual uint32_t GetWidth() override { return Graphics::GetWidth(); };
	virtual uint32_t GetHeight() override { return Graphics::GetHeight(); };
	virtual void Finalize() override { Graphics::Finalize(); };

	virtual bool GetTearingSupport() { return Graphics::GetTearingSupport(); };
	virtual IDXGISwapChain* GetSwapchain() override { return Graphics::GetSwapChain(); };

private:
	void Compute();

	void InitializeIMGUI();
	void InitializeObjects();
	void InitializeMainPassCB();
	void LoadShaders();
	void BuildRenderItems();
	void BuildInputLayouts();
	void BuildRootSignatures();
	void BuildPipelineStateObjects();

	void RenderRItems(ID3D12GraphicsCommandList* cmdList, const std::vector<RenderItem*>& ritems);

	std::vector<std::unique_ptr<FrameResource>>m_frameResources;
	FrameResource* m_currentFrameResource = nullptr;

	ObjectsManage m_GameObjects;
	MaterialsManage m_Materials;
	MeshGeosManage m_Geometries;
	InputLayoutsManage m_InputLayouts;
	DescriptorHeapsManage m_DescriptorHeaps;
	RootSignaturesManage m_RootSignatures;
	PSOsManage  m_PSOs;
	texturesManage m_Textures;
	ShadersManage m_Shaders;
	RenderItem::RenderItemsManage m_RenderItems;
	std::vector<RenderItem*> m_RenderItemsLayer[RenderItemsLayerCount];

	//for mesh object index 
	size_t m_GlobleObjectIndex = 0;
	//main pass cb for default objects
	PassConstants m_mainPassCB;

	//app state flags
	bool m_bIsWireframe = false;
	bool m_bShowIMGUI = false;
	bool m_bShowHelpWindowIMGUI = false;

	//game object pointer
	Camera* m_pCamera = nullptr;
	Particle* m_pParticles0 = nullptr;
	Lights* m_pLights0 = nullptr;
	RigidBody* m_pRigidBodies0 = nullptr;
};