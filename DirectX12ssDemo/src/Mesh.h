//meshdata class
//zhang ruisong

#pragma once
#include "VertexData.h"

enum MeshGeometriesIndex :uint32_t
{
	MeshGeo = 0,
	MeshGeoCount
};

//for saving several geometris in same index and vertex buffer
struct SubMeshGeometry
{
	UINT IndexCount = 0;
	UINT StartIndexLocation = 0;
	int BaseVertexLocation = 0;
};

struct MeshGeometry
{
	using MeshGeometries = std::unordered_map<uint32_t, std::unique_ptr<MeshGeometry>>;

	static void BuildGeometries(MeshGeometries& geometries);
	static MeshGeometries* pMeshGeometriesManage;
	static MeshGeometries* GetMeshGeometries() { return pMeshGeometriesManage; };

	std::string Name;
	ComPtr<ID3DBlob> VertexBufferCPU = nullptr;
	ComPtr<ID3DBlob> IndexBufferCPU = nullptr;

	ComPtr<ID3D12Resource> VertexBufferGPU = nullptr;
	ComPtr<ID3D12Resource> IndexBufferGPU = nullptr;

	ComPtr<ID3D12Resource> VertexBufferUploader = nullptr;
	ComPtr<ID3D12Resource> IndexBufferUploader = nullptr;

	UINT VertexByteStride = 0;
	UINT VertexBufferByteSize = 0;
	DXGI_FORMAT IndexFormat = DXGI_FORMAT_R16_UINT;
	UINT IndexBufferByteSize = 0;

	std::unordered_map<std::string, SubMeshGeometry> DrawArgs;

	D3D12_VERTEX_BUFFER_VIEW VertexBufferView() const;

	D3D12_INDEX_BUFFER_VIEW IndexBufferView()const;

	void DisposeUploaders();
};

struct MeshData
{
	std::vector<VertexData::VertexPosNormTexcoord> Vertices;
	std::vector<uint16_t> Indices;

	//create a cube mesh
	static std::unique_ptr<MeshData> CreateCube(float width = 1.0f,float height = 1.0f,float depth = 1.0f);
	//create a grid mesh
	static std::unique_ptr<MeshData> CreateGrid(float width, float depth, size_t widthCount, size_t depthCount);

};