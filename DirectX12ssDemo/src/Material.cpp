#include "pch.h"
#include "Material.h"
#include "DirectX12Graphics.h"

Material::Materials* Material::pMaterialsManage = nullptr;

void Material::BuildMaterials(Materials& materials)
{
	pMaterialsManage = &materials;

	struct MaterialProp {
		std::string name; int matIndex;
		int heapIndex; Vector4 diffAlbedo; Vector3 fresnel; float roughness;
	};

	auto BuidAMat = [&](const MaterialProp& mat) -> void {
		auto pMat = std::make_unique<Material>();
		pMat->Name = mat.name;
		pMat->MaterialCBIndex = mat.matIndex;
		pMat->DiffuseSrvHeapIndex = mat.heapIndex;
		pMat->NumFramesDirty = Graphics::FrameCount;
		pMat->DiffuseAlbedo = mat.diffAlbedo;
		pMat->FresnelR0 = mat.fresnel;
		pMat->Roughness = mat.roughness;
		materials[mat.name] = std::move(pMat);
	};

	std::vector<MaterialProp> matProps = {
		{"WhiteStone",		 0,0,{1.0f, 1.0f, 1.0f, 1.0f},{0.02f, 0.02f, 0.02f},0.1f},
		{"BroneStone",		 1,1,{1.0f, 1.0f, 1.0f, 1.0f},{0.05f, 0.05f, 0.05f},0.3f},
		{"WoodFloor",		 2,2,{1.0f, 1.0f, 1.0f, 1.0f},{0.08f, 0.08f, 0.08f},0.5f},
		{"TransparentFlower",3,3,{1.0f, 1.0f, 1.0f, 1.0f},{0.1f,  0.1f,  0.1f}, 0.8f},
		{"WaveWater",		 4,4,{1.0f, 1.0f, 1.0f, 1.0f},{0.05f, 0.05f, 0.05f},0.1f},
		{"WhiteMirror",		 5,5,{1.0f, 1.0f, 1.0f, 1.0f},{0.05f, 0.05f, 0.05f},0.0f},
	};

	for (auto& mp : matProps)
	{
		BuidAMat(mp);
	}
}