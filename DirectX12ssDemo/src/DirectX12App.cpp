#include "pch.h"
#include "DirectX12App.h"

DirectX12App::DirectX12App(std::wstring title):
	m_title(title)
{
}

DirectX12App::~DirectX12App()
{
}

void DirectX12App::SetWindowBounds(int left, int top, int right, int bottom)
{
	m_windowBounds.left = static_cast<LONG>(left);
	m_windowBounds.top = static_cast<LONG>(top);
	m_windowBounds.right = static_cast<LONG>(right);
	m_windowBounds.bottom = static_cast<LONG>(bottom);
}

