#include "pch.h"
#include "Camera.h"
#include "WindowApplication.h"

Camera::Camera(uint32_t displayWidth, uint32_t displayHeight,
	Vector3 position, Vector3 focus, Vector3 front, Vector3 right,
	float Fov, float znear, float zfar) :
	m_clientWidth(displayWidth), m_clientHeight(displayHeight),
	m_position(position), m_focus(focus), m_front(front), m_right(right),
	m_fov(Fov), m_near(znear), m_far(zfar), m_aspect(displayWidth / static_cast<float>(displayHeight)),
	m_bYmovement(false)
{
}

Camera::~Camera()
{
}

void Camera::Initialize()
{
	m_up = XMVector3Cross(m_front, m_right);
	m_up = XMVector3Normalize(m_up);
	m_viewMatrix = XMMatrixLookAtLH(m_position, m_focus, m_up);
	m_projMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(m_fov), m_aspect, m_near, m_far);
}

void Camera::Update(double dt)
{
	auto kb = WindowApplication::m_pKeyboard->GetState();
	static Input::Keyboard::KeyboardStateTracker keyTracer;

	auto mouse = WindowApplication::m_pMouse->GetState();


	static const float rotateAngle = 0.25f;
	static float unitStep = 0.02f;
	static float rotateRadians = XMConvertToRadians(rotateAngle);
	static int lastMouseX = 0;
	static int lastMouseY = 0;

	keyTracer.Update(kb);
	if (keyTracer.pressed.O)
	{
		SetYmovement(!m_bYmovement);
#if defined(_DEBUG)
		m_bYmovement ?
			std::cout << "Y axis enabled camera" << std::endl :
			std::cout << "Y axis disabled camera" << std::endl;
#endif
	}
	if (keyTracer.pressed.Space)
	{
		Reset();
	}
	Vector3 lookvelocity{ 0.0f,0.0f,0.0f };

	if (kb.LeftShift)
	{
		unitStep = 0.36f;
	}
	else if (kb.LeftControl)
	{
		unitStep = 0.9f;
	}
	else
	{
		unitStep = 0.02f;
	}

	if (kb.Up || kb.W)
	{
		MoveForword(unitStep);
	}

	if (kb.Down || kb.S)
	{
		MoveForword(-unitStep);
	}

	if (kb.Left || kb.A)
	{
		Strafe(-unitStep);
	}

	if (kb.Right || kb.D)
	{
		Strafe(unitStep);
	}

	if (kb.Q)
	{
		MoveUp(unitStep);
	}

	if (kb.E)
	{
		MoveUp(-unitStep);
	}

	if (mouse.rightButton)
	{
		float dx = XMConvertToRadians(rotateAngle * static_cast<float>(mouse.x - lastMouseX));
		float dy = XMConvertToRadians(rotateAngle * static_cast<float>(mouse.y - lastMouseY));
		
		Pitch(dy);
		RotateY(dx);
	}

	lastMouseX = mouse.x;
	lastMouseY = mouse.y;

	if (m_velocity.Length() > m_maxVelocity)
	{
		m_velocity = XMVector3Normalize(m_velocity) * m_maxVelocity;
	}

	m_position += m_velocity;
	m_velocity = { 0.0f,0.0f,0.0f };
	m_focus = m_position + m_front;

	m_viewMatrix = XMMatrixLookAtLH(m_position, m_focus, m_up);

	Matrix viewMat(m_viewMatrix);
	m_right = { viewMat._11,viewMat._21,viewMat._31 };
	m_up = { viewMat._12,viewMat._22,viewMat._32 };
	m_front = { viewMat._13,viewMat._23,viewMat._33 };

}

void Camera::Compute()
{
}

void Camera::Resize(uint32_t width, uint32_t height)
{
	m_clientWidth = width;
	m_clientHeight = height;
	m_aspect = static_cast<float>(m_clientWidth) / static_cast<float>(m_clientHeight);
	m_projMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(m_fov), m_aspect, m_near, m_far);
}

void Camera::Render()
{
}

void Camera::Finalize()
{
}

void Camera::Reset(Vector3 position, Vector3 focus, Vector3 front, Vector3 right)
{
	m_position = position;
	m_front = front;
	m_right = right;
	m_up = XMVector3Cross(front, right);
	m_up = XMVector3Normalize(m_up);
	m_viewMatrix = XMMatrixLookAtLH(position, focus, m_up);
}

void Camera::MoveForword(float units)
{
	if (m_bYmovement)
	{
		m_velocity += m_front * units;
	}
	else
	{
		Vector3 moveVec(m_front.x, 0.0f, m_front.z);
		moveVec = XMVector3Normalize(moveVec);
		moveVec *= units;
		m_velocity += moveVec;
	}

}

void Camera::Strafe(float units)
{
	m_velocity += m_right * units;
}

void Camera::MoveUp(float units)
{
	if (m_bYmovement)
	{
		m_velocity.y += units;
	}
}

void Camera::Yaw(float radians)
{
	if (0.0f == radians)
	{
		return;
	}
	Matrix rotMat;
	rotMat = XMMatrixRotationAxis(m_up, radians);
	m_right = XMVector3TransformNormal(m_right, rotMat);
	m_front = XMVector3TransformNormal(m_front, rotMat);

}

void Camera::Pitch(float radians)
{
	if (0 == radians)
	{
		return;
	}
	Matrix rotMat;
	rotMat = XMMatrixRotationAxis(m_right, radians);
	m_up = XMVector3TransformNormal(m_up, rotMat);
	m_front = XMVector3TransformNormal(m_front, rotMat);

}

void Camera::Roll(float radians)
{
	if (0.0f == radians)
	{
		return;
	}
	Matrix rotMat;
	rotMat = XMMatrixRotationAxis(m_front, radians);
	m_up = XMVector3TransformNormal(m_up, rotMat);
	m_right = XMVector3TransformNormal(m_right, rotMat);
}

void Camera::RotateY(float radians)
{
	if (0.0f == radians)
	{
		return;
	}

	Matrix rotMat = XMMatrixRotationY(radians);
	m_up = XMVector3TransformNormal(m_up, rotMat);
	m_front = XMVector3TransformNormal(m_front, rotMat);
	m_right = XMVector3TransformNormal(m_right, rotMat);
}

void Camera::SetYmovement(bool isYmovement)
{
	m_bYmovement = isYmovement;
}


