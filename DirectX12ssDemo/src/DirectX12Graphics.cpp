#include "pch.h"
#include "DirectX12Graphics.h"
#include "WindowApplication.h"

namespace Graphics
{
	struct Resolution
	{
		uint32_t Width;
		uint32_t Height;
	};

	const Resolution ResolutionOptions[] =
	{
		{800u,600u},
		{1280u,720u},
		{1920u,1080u},
		{3840u,2160u},
	};

	uint32_t g_ResolutionIndex = 2;

	const uint32_t FrameCount = 3;
	ComPtr<ID3D12Resource> g_RenderTargets[FrameCount];
	uint32_t g_CurrentBackBuffer = 0;
	ComPtr<ID3D12Resource> g_DepthStencilView;

	const size_t kResolutionOptionCount = _countof(ResolutionOptions);

	ComPtr<ID3D12Device2> g_Device = nullptr;
	ComPtr<IDXGIAdapter4> g_Adaptor = nullptr;
	ComPtr<IDXGIFactory5> g_Factory = nullptr;
	ComPtr<IDXGISwapChain4> g_SwapChain = nullptr;

	ComPtr<ID3D12CommandQueue> g_GraphicsQueue = nullptr;
	ComPtr<ID3D12CommandQueue> g_ComputeQueue = nullptr;
	ComPtr<ID3D12CommandQueue> g_CopyQueue = nullptr;

	ComPtr<ID3D12GraphicsCommandList> g_GraphicsCommandList = nullptr;
	ComPtr<ID3D12GraphicsCommandList> g_ComputeCommandList = nullptr;
	ComPtr<ID3D12GraphicsCommandList> g_CopyCommandList = nullptr;

	ComPtr<ID3D12CommandAllocator> g_GraphicsAlloc = nullptr;
	ComPtr<ID3D12CommandAllocator> g_ComputeAlloc = nullptr;
	ComPtr<ID3D12CommandAllocator> g_CopyAlloc = nullptr;

	ComPtr<ID3D12Fence> g_GraphicsFence = nullptr;
	ComPtr<ID3D12Fence> g_ComputeFence = nullptr;
	ComPtr<ID3D12Fence> g_CopyFence = nullptr;

	uint64_t g_GraphicsFenceValue = 0;
	uint64_t g_ComputeFenceValue = 0;
	uint64_t g_CopyFenceValue = 0;

	ComPtr<ID3D12DescriptorHeap> g_RtvHeap = nullptr;
	ComPtr<ID3D12DescriptorHeap> g_DsvHeap = nullptr;

	extern UINT g_DescriptorSizeRtv = 0;
	extern UINT g_DescriptorSizeDsv = 0;
	extern UINT g_DescriptorSizeCbvSrvUav = 0;
	extern UINT g_DescriptorSizeSampler = 0;

	CD3DX12_VIEWPORT g_Viewport;
	CD3DX12_RECT g_ScissorRect;

	bool bIsTearingSupport = false;
	bool bIsWindowMode = true;
	bool bIsWindowVisible = true;

	DXGI_FORMAT s_BackBufferFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
	DXGI_FORMAT s_DepthStencilFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;

	//root signature feature data
	D3D12_FEATURE_DATA_ROOT_SIGNATURE featureData = {};
}

namespace Graphics
{
	//Can ComPtr use as global pointer?
	//Create DirectX12 Device ,Factory and Adaptor of the most powerful(biggest memory) GPU
	inline void CreateDevice()
	{
		UINT createFactoryFlags = 0;
#if defined(_DEBUG)
		ComPtr<ID3D12Debug> debugController;
		ThrowIfFailed(D3D12GetDebugInterface(IID_PPV_ARGS(debugController.GetAddressOf())));
		debugController->EnableDebugLayer();
		createFactoryFlags |= DXGI_CREATE_FACTORY_DEBUG;
#endif

		ThrowIfFailed(CreateDXGIFactory2(createFactoryFlags, IID_PPV_ARGS(g_Factory.GetAddressOf())));
		ComPtr<IDXGIAdapter1> dxgiAdapterTmp;
		SIZE_T maxDedicatedVideoMemory = 0;
		for (UINT i = 0; g_Factory->EnumAdapters1(i, &dxgiAdapterTmp) != DXGI_ERROR_NOT_FOUND; ++i)
		{
			DXGI_ADAPTER_DESC1 ad_desc;
			dxgiAdapterTmp->GetDesc1(&ad_desc);
#if defined(_DEBUG)
			std::wcout << std::wstring(ad_desc.Description) << " Memory: "
				<< (ad_desc.DedicatedVideoMemory >> 20) << "MB" << std::endl;
#endif
			if (SUCCEEDED(D3D12CreateDevice(dxgiAdapterTmp.Get(), D3D_FEATURE_LEVEL_11_0, __uuidof(ID3D12Device), nullptr)) &&
				ad_desc.DedicatedVideoMemory > maxDedicatedVideoMemory)
			{
				maxDedicatedVideoMemory = ad_desc.DedicatedVideoMemory;
				ThrowIfFailed(dxgiAdapterTmp.As(&g_Adaptor));
			}
		}

#if defined(_DEBUG)
		DXGI_ADAPTER_DESC1 ad_desc;
		g_Adaptor->GetDesc1(&ad_desc);
		std::wcout << "Create the DirectX12 Device use the GPU : " << std::wstring(ad_desc.Description) << std::endl;
#endif

		ThrowIfFailed(D3D12CreateDevice(g_Adaptor.Get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(g_Device.GetAddressOf())));
		NAME_D3D12_OBJECT(g_Device);

	}

	//Check if the Factory support tearing
	inline bool CheckTearingSupport()
	{
		BOOL bAllowTearing = FALSE;

		if (FAILED(g_Factory->CheckFeatureSupport(
			DXGI_FEATURE_PRESENT_ALLOW_TEARING, &bAllowTearing, sizeof(bAllowTearing))))
		{
			bAllowTearing = FALSE;
		}
#if defined(_DEBUG)
		std::wcout << L"Tearing support: ";
		if (bAllowTearing == TRUE)
			std::wcout << L"Yes!";
		else
			std::wcout << L"No!";
#endif
		return bAllowTearing == TRUE;
	}

	//initialize the DirectX12 command queues: direct,compute,copy
	//(maybe use the direct replace the copy,
	//because direct command queue can do everything the copy can do)
	//--
	//initialize command list,this is tmp command list management (just show it directly
	//when next refactor ,and understand more obout command objects of directX12
	//encapsulate it and make it more convenient to use
	inline void InitializeCommandObjects()
	{
		//command queues
		D3D12_COMMAND_QUEUE_DESC cmd_queue_desc = {};
		cmd_queue_desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
		cmd_queue_desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
		ThrowIfFailed(g_Device->CreateCommandQueue(&cmd_queue_desc, IID_PPV_ARGS(g_GraphicsQueue.GetAddressOf())));
		NAME_D3D12_OBJECT(g_GraphicsQueue);
		cmd_queue_desc.Type = D3D12_COMMAND_LIST_TYPE_COMPUTE;
		ThrowIfFailed(g_Device->CreateCommandQueue(&cmd_queue_desc, IID_PPV_ARGS(g_ComputeQueue.GetAddressOf())));
		NAME_D3D12_OBJECT(g_ComputeQueue);
		cmd_queue_desc.Type = D3D12_COMMAND_LIST_TYPE_COPY;
		ThrowIfFailed(g_Device->CreateCommandQueue(&cmd_queue_desc, IID_PPV_ARGS(g_CopyQueue.GetAddressOf())));
		NAME_D3D12_OBJECT(g_CopyQueue);

		//command allocator
		ThrowIfFailed(g_Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(g_GraphicsAlloc.GetAddressOf())));
		NAME_D3D12_OBJECT(g_GraphicsAlloc);
		ThrowIfFailed(g_Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_COMPUTE, IID_PPV_ARGS(g_ComputeAlloc.GetAddressOf())));
		NAME_D3D12_OBJECT(g_ComputeAlloc);
		ThrowIfFailed(g_Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_COPY, IID_PPV_ARGS(g_CopyAlloc.GetAddressOf())));
		NAME_D3D12_OBJECT(g_CopyAlloc);

		//command list
		ThrowIfFailed(g_Device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT,
			g_GraphicsAlloc.Get(), nullptr, IID_PPV_ARGS(g_GraphicsCommandList.GetAddressOf())));
		NAME_D3D12_OBJECT(g_GraphicsCommandList);
		ThrowIfFailed(g_Device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_COMPUTE,
			g_ComputeAlloc.Get(), nullptr, IID_PPV_ARGS(g_ComputeCommandList.GetAddressOf())));
		NAME_D3D12_OBJECT(g_ComputeCommandList);
		ThrowIfFailed(g_Device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_COPY,
			g_CopyAlloc.Get(), nullptr, IID_PPV_ARGS(g_CopyCommandList.GetAddressOf())));
		NAME_D3D12_OBJECT(g_CopyCommandList);

		//fence
		ThrowIfFailed(g_Device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(g_GraphicsFence.GetAddressOf())));
		NAME_D3D12_OBJECT(g_GraphicsFence);
		ThrowIfFailed(g_Device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(g_ComputeFence.GetAddressOf())));
		NAME_D3D12_OBJECT(g_ComputeFence);
		ThrowIfFailed(g_Device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(g_CopyFence.GetAddressOf())));
		NAME_D3D12_OBJECT(g_CopyFence);

	}

	//create swapchain
	inline void CreateSwapChain()
	{
		g_SwapChain.Reset();

		auto width = ResolutionOptions[g_ResolutionIndex].Width;
		auto height = ResolutionOptions[g_ResolutionIndex].Height;

		DXGI_SWAP_CHAIN_DESC1 sc_desc = {};
		sc_desc.Width = static_cast<UINT>(width);
		sc_desc.Height = static_cast<UINT>(height);
		sc_desc.Format = s_BackBufferFormat;
		sc_desc.Stereo = FALSE;
		sc_desc.SampleDesc.Count = 1;
		sc_desc.SampleDesc.Quality = 0;
		sc_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sc_desc.BufferCount = FrameCount;
		sc_desc.Scaling = DXGI_SCALING_STRETCH;		//mark for more learning
		sc_desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
		sc_desc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
		sc_desc.Flags = bIsTearingSupport ? DXGI_SWAP_CHAIN_FLAG_ALLOW_TEARING : 0;

		HWND hwnd = WindowApplication::GetHandle();
		ComPtr<IDXGISwapChain1> swapChain1;
		ThrowIfFailed(g_Factory->CreateSwapChainForHwnd(
			g_GraphicsQueue.Get(),
			hwnd,
			&sc_desc,
			nullptr,
			nullptr,
			swapChain1.GetAddressOf()));

		// Disable the Alt+Enter fullscreen
		ThrowIfFailed(g_Factory->MakeWindowAssociation(hwnd, DXGI_MWA_NO_ALT_ENTER));

		ThrowIfFailed(swapChain1.As(&g_SwapChain));
	}

	//get dscriptor sizes
	inline void GetDescriptorSizes()
	{
		g_DescriptorSizeRtv = g_Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
		g_DescriptorSizeDsv = g_Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
		g_DescriptorSizeCbvSrvUav = g_Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
		g_DescriptorSizeSampler = g_Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER);
	}

	//create descriptor heap for render target vie and depth stencil view
	inline void CreateRtvDsvDescriptorHeap()
	{
		D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
		rtvHeapDesc.NumDescriptors = FrameCount;
		rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
		rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		rtvHeapDesc.NodeMask = 0;
		ThrowIfFailed(g_Device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(g_RtvHeap.GetAddressOf())));
		NAME_D3D12_OBJECT(g_RtvHeap);

		D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc = {};
		dsvHeapDesc.NumDescriptors = 1;
		dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
		dsvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		dsvHeapDesc.NodeMask = 0;
		ThrowIfFailed(g_Device->CreateDescriptorHeap(&dsvHeapDesc, IID_PPV_ARGS(g_DsvHeap.GetAddressOf())));
		NAME_D3D12_OBJECT(g_DsvHeap);
	}

	//load size dependent resource
	inline void LoadSizeDependentResources()
	{
		auto width = ResolutionOptions[g_ResolutionIndex].Width;
		auto height = ResolutionOptions[g_ResolutionIndex].Height;

		for (size_t i = 0; i < FrameCount; i++)
		{
			g_RenderTargets[i].Reset();
		}
		g_DepthStencilView.Reset();

		//resize swap chain
		DXGI_SWAP_CHAIN_DESC desc = {};
		g_SwapChain->GetDesc(&desc);
		ThrowIfFailed(g_SwapChain->ResizeBuffers(FrameCount, 
			static_cast<UINT>(width), static_cast<UINT>(height), desc.BufferDesc.Format, desc.Flags));

		BOOL fullscreenState;
		ThrowIfFailed(g_SwapChain->GetFullscreenState(&fullscreenState, nullptr));
		bIsWindowMode = !fullscreenState;

		g_CurrentBackBuffer = g_SwapChain->GetCurrentBackBufferIndex();

		//create resource for back buffer and dsv
		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(g_RtvHeap->GetCPUDescriptorHandleForHeapStart());
		UINT rtvDescriptorSize = g_Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
		for (UINT i = 0; i < FrameCount; i++)
		{
			ThrowIfFailed(g_SwapChain->GetBuffer(i, IID_PPV_ARGS(g_RenderTargets[i].GetAddressOf())));
			g_Device->CreateRenderTargetView(g_RenderTargets[i].Get(), nullptr, rtvHandle);
			rtvHandle.Offset(1, rtvDescriptorSize);
		}

		D3D12_RESOURCE_DESC depthStencilDesc;
		depthStencilDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
		depthStencilDesc.Alignment = 0;
		depthStencilDesc.Width = width;
		depthStencilDesc.Height = height;
		depthStencilDesc.DepthOrArraySize = 1;
		depthStencilDesc.MipLevels = 1;
		depthStencilDesc.Format = s_DepthStencilFormat;
		depthStencilDesc.SampleDesc.Count = 1;
		depthStencilDesc.SampleDesc.Quality = 0;
		depthStencilDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
		depthStencilDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;


		D3D12_CLEAR_VALUE optimizedClearValue = {};
		optimizedClearValue.Format = s_DepthStencilFormat;
		optimizedClearValue.DepthStencil = { 1.0f, 0 };


		ThrowIfFailed(g_Device->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&depthStencilDesc,
			D3D12_RESOURCE_STATE_COMMON,
			&optimizedClearValue,
			IID_PPV_ARGS(g_DepthStencilView.GetAddressOf())
		));

		// Update the depth-stencil view.
		D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc = {};
		dsvDesc.Flags = D3D12_DSV_FLAG_NONE;
		dsvDesc.Format = s_DepthStencilFormat;
		dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
		dsvDesc.Texture2D.MipSlice = 0;

		g_Device->CreateDepthStencilView(g_DepthStencilView.Get(), &dsvDesc,
			g_DsvHeap->GetCPUDescriptorHandleForHeapStart());

		g_GraphicsCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(g_DepthStencilView.Get(),
			D3D12_RESOURCE_STATE_COMMON, D3D12_RESOURCE_STATE_DEPTH_WRITE));

		ThrowIfFailed(g_GraphicsCommandList->Close());
		ID3D12CommandList* cmdsLists[] = { g_GraphicsCommandList.Get() };
		g_GraphicsQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);

		FlushCommandQueue(g_GraphicsFenceValue, g_GraphicsFence.Get(), g_GraphicsQueue.Get());

		//reset commandlist
		ThrowIfFailed(g_GraphicsCommandList->Reset(g_GraphicsAlloc.Get(), nullptr));

		g_Viewport.TopLeftX = 0;
		g_Viewport.TopLeftY = 0;
		g_Viewport.Width = static_cast<float>(width);
		g_Viewport.Height = static_cast<float>(height);
		g_Viewport.MinDepth = 0.0f;
		g_Viewport.MaxDepth = 1.0f;

		g_ScissorRect.left = 0;
		g_ScissorRect.right = width;
		g_ScissorRect.top = 0;
		g_ScissorRect.bottom = height;
	}

	//when create root signatures
	//check if support version 1.1
	inline void CheckRootSigFeatureData()
	{
		featureData.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_1;
		if (FAILED(g_Device->CheckFeatureSupport(D3D12_FEATURE_ROOT_SIGNATURE, &featureData, sizeof(featureData))))
		{
			featureData.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_0;
		}
	}
}

void Graphics::Initialize()
{
	CreateDevice();
	InitializeCommandObjects();
	bIsTearingSupport = CheckTearingSupport();
	CheckRootSigFeatureData();
	CreateSwapChain();
	GetDescriptorSizes();
	CreateRtvDsvDescriptorHeap();
	LoadSizeDependentResources();
}

void Graphics::Finalize(void)
{
	FlushAllCommandQueues();
}

void Graphics::ChangeResolution(uint32_t resolutionIndex)
{
	if ((resolutionIndex == g_ResolutionIndex) || (resolutionIndex >= kResolutionOptionCount))
	{
		return;
	}
	else
	{
		g_ResolutionIndex = resolutionIndex;
		LoadSizeDependentResources();
	}
}

ID3D12Device2* Graphics::GetDevice()
{
	return g_Device.Get();
}

uint32_t Graphics::GetWidth()
{
	return ResolutionOptions[g_ResolutionIndex].Width;
}

uint32_t Graphics::GetHeight()
{
	return ResolutionOptions[g_ResolutionIndex].Height;
}

bool Graphics::GetTearingSupport()
{
	return bIsTearingSupport;
}

D3D12_FEATURE_DATA_ROOT_SIGNATURE& Graphics::GetRootSigFeature()
{
	return featureData;
}

DXGI_FORMAT Graphics::GetBackBufferFormat()
{
	return s_BackBufferFormat;
}

DXGI_FORMAT Graphics::GetDepthStencilFormat()
{
	return s_DepthStencilFormat;
}

IDXGISwapChain4* Graphics::GetSwapChain()
{
	return g_SwapChain.Get();
}

ID3D12CommandQueue* Graphics::GetCommandQueue(D3D12_COMMAND_LIST_TYPE type)
{
	switch (type)
	{
	case D3D12_COMMAND_LIST_TYPE_COMPUTE:
		return g_ComputeQueue.Get();
	case D3D12_COMMAND_LIST_TYPE_COPY:
		return g_CopyQueue.Get();
	default:
		return g_GraphicsQueue.Get();
	}
}

ID3D12CommandAllocator* Graphics::GetCommandAlloc(D3D12_COMMAND_LIST_TYPE type)
{
	switch (type)
	{
	case D3D12_COMMAND_LIST_TYPE_COMPUTE:
		return g_ComputeAlloc.Get();
	case D3D12_COMMAND_LIST_TYPE_COPY:
		return g_CopyAlloc.Get();
	default:
		return g_GraphicsAlloc.Get();
	}
}

ID3D12GraphicsCommandList* Graphics::GetCommandList(D3D12_COMMAND_LIST_TYPE type)
{
	switch (type)
	{
	case D3D12_COMMAND_LIST_TYPE_COMPUTE:
		return g_ComputeCommandList.Get();
	case D3D12_COMMAND_LIST_TYPE_COPY:
		return g_CopyCommandList.Get();
	default:
		return g_GraphicsCommandList.Get();
	}
}

ID3D12Fence* Graphics::GetFence(D3D12_COMMAND_LIST_TYPE type)
{
	switch (type)
	{
	case D3D12_COMMAND_LIST_TYPE_COMPUTE:
		return g_ComputeFence.Get();
	case D3D12_COMMAND_LIST_TYPE_COPY:
		return g_CopyFence.Get();
	default:
		return g_GraphicsFence.Get();
	}
}

ID3D12Resource* Graphics::GetCurrentRenderTarget()
{
	return g_RenderTargets[g_CurrentBackBuffer].Get();
}

ID3D12DescriptorHeap* Graphics::GetRtvHeap()
{
	return g_RtvHeap.Get();
}

ID3D12DescriptorHeap* Graphics::GetDsvHeap()
{
	return g_DsvHeap.Get();
}

CD3DX12_VIEWPORT Graphics::GetViewPort()
{
	return g_Viewport;
}

CD3DX12_RECT Graphics::GetScissorRect()
{
	return g_ScissorRect;
}

void Graphics::FlushCommandQueue(uint64_t& fenceValue, ID3D12Fence* fence, ID3D12CommandQueue* cmdQueue)
{
	fenceValue++;

	ThrowIfFailed(cmdQueue->Signal(fence, fenceValue));

	if (fence->GetCompletedValue() < fenceValue)
	{
		HANDLE eventHandle = CreateEventEx(NULL, FALSE, FALSE, EVENT_ALL_ACCESS);
		ThrowIfFailed(fence->SetEventOnCompletion(fenceValue, eventHandle));

		::WaitForSingleObject(eventHandle, INFINITE);
		::CloseHandle(eventHandle);
	}
}

void Graphics::FlushAllCommandQueues()
{
	FlushCommandQueue(g_GraphicsFenceValue, g_GraphicsFence.Get(), g_GraphicsQueue.Get());
	FlushCommandQueue(g_ComputeFenceValue, g_ComputeFence.Get(), g_ComputeQueue.Get());
	FlushCommandQueue(g_CopyFenceValue, g_CopyFence.Get(), g_CopyQueue.Get());
}

