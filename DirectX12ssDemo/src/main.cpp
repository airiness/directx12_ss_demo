#include "pch.h"
#include "DirectX12ssDemo.h"

namespace
{
	std::wstring g_windowTitle{ L"DirectX12 is hard,please never give up@!" };
}

int main(int, char**)
{
	std::wcout << L"Hello the DirectX12 Interesting and Challenge World!" << std::endl;
	HINSTANCE hInstance = GetModuleHandle(nullptr);
	DirectX12ssDemo ssDemo(g_windowTitle.c_str());
	WindowApplication::Run(&ssDemo, hInstance, SW_SHOWDEFAULT);
	return 0;
}