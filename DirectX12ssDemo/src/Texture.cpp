#include "pch.h"
#include "Texture.h"
#include "DirectX12Graphics.h"

Texture::Textures* Texture::pTexturesManage = nullptr;

void Texture::LoadTextures(Textures& textures, ID3D12DescriptorHeap** descriptorHeap)
{
	auto device = Graphics::GetDevice();
	auto commandList = Graphics::GetCommandList();
	pTexturesManage = &textures;
	
	struct tex { std::string texName; std::wstring texRoute; };
	std::vector<tex> textureNames =
	{
		{"whiteStoneTex",			L"assets/textures/WhiteStone.dds"},
		{"broneStoneTex",			L"assets/textures/BroneStone.dds"},
		{"woodFloorTex",			L"assets/textures/WoodFloor.dds"},
		{"transparentFlowerTex",	L"assets/textures/TransparentFlower.dds"},
		{"waveWaterTex",			L"assets/textures/WaveWater.dds"},
		{"whiteMirrorTex",			L"assets/textures/WhiteMirror.dds"}
	};

	for (auto& n : textureNames)
	{
		auto texP = std::make_unique<Texture>();
		texP->Name = n.texName;
		texP->Filename = n.texRoute;
		ThrowIfFailed(LoadDDSTextureFromFile(device, texP->Filename.c_str(), 
			texP->TextureResource.ReleaseAndGetAddressOf(), texP->TextureData, texP->SubResources));

		const UINT64 uploadBufferSize = GetRequiredIntermediateSize(texP->TextureResource.Get(), 0,
			static_cast<UINT>(texP->SubResources.size()));

		ThrowIfFailed(
			device->CreateCommittedResource(
				&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
				D3D12_HEAP_FLAG_NONE,
				&CD3DX12_RESOURCE_DESC::Buffer(uploadBufferSize),
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(texP->TextureResourceUpload.GetAddressOf())));

		UpdateSubresources(commandList, texP->TextureResource.Get(), texP->TextureResourceUpload.Get(),
			0, 0, static_cast<UINT>(texP->SubResources.size()), texP->SubResources.data());
		commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(texP->TextureResource.Get(),
			D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));

		textures.push_back(std::move(texP));
	};

	D3D12_DESCRIPTOR_HEAP_DESC srv_heap_desc = {};
	srv_heap_desc.NumDescriptors = static_cast<UINT>(textures.size());
	srv_heap_desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	srv_heap_desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	ThrowIfFailed(device->CreateDescriptorHeap(&srv_heap_desc, IID_PPV_ARGS(descriptorHeap)));

	//descript ths textures in descriptor table
	CD3DX12_CPU_DESCRIPTOR_HANDLE hTexDescriptor((*descriptorHeap)->GetCPUDescriptorHandleForHeapStart());
	
	for (auto& t : textures)
	{
		D3DUtility::CreateSRVForTexture(device, t->TextureResource, hTexDescriptor);
		hTexDescriptor.Offset(1, Graphics::g_DescriptorSizeCbvSrvUav);
	}

}
