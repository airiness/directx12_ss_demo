//particle class 
//zhang ruisong
#pragma once
#include "IObject.h"
#include "UploadBuffer.h"

//index for the descriptor heaps of particle
enum ParticleDescriptorHeapIndex : uint32_t
{
	ParticleUav0 = 0,
	ParticleUav1,
	ParticleSrv0,
	ParticleSrv1,
	ParticleDescriptorCount
};

enum ParticleDrawRootParameters : uint32_t
{
	ParticleDrawCBV = 0,
	ParticleDrawSRVTable,
	ParticleDrawRootParametersCount
};

enum ParticleCSRootParameters : uint32_t
{
	ParticleCSCBV = 0,
	ParticleCSSRVTable,
	ParticleCSUAVTable,
	ParticleCSParametersCount
};

struct ParticleDrawConstants
{
	Matrix WorldViewProjection;
	Matrix InverseView;
};

struct ParticleVertex
{
	Vector4 color = { 0.0f,0.0f,0.0f,1.0f };
};

struct ParticleVertexBuffer
{
	ComPtr<ID3D12Resource> ParticleVertexResource;
	ComPtr<ID3D12Resource> ParticleVertexResourceUpload;
	D3D12_VERTEX_BUFFER_VIEW ParticleVertexBufferView;
};

struct ParticleData
{
	Vector4 position = { 0.0f,0.0f,0.0f,0.0f };	//16 bytes
	Vector4 velocity = { 0.0f,0.0f,0.0f,0.0f };	//16 bytes
	uint32_t isUse = 0;							//true
	Vector3 paddings = { 0.0f,0.0f,0.0f };		//4+12 bytes
};

struct ParticleDataBuffer
{
	ComPtr<ID3D12Resource> ParticleDataResource;
	ComPtr<ID3D12Resource> ParticleDataResourceUpload;
};

struct ParticleCSConstants
{
	uint32_t param[4];	//16 bytes
	float paramf[4];	//16 bytes
};

struct ParticleCSConstantsBuffer
{
	uint8_t* MappedData = nullptr;
	ComPtr<ID3D12Resource> ParticleCSConstantResource;
	ComPtr<ID3D12Resource> ParticleCSConstantResourceUpload;
};

class Particle : implements IObject
{
public:
	Particle();
	~Particle();

	virtual void Initialize() override;
	virtual void Update(double dt) override;
	virtual void Compute() override;
	virtual void Render() override;
	virtual void Finalize() override;

	void BuildParticleDatasGPU(DescriptorHeapsManage& descriptorHeaps);
	void BuildParticleRootSignature(RootSignaturesManage& rootSignatures);
	void BuildParticlePSO(PSOsManage& PSOs,	ShadersManage& shaders, InputLayoutsManage& inputLayouts);

	void SetWireFrame(bool isWireFrame) { m_IsWireframe = isWireFrame; }

	ParticleCSConstants ParticleCSConstantsData;
	ParticleDrawConstants ParticleDrawCBForEdit;
	std::unique_ptr<UploadBuffer<ParticleCSConstants>> ParticleCSCB;
private:
	bool m_IsWireframe = false;

	std::vector<ParticleDataBuffer> m_ParticleDataBuffers;
	ParticleVertexBuffer m_ParticleVBuffer;

	uint32_t m_SrvIndex = 0;
	uint32_t m_ParticleCount = 1;
	uint32_t m_ParticleIndex = 0;
	uint32_t m_particleDirtyFlag = 0;
	Vector3 m_Center = { 0.0f,20.0f,0.0f };
	Vector4 m_SpeedBase = { 0.0f,-1.0f,0.0f,1 / 100000000.0f };
	float m_Spread = 2000.0f;

	std::vector<ParticleVertex> m_ParticleVertices;
	std::vector<ParticleData> m_ParticleDatas;

	//save pointers of DirectX objects
	HANDLE m_ComputeEventHandle = nullptr;
	uint64_t m_ParticleComputeFenceValue = 0;
	ID3D12DescriptorHeap* m_pDescriptorHeap = nullptr;
	ID3D12RootSignature* m_pRootSignatureDraw = nullptr;
	ID3D12RootSignature* m_pRootSigCompute = nullptr;
	ID3D12PipelineState* m_pPsoDraw = nullptr;
	ID3D12PipelineState* m_pPsoWireframeDraw = nullptr;
	ID3D12PipelineState* m_pPsoCompute = nullptr;
};