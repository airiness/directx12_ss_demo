//material class
//zhang ruisong
#pragma once
struct Material
{
	//using static funcs to management the resources
	using Materials = std::unordered_map<std::string, std::unique_ptr<Material>>;
	//initialize materials
	static void BuildMaterials(Materials& materials);
	//the pointer copy of material resource unorderdmap management
	static Materials* pMaterialsManage;
	static Materials* GetMaterials() { return pMaterialsManage; };

	//material name
	std::string Name;

	int MaterialCBIndex = -1;
	//the offset(position) of the resource in descriptor heap
	int DiffuseSrvHeapIndex = -1;
	int NormalSrvHeapIndex = -1;
	int NumFramesDirty = 0; //dirty flag for update or not

	Vector4 DiffuseAlbedo = { 1.0f,1.0f,1.0f,1.0f };
	Vector3 FresnelR0 = { 0.01f,0.01f,0.01f };
	float Roughness = 0.25f;
	Matrix MaterialTransform = Matrix::Identity;
};