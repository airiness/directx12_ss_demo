#pragma once
#include "pch.h"

namespace Graphics
{
	extern const uint32_t FrameCount;
	extern uint32_t g_CurrentBackBuffer;
	extern UINT g_DescriptorSizeRtv;
	extern UINT g_DescriptorSizeDsv;
	extern UINT g_DescriptorSizeCbvSrvUav;
	extern UINT g_DescriptorSizeSampler;

	extern uint64_t g_GraphicsFenceValue;
	extern uint64_t g_ComputeFenceValue;
	extern uint64_t g_CopyFenceValue;

	void Initialize();
	void Finalize(void);
	void ChangeResolution(uint32_t resolutionIndex);

	ID3D12Device2* GetDevice();
	uint32_t GetWidth();
	uint32_t GetHeight();
	bool GetTearingSupport();
	D3D12_FEATURE_DATA_ROOT_SIGNATURE& GetRootSigFeature();
	DXGI_FORMAT GetBackBufferFormat();
	DXGI_FORMAT GetDepthStencilFormat();
	IDXGISwapChain4* GetSwapChain();
	ID3D12CommandQueue* GetCommandQueue(D3D12_COMMAND_LIST_TYPE type = D3D12_COMMAND_LIST_TYPE_DIRECT);
	ID3D12CommandAllocator* GetCommandAlloc(D3D12_COMMAND_LIST_TYPE type = D3D12_COMMAND_LIST_TYPE_DIRECT);
	ID3D12GraphicsCommandList* GetCommandList(D3D12_COMMAND_LIST_TYPE type = D3D12_COMMAND_LIST_TYPE_DIRECT);
	ID3D12Fence* GetFence(D3D12_COMMAND_LIST_TYPE type = D3D12_COMMAND_LIST_TYPE_DIRECT);
	ID3D12Resource* GetCurrentRenderTarget();
	ID3D12DescriptorHeap* GetRtvHeap();
	ID3D12DescriptorHeap* GetDsvHeap();
	CD3DX12_VIEWPORT GetViewPort();
	CD3DX12_RECT GetScissorRect();

	void FlushCommandQueue(uint64_t& fenceValue,ID3D12Fence* fence, ID3D12CommandQueue* cmdQueue);
	void FlushAllCommandQueues();


}