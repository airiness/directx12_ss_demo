#pragma once
#include "Material.h"
#include "Mesh.h"

struct RenderItem
{
	using RenderItemsManage = std::unordered_map<std::string, std::unique_ptr<RenderItem>>;

	RenderItem() = default;
	RenderItem(const Matrix worldMat, Matrix texTrans, int framesDirty, UINT objCBIndex,
		Material* pMat, MeshGeometry* meshGeo,
		UINT indexCount, UINT startIndex, int baseVertex,
		D3D12_PRIMITIVE_TOPOLOGY topology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST) :
		WorldMatrix(worldMat), TexTransform(texTrans), NumFramesDirty(framesDirty), ObjCBIndex(objCBIndex),
		RenderItemMaterial(pMat), RenderItemMeshGeomtry(meshGeo),
		IndexCount(indexCount), StartIndexLocation(startIndex), BaseVertexLocation(baseVertex),
		PrimitiveType(topology) {};
	RenderItem(const RenderItem&) = delete;
	
	Matrix WorldMatrix = Matrix::Identity;	//position, rotation and scale
	Matrix TexTransform = Matrix::Identity;

	int NumFramesDirty = 0;
	UINT ObjCBIndex = -1;

	Material* RenderItemMaterial = nullptr;
	MeshGeometry* RenderItemMeshGeomtry = nullptr;

	D3D12_PRIMITIVE_TOPOLOGY PrimitiveType = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	UINT IndexCount = 0;
	UINT StartIndexLocation = 0;
	int BaseVertexLocation = 0;
};