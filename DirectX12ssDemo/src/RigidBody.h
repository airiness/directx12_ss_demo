//rigidbody class
//zhang ruisong

#pragma once
#include "IObject.h"
#include "UploadBuffer.h"
#include "Camera.h"

//render root parameters
enum RigidBodyDrawRootParametersIndex : uint32_t
{
	RigidBodyDrawRootParaCBV,
	RigidBodyDrawRootParaSRVTable,
	RigidBodyDrawRootParaCount
};

//compute root parameters
enum RigidBodyComputeRootParametersIndex : uint32_t
{
	RigidBodyComputeRootParaCBV,
	RigidBodyComputeRootParaSRVTable,
	RigidBodyComputeRootParaUAVTable,
	RigidBodyComputeRootParaDrawUAVTable,
	RigidBodyComputeRootParaCount,
};

//descriptor heap indices
enum RigidBodyDescriptorHeapIndex : uint32_t
{
	RigidBodyUav0 = 0,
	RigidBodyUav1,
	RigidBodyUav2,
	RigidBodySrv0,
	RigidBodySrv1,
	RigidBodySrv2,
	RigidBodyUavCompute0,
	RigidBodyUavCompute1,
	RigidBodySrvCompute0,
	RigidBodySrvCompute1,
	RigidBodyDescriptorCount
};

//constant buffer,update every frame
struct RigidBodyComputeConstants
{
	//for data padding, not put in order
	//gravity
	Vector3 gravity;
	//body count
	uint32_t bodyCount;		//16

	//add new body position
	Vector3 addBodyPosition;
	//active body Index(count now)
	uint32_t bodyIndex;		//16

	//add new body velocity
	Vector3 addBodyVelocity;
	//as a flag to control add a new body ?1:true|0:false
	uint32_t isAddNewBody;	//16

	//add new body angular velocity
	Vector3 addBodyAngularVelocity;
	//thread goup count
	uint32_t threadGroupCount;	//16

	//add new body bodysize
	Vector3 addBodySize;
	// debug mode flag
	uint32_t isDebugMode;		//16

	//delta time
	float deltaTime;
	//body density
	float bodyDensity;
	//body linear Damping 
	float linearDamping = 0.95f;
	//body angularDamping
	float angularDamping = 1.000f;		//16

	//gravity center
	Vector3 gravityCenter;
	//distributing spread
	float spread;				//16

	//render all?
	uint32_t isAllActive;
	//gravity mode or center mode ?
	uint32_t isCenterMode;
	//reset? 
	uint32_t resetFlag;
	float paddings;			//16

	//cube move flags
	//for collision debug using
	uint32_t moveflags[8] = { 0,0,0,0,0,0,0,0 };
	uint32_t rotateflags[8] = { 0,0,0,0,0,0,0,0 }; //16x4
};

//draw constant buffer
struct RigidBodyDrawConstants
{
	//mvp matrix
	Matrix WorldViewProjection = Matrix::Identity;
	//camera position
	Vector3 CameraPosition = { 0.0f,0.0f,0.0f };
	float Padding = 0.0f;
};

//manage the gpu resource
struct RigidBodyGpuResource
{
	ComPtr<ID3D12Resource> RigidBodyDataReource;
	ComPtr<ID3D12Resource> RigidBodyDataReourceUpload;
};

//ridigbody datas for compute
struct RigidBodyDataCompute
{
	Vector3 Position = { 1.0f,1.0f,1.0f };
	float Mass = 0.0f;
	Vector3 BodySize = { 0.0f,0.0f,0.0f };
	float InverseMass = 0.0f;
	Vector4 Orientation = { 0.0f,0.0f,0.0f,1.0f };
	Vector3 Velocity = { 0.0f,0.0f,0.0f };
	//is the body contact
	uint32_t isContact = 0;
	Vector3 AngularVelocity = { 0.0f,0.0f,0.0f };
	//is the body static
	uint32_t isStatic = 0;
	Vector3 Acceleration = { 0.0f,0.0f,0.0f };
	uint32_t isUse = 0;
	Vector3 Paddings0 = { 0.0f,0.0f,0.0f };
	uint32_t id = 0;
};

//rigidbody datas for draw
struct RigidBodyDataDraw
{
	Vector3 position = { 0.0f,0.0f,0.0f };
	//static or cantact body have different color
	uint32_t isStatic = 0;
	Vector3 bodySize = { 0.0f,0.0f,0.0f };
	uint32_t isContact = 0;
	Quaternion quaternion = { 0.0f,0.0f,0.0f,0.0f };
};

class RigidBody : implements IObject
{
public:
	virtual void Initialize() override;
	virtual void Update(double dt) override;
	virtual void Compute() override;
	virtual void Render() override;
	virtual void Finalize() override;

	void GetCamera(Camera* pCam);

	void BuildRigidBodyDatasGPU(DescriptorHeapsManage& descriptorHeaps);
	void BuildRigidBodyRootSignature(RootSignaturesManage& rootSignatures);
	void BuildRigidBodyPSO(PSOsManage& PSOs, ShadersManage& shaders, InputLayoutsManage& inputLayouts);

	void SetWireframe(bool wireframe);

	//when finished the intialize ,release the cpu memory resource
	//should write a memory mangement future
	void ReleaseCpuResource();
private:
	//rigidbody properties

	const float m_density = 1.0f;
	//default speed of shooting body
	const float m_shootingSpeed = 25.0f;
	//default size of shooting body
	const Vector3 m_shootbodySize = { 0.3f,0.5f,0.6f };
private:

private:
	//state properties
	const float m_bodySpread = 60.0f;
	//debug mode flag
	bool m_IsDebugMode = false;
	//center gravity mode flag
	bool m_IsCenterMode = false;
	//render all body flag
	bool m_IsActiveAll = true;
	//rendering?
	bool m_IsRendering = true;
	//the index of shoot body
	uint32_t m_RigidBodyIndex;
	//the index of drawing count
	uint32_t m_RigidBodyDrawCount;
	//shoot body begin index
	const uint32_t m_rRgidBodyStaticLimit = 1000;
	//the max count of rigidbodies
	const uint32_t m_RigidBodyMaxCount = 20000;
	//the thread count of a group same with shader Block_Size
	const uint32_t m_RigidBodyGpuGroupThreadCount = 128;
private:
	std::vector<RigidBodyDataCompute> m_CpuDatasCompute;
	std::vector<RigidBodyGpuResource> m_GpuResources;

	std::unique_ptr<UploadBuffer<RigidBodyComputeConstants>> m_computeConstantResource;
	std::vector<std::unique_ptr<UploadBuffer<RigidBodyDrawConstants>>> m_drawConstantResources;
	RigidBodyComputeConstants m_computeConstantsBufferEdit;
	RigidBodyDrawConstants m_drawConstantsBufferEdit;

	uint32_t m_constantBufferChangedDirtyFlag = 0;
private:
	bool m_IsWireframe = false;

	uint32_t m_srvIndex = 0;

	//command and sync objects
	uint64_t m_fenceValue = 0;
	ComPtr<ID3D12Fence> m_computeFence;
	HANDLE m_computeEventHandle;
	ComPtr<ID3D12CommandAllocator> m_computeCommandAlloc;

	//save the resource pointers,manage the resource by resource managers(onwership) ,just save pointers here
	ID3D12DescriptorHeap* m_pDesciptorHeap = nullptr;
	ID3D12RootSignature* m_pRootSigDraw = nullptr;
	ID3D12RootSignature* m_pRootSigCompute = nullptr;
	ID3D12PipelineState* m_pPsoDraw = nullptr;
	ID3D12PipelineState* m_pPsoWireframeDraw = nullptr;
	ID3D12PipelineState* m_pPsoCompute = nullptr;

	Camera* m_pCamera = nullptr;
};