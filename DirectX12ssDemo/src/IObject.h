//the interface of game object
//zhang ruisong
#include "ResourcesManage.h"

#pragma once
#define Interface class
#define implements public

Interface IObject
{
public:
	IObject() {};
	virtual ~IObject() {};

	virtual void Initialize() = 0;
	virtual void Update(double dt) = 0;
	virtual void Compute() = 0;
	virtual void Render() = 0;
	virtual void Finalize() = 0;

private:

};