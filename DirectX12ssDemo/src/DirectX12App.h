#pragma once
#include "WindowApplication.h"
#include "DirectX12Graphics.h"

class DirectX12App
{
public:
	DirectX12App(std::wstring title);
	virtual ~DirectX12App();

	virtual void Initialize() = 0;
	virtual void Update(double dt) = 0;
	virtual void Render() = 0;
	virtual void Resize(size_t resolutionIndex, bool minimized) = 0;
	virtual void WindowMoved(int x, int y) {};
	virtual uint32_t GetWidth() = 0;
	virtual uint32_t GetHeight() = 0;
	virtual void Finalize() = 0;

	const WCHAR* GetTitle() const { return m_title.c_str(); };
	virtual bool GetTearingSupport() = 0;

	virtual IDXGISwapChain* GetSwapchain() { return nullptr; };

	void SetAppTitle(const std::wstring& title) { m_title = title; };
	void SetWindowBounds(int left, int top, int right, int bottom);

protected:
	RECT m_windowBounds;

private:
	std::wstring m_title;
};