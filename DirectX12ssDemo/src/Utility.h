//helper code of d3d12 applications
//zhang ruisong 
//some code from microsoft examples

#pragma once
#include <windows.h>
#include <string>
#include <stdexcept>
#include <random>
#include <wrl.h>
using Microsoft::WRL::ComPtr;
#include <d3dx12.h>

namespace Utility
{
// Naming helper for ComPtr<T>.
// Assigns the name of the variable as the name of the object.
// The indexed variant will include the index in the name of the object.
#define NAME_D3D12_OBJECT(x) SetName((x).Get(), L#x)
#define NAME_D3D12_OBJECT_INDEXED(x, n) SetNameIndexed((x)[n].Get(), L#x, n)
#define NAME_D3D12_OBJECT_TEXT(x, t) SetName((x).Get(), L#t)

    // Assign a name to the object to aid with debugging.
#if defined(_DEBUG)
    inline void SetName(ID3D12Object* pObject, LPCWSTR name)
    {
        pObject->SetName(name);
    }
    inline void SetNameIndexed(ID3D12Object* pObject, LPCWSTR name, UINT index)
    {
        WCHAR fullName[50];
        if (swprintf_s(fullName, L"%s[%u]", name, index) > 0)
        {
            pObject->SetName(fullName);
        }
    }
#else
    inline void SetName(ID3D12Object*, LPCWSTR)
    {
    }
    inline void SetNameIndexed(ID3D12Object*, LPCWSTR, UINT)
    {
    }
#endif


    inline std::string HrToString(HRESULT hr)
    {
        char s_str[64] = {};
        sprintf_s(s_str, "HRESULT of 0x%08X", static_cast<UINT>(hr));
        return std::string(s_str);
    }

    class HrException : public std::runtime_error
    {
    public:
        HrException(HRESULT hr) : std::runtime_error(HrToString(hr)), m_hr(hr) {}
        HRESULT Error() const { return m_hr; }
    private:
        const HRESULT m_hr;
    };

#define SAFE_RELEASE(p) if (p) (p)->Release()

    inline void ThrowIfFailed(HRESULT hr)
    {
        if (FAILED(hr))
        {
            throw HrException(hr);
        }
    }

#define randseed(x)  Utility::RandomGenerator::GetSingleton()->RandSeed(x)
#define randi(f,t)   Utility::RandomGenerator::GetSingleton()->RandInt(f,t)
#define randf(f,t)   Utility::RandomGenerator::GetSingleton()->RandFloat(f,t)
#define randd(f,t)   Utility::RandomGenerator::GetSingleton()->RandDouble(f,t)

    class RandomGenerator
    {
    public:
        inline void RandSeed(int seed)
        {
            m_randomEngine.seed(seed);
        }

        inline int RandInt(int from, int to)
        {
            std::uniform_int_distribution<int> disti(from, to);
            return disti(m_randomEngine);
        }

        inline float RandFloat(float from, float to)
        {
            std::uniform_real_distribution<float> distf(from, to);
            return distf(m_randomEngine);
        }

        inline double RandDouble(double from, double to)
        {
            std::uniform_real_distribution<double> distd(from, to);
            return distd(m_randomEngine);
        }

        static inline RandomGenerator* GetSingleton()
        {
            if (singleton == nullptr)
            {
                singleton = new RandomGenerator();
            }
            return singleton;
        }

        static inline void DestructRandomGenerator()
        {
            if (singleton) delete singleton;
        }
    private:
        static RandomGenerator* singleton;
        std::mt19937 m_randomEngine;
    };

#define SAMPLER_COUNT 7

    class D3DUtility
    {
    public:
        //create a buffer on GPU and upload subresource
        static ComPtr<ID3D12Resource> CreateGPUBuffer(
            ID3D12Device* device, ID3D12GraphicsCommandList* commandList,
            const void* initData, UINT64 byteSize, ComPtr<ID3D12Resource>& uploadBuffer,
            D3D12_RESOURCE_FLAGS flags = D3D12_RESOURCE_FLAG_NONE);

        static ComPtr<ID3D12Resource> CreateUninitGPUBuffer(
            ID3D12Device* device,UINT64 byteSize, D3D12_RESOURCE_FLAGS flags = D3D12_RESOURCE_FLAG_NONE);
        //load and compile shader
        static ComPtr<ID3DBlob> LoadAndCompileShader(const std::wstring& filename,
            const D3D_SHADER_MACRO* defines, const std::string& entrypoint, const std::string& target);
        //load a compiled shader
        static ComPtr<ID3DBlob> LoadCompiledShader(const std::wstring& filename);
        //texture samplers
        static std::array<const CD3DX12_STATIC_SAMPLER_DESC, SAMPLER_COUNT> GetStaticSamplers();
        //shader resource view descriptor heap initialize for texture
        static void CreateSRVForTexture(ID3D12Device* device,
            ComPtr<ID3D12Resource>& resource, CD3DX12_CPU_DESCRIPTOR_HANDLE hDescriptor);
    };
}