#include "pch.h"
#include "Mesh.h"
#include "VertexData.h"
using namespace VertexData;
#include "DirectX12Graphics.h"

std::unique_ptr<MeshData> MeshData::CreateCube(float width, float height, float depth)
{
	std::unique_ptr<MeshData> mesh(new MeshData());

	float w2 = width * 0.5f;
	float h2 = height * 0.5f;
	float d2 = depth * 0.5f;

	mesh->Vertices = {
		//face 1 front 
		{XMFLOAT3(w2, h2, d2), XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT2(0.0f,0.0f)},
		{XMFLOAT3(-w2, h2, d2), XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT2(0.0f,1.0f)},
		{XMFLOAT3(w2,-h2, d2), XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT2(1.0f,0.0f)},
		{XMFLOAT3(-w2,-h2, d2), XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT2(1.0f,1.0f)},

		//face 2  back					   
		{XMFLOAT3(-w2, h2,-d2), XMFLOAT3(0.0f, 0.0f,-1.0f), XMFLOAT2(0.0f,0.0f)},
		{XMFLOAT3(w2, h2,-d2), XMFLOAT3(0.0f, 0.0f,-1.0f), XMFLOAT2(0.0f,1.0f)},
		{XMFLOAT3(-w2,-h2,-d2), XMFLOAT3(0.0f, 0.0f,-1.0f), XMFLOAT2(1.0f,0.0f)},
		{XMFLOAT3(w2,-h2,-d2), XMFLOAT3(0.0f, 0.0f,-1.0f), XMFLOAT2(1.0f,1.0f)},

		//face 3  	left	
		{XMFLOAT3(-w2, h2, d2),XMFLOAT3(-1.0f, 0.0f,0.0f),XMFLOAT2(0.0f,0.0f)},
		{XMFLOAT3(-w2, h2,-d2),XMFLOAT3(-1.0f, 0.0f,0.0f),XMFLOAT2(0.0f,1.0f)},
		{XMFLOAT3(-w2,-h2, d2),XMFLOAT3(-1.0f, 0.0f,0.0f),XMFLOAT2(1.0f,0.0f)},
		{XMFLOAT3(-w2,-h2,-d2),XMFLOAT3(-1.0f, 0.0f,0.0f),XMFLOAT2(1.0f,1.0f)},

		//face 4  right
		{XMFLOAT3(w2, h2,-d2),XMFLOAT3(1.0f, 0.0f,0.0f),XMFLOAT2(0.0f,0.0f)},
		{XMFLOAT3(w2, h2, d2),XMFLOAT3(1.0f, 0.0f,0.0f),XMFLOAT2(0.0f,1.0f)},
		{XMFLOAT3(w2,-h2,-d2),XMFLOAT3(1.0f, 0.0f,0.0f),XMFLOAT2(1.0f,0.0f)},
		{XMFLOAT3(w2,-h2, d2),XMFLOAT3(1.0f, 0.0f,0.0f),XMFLOAT2(1.0f,1.0f)},

		//face 5 top
		{XMFLOAT3(-w2, h2, d2),XMFLOAT3(0.0f, 1.0f, 0.0f),XMFLOAT2(0.0f,0.0f) },
		{XMFLOAT3(w2, h2, d2), XMFLOAT3(0.0f, 1.0f, 0.0f),XMFLOAT2(0.0f,1.0f) },
		{XMFLOAT3(-w2, h2,-d2),XMFLOAT3(0.0f, 1.0f, 0.0f),XMFLOAT2(1.0f,0.0f) },
		{XMFLOAT3(w2, h2,-d2), XMFLOAT3(0.0f, 1.0f, 0.0f),XMFLOAT2(1.0f,1.0f) },

		//face 6  bottom			
		{XMFLOAT3(w2,-h2, d2),XMFLOAT3(0.0f, -1.0f, 0.0f),XMFLOAT2(0.0f,0.0f) },
		{XMFLOAT3(-w2,-h2, d2),XMFLOAT3(0.0f, -1.0f, 0.0f),XMFLOAT2(0.0f,1.0f) },
		{XMFLOAT3(w2,-h2,-d2),XMFLOAT3(0.0f, -1.0f, 0.0f),XMFLOAT2(1.0f,0.0f) },
		{XMFLOAT3(-w2,-h2,-d2),XMFLOAT3(0.0f, -1.0f, 0.0f),XMFLOAT2(1.0f,1.0f) },
	};

	for (int i = 0; i < 24; i += 4)
	{
		mesh->Indices.push_back(i);
		mesh->Indices.push_back(i + 1);
		mesh->Indices.push_back(i + 2);
		mesh->Indices.push_back(i + 1);
		mesh->Indices.push_back(i + 3);
		mesh->Indices.push_back(i + 2);
	}

	return mesh;
}

std::unique_ptr<MeshData> MeshData::CreateGrid(float width, float depth, size_t widthCount, size_t depthCount)
{
	std::unique_ptr<MeshData> mesh(new MeshData());

	size_t vertexCount = widthCount * depthCount;
	size_t triangleCount = (widthCount - 1) * (depthCount - 1) * 2;

	float halfWidth = 0.5f * width;
	float halfDepth = 0.5f * depth;

	float dx = width / (widthCount - 1);
	float dz = depth / (depthCount - 1);

	float du = 1.0f / (widthCount - 1);
	float dv = 1.0f / (depthCount - 1);

	mesh->Vertices.resize(vertexCount);
	for (uint32_t i = 0; i < depthCount; ++i)
	{
		float z = halfDepth - i * dz;
		for (uint32_t j = 0; j < widthCount; ++j)
		{
			float x = -halfWidth + j * dx;

			mesh->Vertices[i * widthCount + j].Position = XMFLOAT3(x, 0.0f, z);
			mesh->Vertices[i * widthCount + j].Normal = XMFLOAT3(0.0f, 1.0f, 0.0f);
			//mesh->Vertices[i * widthCount + j].TangentU = XMFLOAT3(1.0f, 0.0f, 0.0f);

			mesh->Vertices[i * widthCount + j].TexCoord.x = j * du;
			mesh->Vertices[i * widthCount + j].TexCoord.y = i * dv;
		}
	}

	mesh->Indices.resize(triangleCount * 3);

	uint32_t k = 0;
	for (uint32_t i = 0; i < depthCount - 1; ++i)
	{
		for (uint32_t j = 0; j < widthCount - 1; ++j)
		{
			mesh->Indices[k] = i * widthCount + j;
			mesh->Indices[k + 1] = i * widthCount + j + 1;
			mesh->Indices[k + 2] = (i + 1) * widthCount + j;

			mesh->Indices[k + 3] = (i + 1) * widthCount + j;
			mesh->Indices[k + 4] = i * widthCount + j + 1;
			mesh->Indices[k + 5] = (i + 1) * widthCount + j + 1;

			k += 6; // next quad
		}
	}
	return mesh;
}

MeshGeometry::MeshGeometries* MeshGeometry::pMeshGeometriesManage = nullptr;

void MeshGeometry::BuildGeometries(MeshGeometries& geometries)
{
	auto device = Graphics::GetDevice();
	auto commandList = Graphics::GetCommandList();

	pMeshGeometriesManage = &geometries;
	///1///mesh Geometries
	auto cube = MeshData::CreateCube(1.0f, 1.0f, 1.0f);
	auto grid = MeshData::CreateGrid(16.0f, 16.0f, 8, 8);

	//concatenating all geometry into one vertex-index buffer
	//vertex and offsete
	UINT cubeVertexOffset = 0;
	UINT gridVertexOffset = static_cast<UINT>(cube->Vertices.size());

	//index and offset
	UINT cubeIndexOffset = 0;
	UINT gridIndexOffset = static_cast<UINT>(cube->Indices.size());

	SubMeshGeometry cubeSubMesh;
	cubeSubMesh.IndexCount = static_cast<UINT>(cube->Indices.size());
	cubeSubMesh.StartIndexLocation = cubeIndexOffset;
	cubeSubMesh.BaseVertexLocation = cubeVertexOffset;

	SubMeshGeometry gridSubMesh;
	gridSubMesh.IndexCount = static_cast<UINT>(grid->Indices.size());
	gridSubMesh.StartIndexLocation = gridIndexOffset;
	gridSubMesh.BaseVertexLocation = gridVertexOffset;

	//pack the vertices of all meshs into one vertex buffer
	auto totalVertexCount =
		cube->Vertices.size() +
		grid->Vertices.size();

	std::vector<VertexPosNormTexcoord> vertices(totalVertexCount);
	UINT k = 0;
	for (size_t i = 0; i < cube->Vertices.size(); ++i, ++k)
	{
		vertices[k].Position = cube->Vertices[i].Position;
		vertices[k].Normal = cube->Vertices[i].Normal;
		vertices[k].TexCoord = cube->Vertices[i].TexCoord;
	}

	for (size_t i = 0; i < grid->Vertices.size(); ++i, ++k)
	{
		vertices[k].Position = grid->Vertices[i].Position;
		vertices[k].Normal = grid->Vertices[i].Normal;
		vertices[k].TexCoord = grid->Vertices[i].TexCoord;
	}

	std::vector<uint16_t> indices;
	indices.insert(indices.end(), cube->Indices.begin(), cube->Indices.end());
	indices.insert(indices.end(), grid->Indices.begin(), grid->Indices.end());

	const UINT vbByteSize = static_cast<UINT>(vertices.size() * sizeof(VertexPosNormTexcoord));
	const UINT ibByteSize = static_cast<UINT>(indices.size() * sizeof(uint16_t));

	auto geo = std::make_unique<MeshGeometry>();
	geo->Name = "MeshGeometry";

	ThrowIfFailed(D3DCreateBlob(vbByteSize, &geo->VertexBufferCPU));
	CopyMemory(geo->VertexBufferCPU->GetBufferPointer(), vertices.data(), vbByteSize);

	ThrowIfFailed(D3DCreateBlob(ibByteSize, &geo->IndexBufferCPU));
	CopyMemory(geo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize);

	geo->VertexBufferGPU = D3DUtility::CreateGPUBuffer(
		device, commandList, vertices.data(), vbByteSize, geo->VertexBufferUploader);

	geo->IndexBufferGPU = D3DUtility::CreateGPUBuffer(
		device, commandList, indices.data(), ibByteSize, geo->IndexBufferUploader);


	geo->VertexByteStride = sizeof(VertexPosNormTexcoord);
	geo->VertexBufferByteSize = vbByteSize;
	geo->IndexFormat = DXGI_FORMAT_R16_UINT;
	geo->IndexBufferByteSize = ibByteSize;

	geo->DrawArgs["cube"] = cubeSubMesh;
	geo->DrawArgs["grid"] = gridSubMesh;

	geometries[MeshGeo] = std::move(geo);


}

D3D12_VERTEX_BUFFER_VIEW MeshGeometry::VertexBufferView() const
{

	D3D12_VERTEX_BUFFER_VIEW vbv;
	vbv.BufferLocation = VertexBufferGPU->GetGPUVirtualAddress();
	vbv.StrideInBytes = VertexByteStride;
	vbv.SizeInBytes = VertexBufferByteSize;

	return vbv;

}

D3D12_INDEX_BUFFER_VIEW MeshGeometry::IndexBufferView() const
{
	D3D12_INDEX_BUFFER_VIEW ibv;
	ibv.BufferLocation = IndexBufferGPU->GetGPUVirtualAddress();
	ibv.Format = IndexFormat;
	ibv.SizeInBytes = IndexBufferByteSize;

	return ibv;
}

void MeshGeometry::DisposeUploaders()
{

	VertexBufferUploader = nullptr;
	IndexBufferUploader = nullptr;

}
