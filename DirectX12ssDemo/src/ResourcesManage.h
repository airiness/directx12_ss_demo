#pragma once

using InputLayoutsManage = std::unordered_map<uint32_t, std::unique_ptr<std::vector<D3D12_INPUT_ELEMENT_DESC>>>;
using DescriptorHeapsManage = std::unordered_map<uint32_t, ComPtr<ID3D12DescriptorHeap>>;
using RootSignaturesManage = std::unordered_map<uint32_t, ComPtr<ID3D12RootSignature>>;
using PSOsManage = std::unordered_map<uint32_t, ComPtr<ID3D12PipelineState>>;
using ShadersManage = std::unordered_map<uint32_t, ComPtr<ID3DBlob>>;

//use enum as a mini database to management resources
enum ObjectsIndex : uint32_t
{
	ObjCamera = 0,
	ObjLights0,
	ObjParticle0,
	ObjRigidBody0,
	ObjectCount
};

enum ShadersIndex : uint32_t
{
	ShaderLightingVS = 0,
	ShaderBloomPS,
	ShaderOpaqueLightingPS,
	ShaderAlphaLightingPS,
	ShaderParticleVS,
	ShaderParticleGS,
	ShaderParticlePS,
	ShaderParticleCS,
	ShaderRigidBodyVS,
	ShaderRigidBodyGS,
	ShaderRigidBodyPS,
	ShaderRigidBodyCS,
	ShaderCount
};

enum DescriptorHeapsIndex : uint32_t
{
	DHeapIMGUI = 0,
	DHeapTextures,
	DHeapParticles,
	DHeapRigidBody,
	DescriptorHeapCount
};

enum RootSignaturesIndex : uint32_t
{
	RootSigLighting = 0,
	RootSigParticleDraw,
	RootSigParticleCS,
	RootSigRigidBodyDraw,
	RootSigRigidBodyCompute,
	RootSignatureCount
};

enum PSOsIndex : uint32_t
{
	PSOLightingDefault = 0,
	PSOWireFrame,
	PSOTransparent,
	PSOBloom,
	PSOParticleDraw,
	PSOParticleWireframeDraw,
	PSOParticleCompute,
	PSORigidBodyDraw,
	PSORigidBodyWireframeDraw,
	PSORigidBodyCompute,
	PSOCount
};

enum InputLayoutsIndex : uint32_t
{
	InputLayoutPosNormTex = 0,
	InputLayoutColor,
	InputLayoutPosNormColor,
	InputLayoutCount
};

enum RenderItemsIndex : uint32_t
{
	RILayerDefault = 0,
	RILayerTransparent,
	RenderItemsLayerCount
};
