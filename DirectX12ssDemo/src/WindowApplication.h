#pragma once
#include "Keyboard.h"
#include "Mouse.h"
class DirectX12App;

class WindowApplication
{
public:
	static int Run(DirectX12App* d3d12app,HINSTANCE hInstance ,int nCmdShow);
	static void ToggleFullscreenWindow(IDXGISwapChain* swapchain = nullptr);
	static HWND GetHandle() { return m_hWnd; }

	static std::unique_ptr<Input::Keyboard> m_pKeyboard;
	static std::unique_ptr<Input::Mouse> m_pMouse;

protected:
	static LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static double CalculateFrameState(const std::wstring& TitleText);

	static bool IsDone();
private:
	static HWND m_hWnd;
	static bool m_fullscreenMode;
	static const UINT m_windowStyle = WS_OVERLAPPEDWINDOW & ~(WS_THICKFRAME | WS_MAXIMIZEBOX);
	static bool m_bIsQuit;
	static RECT m_windowRect;	//for saving the window state when in windows mode
};

