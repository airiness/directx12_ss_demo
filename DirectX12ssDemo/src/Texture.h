//textures and load textures
//zhang ruisong
#pragma once
struct Texture
{
	using Textures = std::vector<std::unique_ptr<Texture>>;

	static void LoadTextures(Textures& textures,ID3D12DescriptorHeap** descriptorHeap);
	static Textures* pTexturesManage;
	static Textures* GetTextures() { return pTexturesManage; };

	std::string Name;
	std::wstring Filename;

	ComPtr<ID3D12Resource> TextureResource = nullptr;
	ComPtr<ID3D12Resource> TextureResourceUpload = nullptr;

	std::unique_ptr<uint8_t[]> TextureData = nullptr;
	std::vector<D3D12_SUBRESOURCE_DATA> SubResources;
};