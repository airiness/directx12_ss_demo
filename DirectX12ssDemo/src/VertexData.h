#pragma once
namespace VertexData
{
	struct VertexColor
	{
		Vector4 Color;
	};

	struct VertexPosColor
	{
		Vector3 Position;
		Vector4 Color;
	};

	struct VertexPosNormTexcoord
	{
		Vector3 Position;
		Vector3 Normal;
		Vector2 TexCoord;
	};
}