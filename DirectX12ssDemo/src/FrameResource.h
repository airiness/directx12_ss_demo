#pragma once
#include "UploadBuffer.h"
#include "VertexData.h"
#include "Light.h"
#include "Particle.h"
#include "RigidBody.h"

struct ObjectConstants
{
	Matrix WorldMatrix = Matrix::Identity;				//64bytes
	Matrix TexTransform = Matrix::Identity;				//64bytes
};

struct PassConstants
{
	Matrix ViewMatrix = Matrix::Identity;				//64bytes
	Matrix InverseViewMatrix = Matrix::Identity;		//64bytes
	Matrix ProjectionMatrix = Matrix::Identity;			//64bytes
	Matrix InverseProjectionMatrix = Matrix::Identity;	//64bytes
	Matrix ViewProjectionMatrix = Matrix::Identity;		//64bytes
	Matrix InverseViewProjection = Matrix::Identity;	//64bytes
	Vector3 EyePositionWorld = { 0.0f,0.0f,0.0f };		//
	float cbPadding = 0.0f;								//12+4=16bytes
	Vector2 RenderTargetSize = { 0.0f,0.0f };			//32bytes
	Vector2 InverseRenderTargetSize = { 0.0f,0.0f };	//32bytes

	float NearZ = 0.0f;
	float FarZ = 0.0f;
	float TotalTime = 0.0f;
	float deltaTime = 0.0f;								//4 * 4 = 16bytes

	Vector4 AmbientLight = { 0.0f,0.0f,0.0f,1.0f };		//16bytes

	int DirectionLightNum = 0;
	int PointLightNum = 0;
	int SpotLightNum = 0;
	int MaxLights = 0;
};

struct MaterialConstants
{
	Vector4 DiffuseAlbedo = { 1.0f,1.0f,1.0f,1.0f };
	Vector3 FresnelR0 = { 0.01f,0.01f,0.01f };
	float Roughness = 0.25f;
	Matrix TransformMatrix = Matrix::Identity;
};

struct FrameResource
{
	using FrameResourcesManage = std::vector<std::unique_ptr<FrameResource>>;
	static void BuildFrameResources(FrameResourcesManage& frameResources,ID3D12Device* device,
		size_t passCount, size_t objectCount,size_t materialCount, size_t lightCount);
	static FrameResourcesManage* pFrameResourcesManage;
	static FrameResourcesManage* GetFrameResources() { return pFrameResourcesManage; };
	static FrameResource* GetCurrentFrameResource();

	FrameResource(ID3D12Device* device, 
		size_t passCount, size_t objectCount, 
		size_t materialCount,size_t lightCount);
	FrameResource(const FrameResource&) = delete;
	FrameResource& operator=(const FrameResource&) = delete;
	~FrameResource();

	ComPtr<ID3D12CommandAllocator> CmdListAlloc;

	std::unique_ptr<UploadBuffer<PassConstants>> PassCB = nullptr;
	std::unique_ptr<UploadBuffer<MaterialConstants>> MaterialCB = nullptr;
	std::unique_ptr<UploadBuffer<ObjectConstants>> ObjectCB = nullptr;
	std::unique_ptr<UploadBuffer<LightData>> LightSrv = nullptr;
	std::unique_ptr<UploadBuffer<ParticleDrawConstants>> ParticleDrawCB = nullptr;
	uint64_t FenceValue = 0;
};