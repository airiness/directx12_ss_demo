//create a uploadbuffer on gpu ,and map a pointer on cpu
//zhangc rui song
#pragma once

template<typename T>
class UploadBuffer
{
public:
	UploadBuffer(ID3D12Device* device, size_t elementCount, bool isConstantBuffer);
	UploadBuffer(const UploadBuffer&) = delete;
	UploadBuffer& operator=(const UploadBuffer&) = delete;
	~UploadBuffer();

	ID3D12Resource* Resource() const;

	void CopyData(int elementIndex, const T& data);
	void CopyAllData(const T* data);

private:
	ComPtr<ID3D12Resource> m_UploadBuffer;
	uint8_t* m_MappedData = nullptr;
	size_t m_ElementByteSize = 0;
	size_t m_ElementCount = 0;
	bool m_bIsConstantBuffer = false;
};

template<typename T>
inline UploadBuffer<T>::UploadBuffer(ID3D12Device* device, size_t elementCount, bool isConstantBuffer)
	:m_ElementCount(elementCount)
{
	//conatant buffer must be 256 byte aligned
	m_ElementByteSize = isConstantBuffer ? ((sizeof(T) + 255) & ~255) : sizeof(T);
	ThrowIfFailed(device->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(m_ElementByteSize * elementCount),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(m_UploadBuffer.GetAddressOf())));

	ThrowIfFailed(m_UploadBuffer->Map(0, nullptr, reinterpret_cast<void**>(&m_MappedData)));
}

template<typename T>
inline UploadBuffer<T>::~UploadBuffer()
{
	if (m_UploadBuffer!=nullptr)
	{
		m_UploadBuffer->Unmap(0, nullptr);
	}

	m_MappedData = nullptr;
}

template<typename T>
inline ID3D12Resource* UploadBuffer<T>::Resource() const
{
	return m_UploadBuffer.Get();
}

template<typename T>
inline void UploadBuffer<T>::CopyData(int elementIndex, const T& data)
{
	std::memcpy(&m_MappedData[elementIndex * m_ElementByteSize], &data, sizeof(T));
}

template<typename T>
inline void UploadBuffer<T>::CopyAllData(const T* data)
{
	std::memcpy(m_MappedData, data, m_ElementByteSize * m_ElementCount);
}


