#pragma once

#ifndef NOMINMAX
#define NOMINMAX
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>

#include <d3d12.h>
#include <dxgi1_6.h>
#include <d3dcompiler.h>

#pragma comment(lib,"d3d12.lib")
#pragma comment(lib,"dxgi.lib")
#pragma comment(lib,"d3dcompiler.lib")
#pragma comment(lib,"dxguid.lib")

#include "d3dx12.h"

#include <wrl.h>
using namespace Microsoft::WRL;

//include PIX event support
#include "pix3.h"

#include <DirectXMath.h>
#include <DirectXColors.h>
using namespace DirectX;

#include "SimpleMath.h"
using namespace SimpleMath;

#include "DDSTextureLoader12.h"

#include "Utility.h"
using namespace Utility;

#include <iostream>
#include <string>
#include <algorithm>
#include <chrono>
#include <cassert>
#include <cstdint>
#include <queue>
#include <array>
#include <random>
#include <map>
#include <unordered_map>

#pragma warning(disable:26812) //disable enum scope warning