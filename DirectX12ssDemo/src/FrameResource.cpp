#include "pch.h"
#include "FrameResource.h"
#include "DirectX12Graphics.h"

FrameResource::FrameResourcesManage* FrameResource::pFrameResourcesManage = nullptr;

void FrameResource::BuildFrameResources(FrameResourcesManage& frameResources, ID3D12Device* device,
	size_t passCount, size_t objectCount, size_t materialCount, size_t lightCount)
{
	pFrameResourcesManage = &frameResources;
	for (size_t i = 0; i < Graphics::FrameCount; i++)
	{
		//
		frameResources.push_back(std::make_unique<FrameResource>(device, 
			passCount,objectCount,materialCount,lightCount));
	}
}

FrameResource* FrameResource::GetCurrentFrameResource()
{
	return (*pFrameResourcesManage)[Graphics::g_CurrentBackBuffer].get();
}

FrameResource::FrameResource(ID3D12Device* device,
	size_t passCount, size_t objectCount, 
	size_t materialCount, size_t lightCount)
{
	ThrowIfFailed(device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(CmdListAlloc.GetAddressOf())));

	PassCB = std::make_unique<UploadBuffer<PassConstants>>(device, passCount, true);
	MaterialCB = std::make_unique<UploadBuffer<MaterialConstants>>(device, materialCount, true);
	ObjectCB = std::make_unique<UploadBuffer<ObjectConstants>>(device, objectCount, true);
	LightSrv = std::make_unique<UploadBuffer<LightData>>(device, lightCount, false);
	ParticleDrawCB = std::make_unique<UploadBuffer<ParticleDrawConstants>>(device, 1, true);
}

FrameResource::~FrameResource()
{
}
