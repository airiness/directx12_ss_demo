# DirectX12_ss_Demo

DirectX12 is so interesting!
I will add what I learned here about graphics or DirectX12 and something else.

## screenshots:
***
Phong-blinn reflection model lighting:<br>
![](screenshots/lighting.png)
***
compute shader particles:<br>
![](screenshots/CSparticles.png)
***
OBB collision detection:<br>
![](screenshots/OBBcollision.png)

***
quternion rotation and movement in gravity:<br>
![](/screenshots/QuternionRotation.png)
